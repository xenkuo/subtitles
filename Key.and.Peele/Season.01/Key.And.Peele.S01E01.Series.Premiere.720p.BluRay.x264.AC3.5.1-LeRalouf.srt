﻿1
00:00:01,627 --> 00:00:04,253
- Because you're my wife
and you love the theater剧场,

2
00:00:04,380 --> 00:00:07,006
and, uh,
it's your birthday.

3
00:00:07,132 --> 00:00:08,675
Great.

4
00:00:08,801 --> 00:00:11,969
Unfortunately the orchestra乐池's
already filled up,

5
00:00:12,096 --> 00:00:13,805
but they do have seats
that are still left

6
00:00:13,931 --> 00:00:15,556
in the dress circle.

7
00:00:15,683 --> 00:00:17,475
So if you want to, uh, me to get
them theater tickets right now,

8
00:00:17,601 --> 00:00:18,685
I'ma do it right now.

9
00:00:18,811 --> 00:00:19,852
- What's up, dog?

10
00:00:19,978 --> 00:00:21,020
I'm about five minutes away.

11
00:00:21,146 --> 00:00:22,563
- Yeah, okay.
Yeah, cool.

12
00:00:22,690 --> 00:00:24,148
No, they all good singers,
they all good singers.

13
00:00:24,274 --> 00:00:25,566
- Yeah, son. Nah, man,
I'm telling you, man,

14
00:00:25,693 --> 00:00:27,193
I'm about
to cross the street, man.

15
00:00:27,319 --> 00:00:28,361
- No, they got that one dude
in it that you love, man.

16
00:00:28,487 --> 00:00:30,029
He gon' be in it, yeah...

17
00:00:30,155 --> 00:00:31,781
- Come on, man, you know
I'm almost there, all right?

18
00:00:31,907 --> 00:00:32,990
- Right, no, I'ma pick
your ass up at 6:30, then.

19
00:00:33,117 --> 00:00:34,659
- Cool.
- Cool.

20
00:00:34,785 --> 00:00:36,869
All right, yeah, yeah,
yeah, the parking is, uh,

21
00:00:36,995 --> 00:00:38,788
the parking's free.

22
00:00:38,914 --> 00:00:40,206
Oh, my God, Christian.

23
00:00:40,332 --> 00:00:42,917
I almost totally
just got mugged被抢劫 right now.

24
00:01:05,691 --> 00:01:11,446
- Yeah, whoa.

25
00:01:11,572 --> 00:01:13,030
Welcome.

26
00:01:13,157 --> 00:01:14,949
Welcome to the show,
everybody.

27
00:01:15,075 --> 00:01:16,659
I am Keegan.
- I am Jordan.

28
00:01:16,785 --> 00:01:18,035
- And this is
Key & Peele.

29
00:01:18,162 --> 00:01:19,370
Thank you, yes.
Thank you.

30
00:01:22,040 --> 00:01:24,041
Jordan and I are-
we're biracial双种族的.

31
00:01:24,168 --> 00:01:26,002
- Yes, half black,
half white.

32
00:01:26,128 --> 00:01:27,378
- And because of that,
we find ourselves

33
00:01:27,504 --> 00:01:32,049
particularly adept擅长的
at lying.

34
00:01:32,176 --> 00:01:35,303
Because on a daily basis we
have to adjust our blackness.

35
00:01:35,429 --> 00:01:37,638
- Yes.
- You know what I mean?

36
00:01:37,765 --> 00:01:38,890
- Oh, no, there's many
reasons we do that.

37
00:01:39,016 --> 00:01:40,391
- Yeah.

38
00:01:40,517 --> 00:01:41,851
- I mean,
to terrify white people.

39
00:01:41,977 --> 00:01:43,019
- Yes, that's one
of the main reasons.

40
00:01:43,145 --> 00:01:44,854
One of the main reasons,
yeah.

41
00:01:44,980 --> 00:01:46,481
- Because, I mean, you know,
with the way that we sound,

42
00:01:46,607 --> 00:01:48,274
the way
that we actually talk,

43
00:01:48,400 --> 00:01:49,734
we're not intimidating威吓
anybody with the way we sound.

44
00:01:49,860 --> 00:01:51,110
- Oh, no, no, no.
- We sound very white.

45
00:01:51,236 --> 00:01:53,362
- We sound whiter
than the black dude

46
00:01:53,489 --> 00:01:56,032
in the college
a cappella group合唱团.

47
00:01:56,158 --> 00:02:00,036
- Yes, we sound whiter than
Mitt Romney in a snowstorm.

48
00:02:00,162 --> 00:02:04,916
You know what I mean?

49
00:02:05,042 --> 00:02:06,417
But that's just-
that's one of-

50
00:02:06,543 --> 00:02:08,127
that's the reason
when we're around

51
00:02:08,253 --> 00:02:10,463
other brothers and sisters,
you will see us-

52
00:02:10,589 --> 00:02:11,672
you got to just-
- You know, you got to

53
00:02:11,799 --> 00:02:12,840
dial it up a little bit.

54
00:02:12,966 --> 00:02:14,550
- You got to dial it up.

55
00:02:14,676 --> 00:02:15,927
Oh, see, you know what
I'm talking about, brother.

56
00:02:16,053 --> 00:02:17,094
- Oh, you know I know

57
00:02:17,221 --> 00:02:18,387
what you talking about,
right?

58
00:02:18,514 --> 00:02:20,181
- You know I know you know-

59
00:02:20,307 --> 00:02:21,682
- You know I know what you know
I know what you talking about.

60
00:02:21,809 --> 00:02:23,184
No doubt, man.
No doubt, no doubt, no doubt.

61
00:02:23,310 --> 00:02:24,644
No doubt, no doubt, no doubt,

62
00:02:24,770 --> 00:02:26,938
no doubt, no doubt,
no doubt, no doubt.

63
00:02:27,064 --> 00:02:30,942
- Because you never want
to be the whitest-sounding

64
00:02:31,068 --> 00:02:32,735
black guy in a room.

65
00:02:32,861 --> 00:02:35,196
- You put five white-talking
black guys in the same room,

66
00:02:35,322 --> 00:02:36,656
you come back in a hour,
it's gonna be like

67
00:02:36,782 --> 00:02:37,865
Ladysmith Black Mambazo
up in there.

68
00:02:37,991 --> 00:02:39,033
- Straight up.

69
00:02:51,171 --> 00:02:53,381
- Hey, hey, hey, all right.
- Hey.

70
00:02:53,507 --> 00:02:54,924
- Hi, sweetie,
that's for you.

71
00:02:55,050 --> 00:02:56,092
- Hey, Trace.
How you doing, girl?

72
00:02:56,218 --> 00:02:57,635
- Check out the house.
- Oh, my.

73
00:02:57,761 --> 00:02:59,178
- Girl, I got a sunken凹陷的 tub盆.
You got to see it.

74
00:02:59,304 --> 00:03:00,888
- Oh, I got to see this.
- All right.

75
00:03:01,014 --> 00:03:02,223
- You two
have a good time now.

76
00:03:02,349 --> 00:03:04,058
- Have fun, have fun.

77
00:03:04,184 --> 00:03:05,518
Dude, I am sorry
we're late, man.

78
00:03:05,644 --> 00:03:07,228
- Man, it happens, man.

79
00:03:07,354 --> 00:03:08,729
- And she talk about how
we're supposed to be in the car

80
00:03:08,856 --> 00:03:09,897
at 6:45,
I'm like, "All right."

81
00:03:10,023 --> 00:03:11,607
- Uh-oh.

82
00:03:11,733 --> 00:03:13,067
- Tell me my dumb ass
ain't sitting in the car,

83
00:03:13,193 --> 00:03:16,487
waiting until 7:15.
- Nuh-uh.

84
00:03:16,613 --> 00:03:18,197
- Okay, when I track my wife
down 20 minutes later,

85
00:03:18,323 --> 00:03:20,533
she's stepping out
the damn shower淋浴 talking about,

86
00:03:20,659 --> 00:03:22,159
"Can I help you?"

87
00:03:22,286 --> 00:03:23,452
- See, that's crazy
right there.

88
00:03:23,579 --> 00:03:24,996
- I looked this woman
in the eye,

89
00:03:25,122 --> 00:03:27,248
I said...

90
00:03:27,374 --> 00:03:31,043
"Bitch, you told me 6:45."

91
00:03:31,169 --> 00:03:32,211
- You said that?

92
00:03:32,337 --> 00:03:34,297
- Psh, yeah I said...

93
00:03:34,423 --> 00:03:37,008
"Bitch."
Then I laid it out.

94
00:03:37,134 --> 00:03:38,175
- But you said, "bitch,"
though?

95
00:03:38,302 --> 00:03:39,552
- Hmm?

96
00:03:39,678 --> 00:03:41,012
- You said, "bitch"?

97
00:03:41,138 --> 00:03:43,389
- Yeah.

98
00:03:43,515 --> 00:03:45,474
- You got to see the fireplace
downstairs in the living room.

99
00:03:45,601 --> 00:03:47,435
- Okay.

100
00:03:47,561 --> 00:03:48,895
- Don't play games, man.

101
00:03:49,021 --> 00:03:50,396
Just tell me what you're
going to tell me.

102
00:03:50,522 --> 00:03:51,606
- Exactly, it's like,
say what you mean,

103
00:03:51,732 --> 00:03:52,857
mean what you say.
- Is that so hard?

104
00:03:52,983 --> 00:03:54,025
- It's like last week, man.

105
00:03:54,151 --> 00:03:55,359
We going out to dinner, right?

106
00:03:55,485 --> 00:03:56,819
I'm like,
"where do you want to go?"

107
00:03:56,945 --> 00:03:58,821
She's like, "You decide."
- Uh-oh.

108
00:03:58,947 --> 00:03:59,989
- I'm like, "All right,
Outback内地的 Steakhouse牛排餐厅."

109
00:04:00,115 --> 00:04:01,490
She like, "Nah."
- Mm-hmm.

110
00:04:01,617 --> 00:04:02,825
- I'm like,
"Straight up, Chili's."

111
00:04:02,951 --> 00:04:04,619
She's like, "Ehh."
- No, no.

112
00:04:04,745 --> 00:04:06,662
- Darrell, I named
seven more restaurants.

113
00:04:06,788 --> 00:04:08,289
- No, Craig, no.

114
00:04:08,415 --> 00:04:09,498
- I finally said,
"Taylor's," the place I know

115
00:04:09,625 --> 00:04:10,666
she wants to go
in the first place.

116
00:04:10,792 --> 00:04:11,834
- Right, right.

117
00:04:11,960 --> 00:04:13,085
- She look at me, she said,

118
00:04:13,211 --> 00:04:14,879
"If that's where
you want to go."

119
00:04:15,005 --> 00:04:17,006
- No, she didn't, Craig.
- If that's where I want to go.

120
00:04:17,132 --> 00:04:19,675
Darrell, I looked my woman
in the eye sockets.

121
00:04:19,801 --> 00:04:22,762
I told her straight out,
I just said it, man, I said it.

122
00:04:22,888 --> 00:04:25,222
I said, I said, I said...

123
00:04:29,645 --> 00:04:31,270
I said, "Bi..."
- Hey, guys.

124
00:04:31,396 --> 00:04:32,438
- Hey, girl, how you doing?
- Oh, how you doing?

125
00:04:32,564 --> 00:04:34,023
- You having a good time?

126
00:04:34,149 --> 00:04:35,733
- You seen the bedroom?
- Just looking at the wood.

127
00:04:35,859 --> 00:04:37,526
- That washing machine is huge.
- Up on the ceiling here.

128
00:04:37,653 --> 00:04:39,862
- You get a whole bunch of
clothes in that washing machine.

129
00:04:39,988 --> 00:04:41,155
- Hey, baby, I'm going to take
her back up to the kitchen

130
00:04:41,281 --> 00:04:42,990
and show her the dishwasher洗碗机.

131
00:04:43,116 --> 00:04:44,909
- Darrell?
- Yeah, baby?

132
00:04:45,035 --> 00:04:46,786
- I want a kitchen island
just like the one upstairs.

133
00:04:46,912 --> 00:04:48,371
- You gonna get it too.

134
00:04:48,497 --> 00:04:52,083
- I love you.

135
00:04:52,209 --> 00:04:57,046
- I said, "Bitch,
if you wanted to go to Taylor's,

136
00:04:57,172 --> 00:04:59,131
just tell a brother
you want to go to Taylor's!"

137
00:04:59,257 --> 00:05:00,758
Okay?
- You said that?

138
00:05:00,884 --> 00:05:02,927
- Oh, hell yeah, man.
I laid it out, right?

139
00:05:03,053 --> 00:05:08,849
I says-I says-I says...

140
00:05:08,976 --> 00:05:12,520
I said, "Bitch,
I'm the man of the house."

141
00:05:12,646 --> 00:05:13,688
- You said, "bitch," though?

142
00:05:13,814 --> 00:05:14,855
- Hmm?

143
00:05:14,982 --> 00:05:16,732
- You called your wife a bitch?

144
00:05:16,858 --> 00:05:18,317
- Uh-huh, yeah.

145
00:05:18,443 --> 00:05:19,485
- Craig.
- Darrell.

146
00:05:19,611 --> 00:05:21,112
- Where are those guys?

147
00:05:21,238 --> 00:05:25,574
- I don't know.
Let's go...

148
00:05:25,701 --> 00:05:27,201
- So she's like,
"why don't you rent a movie

149
00:05:27,327 --> 00:05:28,953
we both like?"
- No, she didn't.

150
00:05:29,079 --> 00:05:31,163
- After I spent 25 minutes
in the goddamn Blockbuster.

151
00:05:31,289 --> 00:05:35,042
Craig, I looked this woman
in her optic stems, and I says-

152
00:05:35,168 --> 00:05:37,169
I said...

153
00:05:39,339 --> 00:05:41,424
I says, "Bitch."

154
00:05:41,550 --> 00:05:42,633
- You said that?

155
00:05:42,759 --> 00:05:44,218
- Ain't nothing
but a thing.

156
00:05:44,344 --> 00:05:46,595
- But you said, "bitch,"
though?

157
00:05:46,722 --> 00:05:48,431
- Yep.
- See...

158
00:05:52,394 --> 00:05:54,228
- Oh, sh-

159
00:05:54,354 --> 00:05:57,273
Hey, honey, Craig's just
giving me the neighborhood tour.

160
00:05:57,399 --> 00:05:59,316
- So then she's like,
"I didn't know we'd be doing

161
00:05:59,443 --> 00:06:01,360
so much walking."
- Nuh-uh.

162
00:06:01,486 --> 00:06:04,280
- I'm like, "I didn't tell you
to wear those shoes."

163
00:06:04,406 --> 00:06:06,615
She said, "Don't raise
your voice at me."

164
00:06:06,742 --> 00:06:07,992
- What?

165
00:06:08,118 --> 00:06:11,746
- Dar-rell, I looked this woman

166
00:06:11,872 --> 00:06:14,415
dead in the windows
of her soul.

167
00:06:14,541 --> 00:06:16,751
- Mm-hmm.
- I said...

168
00:06:23,383 --> 00:06:25,760
I said...

169
00:06:39,024 --> 00:06:44,528
I said, "Bitch."

170
00:06:51,912 --> 00:06:54,663
- So I'm a huge fan
of reality television真人秀.

171
00:06:54,790 --> 00:06:56,749
- And I have
little use for it.

172
00:06:56,875 --> 00:06:58,542
- Yes.

173
00:06:58,668 --> 00:07:00,920
All right, well, but, no,
we agree on one thing.

174
00:07:01,046 --> 00:07:04,590
That reality TV
has completely gone bananos.

175
00:07:04,716 --> 00:07:06,842
- Is redink to the hinkulous.

176
00:07:06,968 --> 00:07:08,969
- It is, it is.
- Yes.

177
00:07:09,096 --> 00:07:10,930
- It used to be very simple,
it used to be just

178
00:07:11,056 --> 00:07:13,307
live in a house
and we'll film it,

179
00:07:13,433 --> 00:07:15,518
and then we'll do
the same thing next year.

180
00:07:15,644 --> 00:07:19,188
And it was-
it was like that for ten years.

181
00:07:19,314 --> 00:07:20,981
- And then all of a sudden
it turned into

182
00:07:21,108 --> 00:07:23,025
you can live in the house,
but we might kick you out

183
00:07:23,151 --> 00:07:27,113
if you don't run the raw diet日常饮食
food co-op correctly.

184
00:07:27,239 --> 00:07:28,322
- That's right,
that's right.

185
00:07:28,448 --> 00:07:30,407
And then it was
no house for you.

186
00:07:30,534 --> 00:07:33,160
Live on a beach, starve挨饿, and
make alliances with each other.

187
00:07:33,286 --> 00:07:34,662
- Right.

188
00:07:34,788 --> 00:07:36,247
When that show happened,
that's when it went

189
00:07:36,373 --> 00:07:37,915
to straight up nut burgers
crazy town right there.

190
00:07:38,041 --> 00:07:40,251
- All of a sudden it was
be a gay man designing things

191
00:07:40,377 --> 00:07:44,171
and get angry at other gay men
designing things.

192
00:07:44,297 --> 00:07:46,423
- Or mad lesbians女同志
in a kitchen.

193
00:07:46,550 --> 00:07:49,260
- Please install a television
on my mode of transportation.

194
00:07:49,386 --> 00:07:51,512
- Mm-hmm.
Eat a bug, win a car.

195
00:07:51,638 --> 00:07:54,890
- Mm-hmm.
Dangerous fishing.

196
00:07:55,016 --> 00:07:58,894
- Or you've always been poor,
but now you have a pool table台球桌.

197
00:07:59,020 --> 00:08:00,813
- Or you have
a mental精神 illness.

198
00:08:00,939 --> 00:08:04,150
Let us rearrange
your furniture家具 for you.

199
00:08:12,367 --> 00:08:15,411
- Drew, come forward.

200
00:08:15,537 --> 00:08:16,829
- Yes, Chef.

201
00:08:16,955 --> 00:08:18,998
This is a chicken quiche乳蛋饼
with crimini克里米尼 mushrooms蘑菇,

202
00:08:19,124 --> 00:08:24,628
baby spinach菠菜,
and feta羊乳酪 cheese奶酪.

203
00:08:32,804 --> 00:08:35,848
- Unbelievable.

204
00:08:35,974 --> 00:08:38,851
Well, Drew, I have
a huge problem with this dish一道菜.

205
00:08:40,395 --> 00:08:41,437
It's that you haven't made it

206
00:08:41,563 --> 00:08:42,730
for me sooner.

207
00:08:42,856 --> 00:08:45,065
- Thank you, Chef.

208
00:08:45,192 --> 00:08:48,110
- Because, if you had,
Drew, then I would know

209
00:08:48,236 --> 00:08:51,113
how good you are
at cooking food that's bad.

210
00:08:52,407 --> 00:08:53,449
- I'm sorry, Chef.

211
00:08:53,575 --> 00:08:54,617
- And when I say, "bad,"

212
00:08:54,743 --> 00:08:57,661
I mean, Michael Jackson Bad.

213
00:08:57,787 --> 00:08:59,747
- Thank you, Chef.

214
00:08:59,873 --> 00:09:00,915
- You know how he looked
really, really bad

215
00:09:01,041 --> 00:09:02,082
at the end of his life?

216
00:09:02,209 --> 00:09:05,211
- Chef, I'm sorry.

217
00:09:05,337 --> 00:09:07,713
I don't know if you
like the dish or not.

218
00:09:07,839 --> 00:09:09,924
- You don't know
if I like the dish or not?

219
00:09:10,050 --> 00:09:11,759
Well,
let's put it this way-

220
00:09:11,885 --> 00:09:14,595
Pack your fucking knives,
get out, you're off the show.

221
00:09:14,721 --> 00:09:17,848
- Sorry, Chef.

222
00:09:17,974 --> 00:09:20,809
- Because you should
be working

223
00:09:20,936 --> 00:09:23,354
in the finest出色的 restaurant
in the world.

224
00:09:23,480 --> 00:09:25,397
- Thank you, Chef.

225
00:09:25,523 --> 00:09:27,191
- Just not any world
that I live in.

226
00:09:27,317 --> 00:09:29,193
- Sorry, Chef.

227
00:09:29,319 --> 00:09:31,445
- Because, frankly, Drew,
I'm jealous of you.

228
00:09:31,571 --> 00:09:33,864
- Thank you, Chef.

229
00:09:33,990 --> 00:09:36,033
- And your ability
to not give a shit

230
00:09:36,159 --> 00:09:38,619
about what you cook.

231
00:09:38,745 --> 00:09:42,706
This is not fit
for human consumption消费.

232
00:09:42,832 --> 00:09:46,585
No, this should be eaten
by a higher life-form

233
00:09:46,711 --> 00:09:50,464
with a more complex复杂的 palate味觉,
but also an altruistic利他的 drive

234
00:09:50,590 --> 00:09:53,968
to save humanity
from dishes like this.

235
00:09:54,094 --> 00:09:57,012
Joking. Not!
You deserve to die!

236
00:09:57,138 --> 00:09:59,765
Aah!

237
00:09:59,891 --> 00:10:03,060
So you won't have to endure
a life in which you will

238
00:10:03,186 --> 00:10:08,732
never exceed what
you have achieved here today.

239
00:10:17,117 --> 00:10:21,370
- Thank you, Chef.

240
00:10:21,496 --> 00:10:25,165
- In conclusion...
eh.

241
00:10:28,586 --> 00:10:30,629
- Lil Wayne, y'all.

242
00:10:30,755 --> 00:10:33,215
Tha Carter.
Yeah, yeah, yeah.

243
00:10:33,341 --> 00:10:34,633
Yeah, I'm coming to y'all

244
00:10:34,759 --> 00:10:37,136
straight from Rikers
penitentiary监狱, bitch.

245
00:10:37,262 --> 00:10:38,595
You heard, man?

246
00:10:38,722 --> 00:10:40,014
Hey, yo, man-

247
00:10:40,140 --> 00:10:41,598
haters out there
think I'm playing,

248
00:10:41,725 --> 00:10:44,435
think I'm some
kind of joke, man.

249
00:10:44,561 --> 00:10:46,145
Young money.

250
00:10:46,271 --> 00:10:47,521
Check this out
right here, man.

251
00:10:49,441 --> 00:10:50,816
I'm the baddest
motherfucker

252
00:10:50,942 --> 00:10:54,028
on this whole prison here,
bitch, you heard?

253
00:10:54,154 --> 00:10:56,238
Check it out, let's go,
I'm the baddest dude-

254
00:10:56,364 --> 00:10:59,366
aah!

255
00:11:04,247 --> 00:11:06,457
- What can I say
about ancestry祖先. Com?

256
00:11:06,583 --> 00:11:08,167
My adventure began
when I received a leaf

257
00:11:08,293 --> 00:11:11,462
pointing me in the direction
of my great grandfather,

258
00:11:11,588 --> 00:11:14,798
who was a pilot领航员 with
the flying aces in World War I.

259
00:11:14,924 --> 00:11:17,384
- On my mother's side,
I found an aunt阿姨 who traced

260
00:11:17,510 --> 00:11:21,555
her lineage血统 all the way
back to Thomas Jefferson.

261
00:11:21,681 --> 00:11:23,557
- I was able to trace
my bloodline back to nobles贵族,

262
00:11:23,683 --> 00:11:25,684
and even a king.

263
00:11:25,810 --> 00:11:28,771
- I discovered I'm a direct
descendent后裔 of Erik the Red.

264
00:11:28,897 --> 00:11:31,106
- After only a couple of hours
on ancestry. Com,

265
00:11:31,232 --> 00:11:33,150
I was able to trace
my family line

266
00:11:33,276 --> 00:11:35,986
all the way back to none other
than our third president,

267
00:11:36,112 --> 00:11:38,197
Thomas Jefferson.

268
00:11:38,323 --> 00:11:39,907
- Marie Antoinette.

269
00:11:40,033 --> 00:11:43,077
- Thomas Jefferson.
- Aristotle.

270
00:11:43,203 --> 00:11:45,746
- Thomas Jefferson.
- Alexander the Great.

271
00:11:45,872 --> 00:11:49,083
- Thomas motherfucking
Jefferson.

272
00:11:49,209 --> 00:11:51,877
- Join ancestry.com and begin
a one-of-a-kind journey

273
00:11:52,003 --> 00:11:53,796
into your unique past.

274
00:11:53,922 --> 00:11:56,632
Because you never know
where your story begins.

275
00:11:56,758 --> 00:11:58,926
- Thomas Jefferson.

276
00:12:03,807 --> 00:12:05,474
- So Jordan
does not drive,

277
00:12:05,600 --> 00:12:08,227
which I think
is completely crazy.

278
00:12:08,353 --> 00:12:09,937
- That's true.
I can explain.

279
00:12:10,063 --> 00:12:12,981
I'm from New York originally,
first of all, so that-

280
00:12:13,108 --> 00:12:14,316
yeah, we don't drive.
We don't drive in New York.

281
00:12:14,442 --> 00:12:15,526
- And I'm from
the polar极 opposite.

282
00:12:15,652 --> 00:12:17,277
I'm from the motor city.

283
00:12:17,404 --> 00:12:19,113
I'm from Detroit,
and so you have to drive

284
00:12:19,239 --> 00:12:24,076
or you get stabbed刺伤.
- Yeah, so...

285
00:12:24,202 --> 00:12:26,203
but that's just it.
You just landed on it.

286
00:12:26,329 --> 00:12:27,663
That's why I don't drive.

287
00:12:27,789 --> 00:12:29,164
Is because people
go crazy, man.

288
00:12:29,290 --> 00:12:30,999
People turn into Mr. Hyde.

289
00:12:31,126 --> 00:12:32,543
- See, that is not true
that everybody

290
00:12:32,669 --> 00:12:35,462
who gets behind the wheel
of a car flips out.

291
00:12:35,588 --> 00:12:36,880
- You do.
- No, I don't.

292
00:12:37,006 --> 00:12:38,882
I do not.
When have I done that?

293
00:12:39,008 --> 00:12:41,677
- When that one lady
cut us off that one time.

294
00:12:41,803 --> 00:12:43,303
- Okay.
- My man goes into top speed-

295
00:12:43,430 --> 00:12:44,680
- I did.
- You started swerving

296
00:12:44,806 --> 00:12:46,432
in between vehicles.
- It's true.

297
00:12:46,558 --> 00:12:48,350
I did- I swung between-
- He had to catch up with her.

298
00:12:48,476 --> 00:12:49,726
- You're right, you're right.

299
00:12:49,853 --> 00:12:51,478
- And when we caught up
with her,

300
00:12:51,604 --> 00:12:53,272
my man rolls down his window.
What did you yell喊叫 at her?

301
00:12:53,398 --> 00:12:58,360
- I said, "Selfish!"
- "Selfish."

302
00:12:58,486 --> 00:13:01,321
- "You are selfish!"

303
00:13:01,448 --> 00:13:04,324
- And please, no,
you have to understand-

304
00:13:04,451 --> 00:13:06,034
you have to understand.

305
00:13:06,161 --> 00:13:08,078
That's the nicest guy
in the world, right here, okay?

306
00:13:08,204 --> 00:13:11,206
To him, "selfish,"
that's the "C" word.

307
00:13:11,332 --> 00:13:13,000
- It is.

308
00:13:13,126 --> 00:13:14,168
- I love that that
is your version of road rage,

309
00:13:14,294 --> 00:13:15,919
by the way.

310
00:13:16,045 --> 00:13:17,171
- So you think if you got
a driver's license,

311
00:13:17,297 --> 00:13:18,505
that you would
just flip out翻转出来.

312
00:13:18,631 --> 00:13:20,215
- No, that's other people.

313
00:13:20,341 --> 00:13:21,800
L-I don't drive
because I smoke weed

314
00:13:21,926 --> 00:13:23,760
and I don't want
to kill anybody.

315
00:13:23,887 --> 00:13:26,263
- Oh, okay. Okay.

316
00:13:26,389 --> 00:13:29,558
Well, Jordan, you could always
just stop smoking weed.

317
00:13:35,148 --> 00:13:36,440
- I'm sorry.
What'd you say?

318
00:13:36,566 --> 00:13:39,651
I'm high right now.

319
00:13:39,777 --> 00:13:41,820
- I'm telling you,
it's totally legal合法的.

320
00:13:41,946 --> 00:13:43,447
These doctors,
they'll prescribe开药方

321
00:13:43,573 --> 00:13:45,574
medical marijuana大麻
to anyone, man.

322
00:13:45,700 --> 00:13:47,242
- What do I tell them
I need it for?

323
00:13:47,368 --> 00:13:48,994
- It doesn't matter.

324
00:13:49,120 --> 00:13:50,787
They want to give you
a prescription处方.

325
00:13:50,914 --> 00:13:52,581
It's how they get paid.

326
00:13:52,707 --> 00:13:55,584
Just make something up.

327
00:13:55,710 --> 00:13:56,752
- Hey, Mr. Washington,
welcome.

328
00:13:56,878 --> 00:13:58,587
- Hey, doctor.

329
00:13:58,713 --> 00:13:59,755
- All right, I'm going
to ask you a couple questions,

330
00:13:59,881 --> 00:14:00,923
and then we're-
- AIDS.

331
00:14:01,049 --> 00:14:04,551
- What?
- AIDS.

332
00:14:04,677 --> 00:14:07,679
- AIDS?
As in, uh-

333
00:14:07,805 --> 00:14:11,683
- As in I got it, need lots
of weed to get rid of it.

334
00:14:11,809 --> 00:14:13,852
- Oh, okay, AIDS.

335
00:14:13,978 --> 00:14:15,812
Um, wow.

336
00:14:15,939 --> 00:14:17,731
All right, are you sure
you're not suffering

337
00:14:17,857 --> 00:14:20,526
from anything else like,
you know, back pain?

338
00:14:20,652 --> 00:14:22,319
- Nope.
- Anxiety焦虑?

339
00:14:22,445 --> 00:14:24,738
- Cool as a cucumber黄瓜.
- How about insomnia失眠?

340
00:14:24,864 --> 00:14:28,116
- Sleep like a baby...
with AIDS.

341
00:14:28,243 --> 00:14:29,701
- Okay, Mr. Washington.

342
00:14:29,827 --> 00:14:31,286
Let's just slow your roll
for a second here.

343
00:14:31,412 --> 00:14:33,121
If you had AIDS,
then I would have to verify it

344
00:14:33,248 --> 00:14:34,957
by seeing test results.

345
00:14:35,083 --> 00:14:37,709
Whereas if you have back pain
or anxiety or anything else

346
00:14:37,835 --> 00:14:39,878
that I can't test for,
then I can give you

347
00:14:40,004 --> 00:14:42,381
this prescription
for cannabis大麻 right now.

348
00:14:42,507 --> 00:14:43,632
- Oh.
- Understand?

349
00:14:43,758 --> 00:14:45,050
- Oh, I see, I get it.
- Awesome.

350
00:14:45,176 --> 00:14:46,760
- My bad.
- Good, good, good.

351
00:14:46,886 --> 00:14:48,845
- Okay, so-
- Leprosy麻风病.

352
00:14:48,972 --> 00:14:50,055
- No, no.

353
00:14:50,181 --> 00:14:51,348
Because if you had leprosy,

354
00:14:51,474 --> 00:14:52,516
I'd have to quarantine隔离 you.

355
00:14:52,642 --> 00:14:54,142
- Ouch.
- What are you-no.

356
00:14:54,269 --> 00:14:56,061
Okay, I see
your finger's falling off.

357
00:14:56,187 --> 00:14:58,313
No, don't do that, though.
That's not going to work.

358
00:14:58,439 --> 00:14:59,606
- Scurvy坏血病.
- Nope.

359
00:14:59,732 --> 00:15:01,024
- Rickets软骨病.
- What? No.

360
00:15:01,150 --> 00:15:02,693
- Consumption结核病.
- No, Mr. Washington.

361
00:15:02,819 --> 00:15:04,111
Something from this century.

362
00:15:04,237 --> 00:15:05,696
- Schizophrenia精神分裂.

363
00:15:05,822 --> 00:15:06,905
You don't have schizophrenia.

364
00:15:07,031 --> 00:15:08,282
Yes, you do.
- Keep it simple.

365
00:15:08,408 --> 00:15:10,200
Please.

366
00:15:10,326 --> 00:15:12,286
What the hell is that?
I don't know what that is.

367
00:15:12,412 --> 00:15:13,829
- There's a fish hook
in my lip嘴唇.

368
00:15:13,955 --> 00:15:15,247
- Just pick something
off this list.

369
00:15:15,373 --> 00:15:17,124
- I can't reach it.
- Why not?

370
00:15:17,250 --> 00:15:19,585
- Paralyzed麻痹的.

371
00:15:19,711 --> 00:15:21,628
- Does your face hurt,
Mr. Washington?

372
00:15:21,754 --> 00:15:22,796
- Yeah.

373
00:15:22,922 --> 00:15:24,840
- Then this should help.

374
00:15:27,969 --> 00:15:30,679
- Lil Wayne, y'all.

375
00:15:30,805 --> 00:15:33,974
Young money.

376
00:15:34,100 --> 00:15:37,060
Y'all need to watch
your back, you heard?

377
00:15:37,186 --> 00:15:38,604
That's right,
yeah, yeah, yeah.

378
00:15:38,730 --> 00:15:40,355
Because I'm coming
for y'all, you know?

379
00:15:40,481 --> 00:15:42,441
What's up, man?
I run this, y'all.

380
00:15:42,567 --> 00:15:43,859
In fact, yo,
let's go.

381
00:15:43,985 --> 00:15:45,152
I'm the baddest-

382
00:15:45,278 --> 00:15:46,820
where you going, man?
Where you going?

383
00:15:46,946 --> 00:15:47,988
Hey, man, where you going?
You ain't got to go-

384
00:15:58,875 --> 00:16:00,208
Okay, I see.

385
00:16:00,335 --> 00:16:04,338
Pick on the little guy, huh?
Punks废物.

386
00:16:04,464 --> 00:16:05,964
Aah!

387
00:16:06,090 --> 00:16:09,301
This little ironic讽刺的
motherfucker right here.

388
00:16:14,015 --> 00:16:15,140
- So I had
this friend in grade school小学,

389
00:16:15,266 --> 00:16:16,850
and we were really tight.

390
00:16:16,976 --> 00:16:18,977
And then we grew up and we went
to different high schools,

391
00:16:19,103 --> 00:16:20,812
and we lost touch
with each other.

392
00:16:20,938 --> 00:16:24,900
And, uh,
yeah, it's already funny.

393
00:16:25,026 --> 00:16:26,735
And so he said to me-

394
00:16:26,861 --> 00:16:28,570
he calls me
out of the blue one day.

395
00:16:28,696 --> 00:16:30,030
- This is my jam.

396
00:16:30,156 --> 00:16:31,948
- And we're talking
on the phone,

397
00:16:32,075 --> 00:16:34,993
and he's asking me about
this girl in my high school.

398
00:16:35,119 --> 00:16:37,954
And he says, "So, man,
what you think of that girl?"

399
00:16:38,081 --> 00:16:39,998
And so I told him what I
thought, I told him the truth,

400
00:16:40,124 --> 00:16:45,504
and I said, "Oh, dog, man,
my girl is not attractive.

401
00:16:45,630 --> 00:16:48,465
"My girl is, like,
rough in this area, you know?

402
00:16:48,591 --> 00:16:51,343
My girl is straight hit,"
you know?

403
00:16:51,469 --> 00:16:54,930
"Nice enough girl,
but she is ugly," you know?

404
00:16:55,056 --> 00:16:56,181
And my man goes like this.

405
00:16:56,307 --> 00:16:57,724
My man goes like this.
He says-

406
00:16:57,850 --> 00:17:00,936
he says, "Ah, dog,
that's my girlfriend."

407
00:17:01,062 --> 00:17:02,896
- And so I literally逐字地
go like this. I go...

408
00:17:17,662 --> 00:17:20,956
"Brother,
you know I'm playing."

409
00:17:24,961 --> 00:17:27,087
"She fine as hell."

410
00:17:37,765 --> 00:17:40,851
- I don't think we should
be doing this right now, man.

411
00:17:40,977 --> 00:17:42,894
Young money.

412
00:17:43,020 --> 00:17:44,187
You know what I mean?

413
00:17:44,313 --> 00:17:45,731
There's a time
and a place-

414
00:17:45,857 --> 00:17:50,193
you know what I'm saying,
man?

415
00:17:50,319 --> 00:17:54,114
Man, I ain't scared, yo.

416
00:18:12,800 --> 00:18:15,218
- Good evening,
my fellow Americans.

417
00:18:15,344 --> 00:18:17,929
Now, before I begin,
I just want to say

418
00:18:18,055 --> 00:18:19,681
that I know a lot of people
out there seem to think

419
00:18:19,807 --> 00:18:22,434
that I don't get angry.

420
00:18:22,560 --> 00:18:23,727
Well,
that's just not true.

421
00:18:23,853 --> 00:18:25,562
I get angry a lot.

422
00:18:25,688 --> 00:18:27,731
It's just that the way
I express passion热情

423
00:18:27,857 --> 00:18:31,193
is different from most.

424
00:18:31,319 --> 00:18:33,028
So just so there's
no more confusion,

425
00:18:33,154 --> 00:18:35,322
we've hired Luther here
to be my anger translator.

426
00:18:35,448 --> 00:18:37,532
Luther?
- Hi.

427
00:18:37,658 --> 00:18:39,493
- First off, concerning
the recent developments

428
00:18:39,619 --> 00:18:42,621
in the middle-eastern region,
I just want to reiterate重申

429
00:18:42,747 --> 00:18:44,664
our unflinching无所畏惧的 support
for all people

430
00:18:44,791 --> 00:18:46,750
and their right
to a democratic民主的 process.

431
00:18:46,876 --> 00:18:48,710
- Hey, all y'all dictators独裁者
out there,

432
00:18:48,836 --> 00:18:50,295
keep messing around胡闹
and see what happens.

433
00:18:50,421 --> 00:18:52,672
Just see what happens.
Watch.

434
00:18:52,799 --> 00:18:56,968
- Also, to the governments
of Iran and North Korea,

435
00:18:57,094 --> 00:18:59,596
we once again urge you
to discontinue

436
00:18:59,722 --> 00:19:01,264
your uranium铀 enrichment program.

437
00:19:01,390 --> 00:19:03,058
- Hey, Mahmoud?
Kim Jong?

438
00:19:03,184 --> 00:19:05,018
I think I already
done told both y'all,

439
00:19:05,144 --> 00:19:06,895
86 your shit, bitches,

440
00:19:07,021 --> 00:19:09,606
or I'm going to come over there
and do it for y'all.

441
00:19:09,732 --> 00:19:12,025
Please test me
and see what happens.

442
00:19:12,151 --> 00:19:16,029
- On the domestic国内的 front, I just
want to say to my critics批评家,

443
00:19:16,155 --> 00:19:19,032
I hear your voices and
I'm aware of your concerns.

444
00:19:19,158 --> 00:19:20,575
- So maybe if you could
chill使变冷 the hell out

445
00:19:20,701 --> 00:19:22,327
for, like, a second,
then maybe I could focus

446
00:19:22,453 --> 00:19:24,412
on some shit, you know?

447
00:19:24,539 --> 00:19:25,789
- And that goes for everybody,

448
00:19:25,915 --> 00:19:27,541
including members
of the Tea Party.

449
00:19:27,667 --> 00:19:28,708
- Oh, don't even get me started
on these motherfuckers

450
00:19:28,835 --> 00:19:30,168
right here.

451
00:19:30,294 --> 00:19:31,962
- I want to assure you
that we will be looking

452
00:19:32,088 --> 00:19:35,131
for new compromises折中方法 with
the GOP in the months ahead.

453
00:19:35,258 --> 00:19:36,508
- And you know these
motherfuckers gonna say no

454
00:19:36,634 --> 00:19:38,426
before I even suggest
some shit.

455
00:19:38,553 --> 00:19:42,806
- Now I know a lot of folks say
that I haven't done a good job

456
00:19:42,932 --> 00:19:45,642
at communicating my
accomplishments造诣 to the public.

457
00:19:45,768 --> 00:19:48,311
- Because y'all
motherfuckers don't listen!

458
00:19:48,437 --> 00:19:51,064
- Uh, since being in office,

459
00:19:51,190 --> 00:19:53,149
we've created
3 million new jobs.

460
00:19:53,276 --> 00:19:55,151
- 3 million new jobs.

461
00:19:55,278 --> 00:19:57,237
- We ended the war in Iraq.
- Ended a war, y'all.

462
00:19:57,363 --> 00:19:59,322
We ended a war,
remember that?

463
00:19:59,448 --> 00:20:01,783
- These achievements
should serve as a reminder

464
00:20:01,909 --> 00:20:03,285
that I am on your side.

465
00:20:03,411 --> 00:20:05,745
- I am not a Muslim!

466
00:20:05,872 --> 00:20:08,039
- And that my intentions目的,
as your president,

467
00:20:08,165 --> 00:20:10,458
are coming from
the right place.

468
00:20:10,585 --> 00:20:12,502
- They coming from Hawaii,
which is where I'm from,

469
00:20:12,628 --> 00:20:14,504
which is in the United States
of America, y'all.

470
00:20:14,630 --> 00:20:16,590
Okay?
This is ridiculous.

471
00:20:16,716 --> 00:20:20,010
I have a birth certificate!
I have a birth certificate!

472
00:20:20,136 --> 00:20:22,721
I have a hot,
diggity-doggity,

473
00:20:22,847 --> 00:20:24,931
mamase mamasa mamakusa,
birth certificate,

474
00:20:25,057 --> 00:20:26,433
you dumbass crackers!

475
00:20:26,559 --> 00:20:29,060
- Okay, Luther.
Rope it in.

476
00:20:29,186 --> 00:20:30,562
- Yeah, dial it back, Luther.
Damn.

477
00:20:30,688 --> 00:20:33,398
- In conclusion,
last night I had

478
00:20:33,524 --> 00:20:34,566
a conversation
with Michelle.

479
00:20:34,692 --> 00:20:36,151
- I says, "Bitch."

480
00:20:36,277 --> 00:20:37,319
- Nope,
I did not say that.

481
00:20:37,445 --> 00:20:39,821
I did not say that.

482
00:20:42,158 --> 00:20:44,784
- So, uh, we are going to be
saying good night right now,

483
00:20:44,911 --> 00:20:47,120
but before we do,
I just want say that we are-

484
00:20:47,246 --> 00:20:49,748
I do think we're lucky.

485
00:20:49,874 --> 00:20:51,499
In the lottery抽彩给奖法 of life,
I think we are lucky

486
00:20:51,626 --> 00:20:54,127
to have gotten
the best quality of both races.

487
00:20:54,253 --> 00:20:56,129
- I think so too.
- That we are a part of.

488
00:20:56,255 --> 00:20:57,547
No, it is.
- I think so too.

489
00:20:57,673 --> 00:20:59,424
- No, because I know
there are two dudes

490
00:20:59,550 --> 00:21:01,718
out there somewhere right now
with little dicks

491
00:21:01,844 --> 00:21:04,596
and sickle cell.
- Yeah, it's true.

492
00:21:04,722 --> 00:21:07,807
Good night.
- Thank you.

493
00:21:12,271 --> 00:21:15,148
- ♪ I'm gonna do
my one line here ♪

494
00:21:15,274 --> 00:21:17,275
- Oh, yeah.

495
00:21:20,279 --> 00:21:21,738
- When did Asian guys
become fucking cool?

496
00:21:21,864 --> 00:21:23,823
I don't understand that.

497
00:21:23,950 --> 00:21:26,785
Y'all stole break dancing.
- When did that happen?

498
00:21:26,911 --> 00:21:28,370
I just figured
you would know-

499
00:21:28,496 --> 00:21:30,163
you don't know the answer?
- It's not fair.

500
00:21:30,289 --> 00:21:31,998
- They just straight up
stole it.

501
00:21:32,124 --> 00:21:34,292
- "Y'all been trying to perfect
this art?" Backflips反向翻转.

