﻿1
00:00:01,001 --> 00:00:03,711
- ♪ When I roll down the street
brothers all take a knee ♪

2
00:00:03,837 --> 00:00:05,880
♪ Honeys dropping their Gs,
wanting pieces of me ♪

3
00:00:06,006 --> 00:00:07,382
♪ 'Cause I run
this whole block ♪

4
00:00:07,508 --> 00:00:08,925
♪ With these rhymes押韵
and this glock格洛克 ♪

5
00:00:09,051 --> 00:00:10,760
♪ Make this whole
damn 'hood头巾 tick ♪

6
00:00:10,886 --> 00:00:13,096
♪ This my town, I'm the talk,
cock公鸡 of the walk ♪

7
00:00:13,222 --> 00:00:15,515
♪ I'm the boss and the king,
niggas kissing my ass ♪

8
00:00:15,641 --> 00:00:17,183
♪ And bitches
kissing my ring ♪

9
00:00:17,309 --> 00:00:19,894
What's up, yeah!
Oh, shit, nigga.

10
00:00:20,020 --> 00:00:21,270
You know it's true.

11
00:00:21,397 --> 00:00:23,606
Yeah.
Y-

12
00:00:33,075 --> 00:00:36,035
- I'm the leader
of the free world.

13
00:00:45,129 --> 00:00:47,630
- I mean, the president,
how you-?

14
00:00:47,756 --> 00:00:49,424
Forget y'all.

15
00:01:13,741 --> 00:01:14,782
- Hey.

16
00:01:14,908 --> 00:01:17,952
- Wow.
- Pretty good.

17
00:01:18,078 --> 00:01:20,246
Really?

18
00:01:20,372 --> 00:01:22,915
- Welcome to the show.
I am Keegan.

19
00:01:23,041 --> 00:01:24,751
- And I am Jordan.
- And this is Key and Peele.

20
00:01:24,877 --> 00:01:28,045
- Yes, welcome.
- Thank you for coming.

21
00:01:28,172 --> 00:01:29,213
- Keegan and I
both have white moms.

22
00:01:29,339 --> 00:01:30,590
- Yes, yes.
- This is true.

23
00:01:30,716 --> 00:01:32,383
We have white moms,
and, uh, the thing

24
00:01:32,509 --> 00:01:34,594
about having a white mom
and being a black guy is,

25
00:01:34,720 --> 00:01:38,055
as a kid, a white mom
can't hit a black kid in public.

26
00:01:38,182 --> 00:01:40,558
- Can't do it.
Can't do it.

27
00:01:40,684 --> 00:01:43,644
Gets too racial种族的 too fast.
- That's right.

28
00:01:43,771 --> 00:01:46,689
It escalates逐步升级. It escalates to
this racial thing, and I was-

29
00:01:46,815 --> 00:01:48,941
I became aware of this
at a very young age.

30
00:01:49,067 --> 00:01:51,861
I think my mother spanked用手掌打 me
once in the grocery杂货店 store.

31
00:01:51,987 --> 00:01:54,155
I swear to God, Keegan, l-
you know that tear

32
00:01:54,281 --> 00:01:56,449
that Denzel Washington
does in Glory...

33
00:01:56,575 --> 00:01:58,576
- Yeah.
- When he's getting whipped鞭打?

34
00:01:58,702 --> 00:02:00,036
- Yeah, yeah.
- That's what I did, and l-

35
00:02:00,162 --> 00:02:01,662
I promise you,
just instinctively本能地,

36
00:02:01,789 --> 00:02:03,706
it came out of me.
This is what I did.

37
00:02:03,832 --> 00:02:05,666
Just hit me like you're my mom
in the grocery store.

38
00:02:05,793 --> 00:02:07,502
I said...
♪ Mama, don't you hit ♪

39
00:02:07,628 --> 00:02:11,631
♪ No black boy's booty赃物 ♪

40
00:02:14,259 --> 00:02:18,262
♪ 200 years,
ain't nothing changed ♪

41
00:02:23,185 --> 00:02:24,894
Mr. Louis...

42
00:02:25,020 --> 00:02:27,063
Thank you
for coming in

43
00:02:27,189 --> 00:02:28,940
with your associates伙伴.

44
00:02:29,066 --> 00:02:32,568
I know this is hard to hear,
but...

45
00:02:32,694 --> 00:02:34,153
we need to make
some serious decisions

46
00:02:34,279 --> 00:02:36,197
about your mother's health.
Let's be honest.

47
00:02:36,323 --> 00:02:38,074
She is getting on
in years.

48
00:02:38,200 --> 00:02:40,660
- Oh, snap突然的.

49
00:02:40,786 --> 00:02:43,079
Okay, I see how it is.
I see how it is.

50
00:02:43,205 --> 00:02:47,500
Well, yo mama so old,
her last name is "osaurus."

51
00:02:47,626 --> 00:02:52,004
- Boom!

52
00:02:52,130 --> 00:02:55,049
- N-no, Mr. Louis,
it wasn't an insult冒犯.

53
00:02:55,175 --> 00:02:57,510
L-I was just saying
that your mother's condition

54
00:02:57,636 --> 00:03:00,179
is deteriorating恶化, uh,
because she's getting older.

55
00:03:00,305 --> 00:03:03,975
- Oh, see that's cold, though.
That's a cold one, okay.

56
00:03:04,101 --> 00:03:06,269
Okay, I see how it is.
Hey, y'all.

57
00:03:06,395 --> 00:03:09,981
Yo mama so old,
in her history class,

58
00:03:10,107 --> 00:03:11,440
they just wrote down
what they was doing.

59
00:03:13,527 --> 00:03:15,570
He's talking about-
he's talking about-

60
00:03:15,696 --> 00:03:17,446
ch-ch-ch-ch-ch-
ch-ch-ch-ch-ch-ch-ch-ch-ch-ch-

61
00:03:17,573 --> 00:03:18,990
- That's you!

62
00:03:19,116 --> 00:03:20,992
- Mr. Louis,
this isn't an insult contest竞赛.

63
00:03:21,118 --> 00:03:22,493
Not only
is your mother elderly,

64
00:03:22,619 --> 00:03:24,829
but also her ability
to walk

65
00:03:24,955 --> 00:03:26,956
is currently being affected
by her weight.

66
00:03:27,082 --> 00:03:28,416
- Oh.
- Ooh.

67
00:03:28,542 --> 00:03:30,835
- See, okay.
It just got real.

68
00:03:30,961 --> 00:03:34,088
- No, I'm not insulting her.
I'm trying to tell you the-

69
00:03:34,214 --> 00:03:37,300
- Yo mama is so fat, when
she go to the movie theater,

70
00:03:37,426 --> 00:03:39,385
bitch sitting
next to everyone.

71
00:03:39,511 --> 00:03:43,055
- Excuse me. Excuse me.
- Listen to me, okay?

72
00:03:43,181 --> 00:03:45,558
Your mother needs
to manage her weight,

73
00:03:45,684 --> 00:03:47,101
or there can be
some real problems.

74
00:03:47,227 --> 00:03:49,562
- Oh... kay!

75
00:03:49,688 --> 00:03:51,772
All right.
Oh, yeah.

76
00:03:51,899 --> 00:03:55,276
Yo mama is so fat, she need
a latitude and longitude number

77
00:03:55,402 --> 00:03:57,528
to find
her own asshole.

78
00:03:57,654 --> 00:03:59,280
- Oh!

79
00:03:59,406 --> 00:04:02,783
- Look at his face! Yeah!
Look at his face!

80
00:04:02,910 --> 00:04:04,327
- Ah! Ah!
- Give it to him.

81
00:04:04,453 --> 00:04:05,703
- Oh.
- Give me some.

82
00:04:05,829 --> 00:04:07,914
- It's coordinates, son.
Coordinates.

83
00:04:08,040 --> 00:04:09,582
Now who's the doctor?

84
00:04:09,708 --> 00:04:12,001
- I am the doctor.
Mr. Louis, I am.

85
00:04:12,127 --> 00:04:14,295
And it is my job to make sure
that your mother

86
00:04:14,421 --> 00:04:16,047
is getting the care
that she needs.

87
00:04:16,173 --> 00:04:17,214
We are talking about the woman
who took care

88
00:04:17,341 --> 00:04:18,633
of you your whole life.

89
00:04:18,759 --> 00:04:20,259
The least you can do
is take care of her

90
00:04:20,385 --> 00:04:22,720
in her old age
and take this seriously!

91
00:04:28,769 --> 00:04:30,019
- I'm sorry.

92
00:04:30,145 --> 00:04:32,396
- I'm sorry
that I lost my temper.

93
00:04:32,522 --> 00:04:35,274
- No, really, doctor.

94
00:04:35,400 --> 00:04:38,027
I guess...
I was just joking around

95
00:04:38,153 --> 00:04:41,656
because I know her condition
is really serious.

96
00:04:41,782 --> 00:04:45,493
And... I don't know,
I guess humor

97
00:04:45,619 --> 00:04:48,913
is the only way
I can really deal with it, uh-

98
00:04:50,999 --> 00:04:53,084
I just know
that she's said so many times

99
00:04:53,210 --> 00:04:55,711
she doesn't want
anything invasive扩散性的 done.

100
00:04:55,837 --> 00:04:58,965
- Really?
I was not aware of that.

101
00:04:59,091 --> 00:05:00,883
Um, that's interesting,
because she did not mention that

102
00:05:01,009 --> 00:05:02,510
to me in our
previous conversation.

103
00:05:02,636 --> 00:05:05,513
'Course it was difficult
to hear her

104
00:05:05,639 --> 00:05:10,226
with my dick
in her mouth.

105
00:05:18,068 --> 00:05:20,820
Snap, Mr. Louis.

106
00:05:20,946 --> 00:05:23,155
Oh, snap.

107
00:05:23,281 --> 00:05:24,907
Now let's talk
about the procedure.

108
00:05:26,034 --> 00:05:27,535
- Hello,
everybody out there.

109
00:05:27,661 --> 00:05:29,412
This is Brock Favors
with traffic on the ones.

110
00:05:29,538 --> 00:05:31,372
Chad Armstrong
is out sick today,

111
00:05:31,498 --> 00:05:33,916
so I am filling in代课
from my usual land reports

112
00:05:34,042 --> 00:05:36,293
and, uh, I'm up here
in the chopper直升机.

113
00:05:36,420 --> 00:05:39,046
But I gotta tell you guys,
I am loving the view.

114
00:05:39,172 --> 00:05:41,382
Oh, shit!

115
00:05:41,508 --> 00:05:42,925
Oh, my god!

116
00:05:43,051 --> 00:05:45,386
Oh, oh, no!
Stop, stop, stop, stop.

117
00:05:45,512 --> 00:05:48,639
Oh!

118
00:05:48,765 --> 00:05:50,516
Whew.

119
00:05:50,642 --> 00:05:52,351
All right.

120
00:05:52,477 --> 00:05:54,603
Well, we are,
um, uh-

121
00:05:54,730 --> 00:05:56,272
we are over the 10.

122
00:05:56,398 --> 00:05:58,149
It is massively可观的 clogged堵塞的
down there,

123
00:05:58,275 --> 00:06:01,152
like a pint品脱 of maple枫树 syrup糖浆
on a cool November morning.

124
00:06:01,278 --> 00:06:03,904
And we just-oh, shit!
Motherfucker, god damn!

125
00:06:04,031 --> 00:06:06,574
We gon' die
in this motherfucker!

126
00:06:06,700 --> 00:06:08,993
Oh, shit!
Oh, shit!

127
00:06:09,119 --> 00:06:10,870
Oh, my god!

128
00:06:10,996 --> 00:06:16,709
Whew, whew, whew!

129
00:06:16,835 --> 00:06:18,502
Okay.

130
00:06:18,628 --> 00:06:22,465
Okay, I am, uh-
I am very sorry, folks.

131
00:06:22,591 --> 00:06:25,468
It's a little bit
of a bumpy颠簸的 ride up here.

132
00:06:25,594 --> 00:06:28,471
We are now approaching
the 405,

133
00:06:28,597 --> 00:06:31,640
ah, where the left lane车道
is blocked by a mattress床垫.

134
00:06:31,767 --> 00:06:34,018
So somebody is,
uh, gonna be doing

135
00:06:34,144 --> 00:06:36,604
a little return to Ikea宜家 later
today, you know what I'm say-

136
00:06:36,730 --> 00:06:40,149
Oh, no, nigga, please!
Shit! Oh, god damn!

137
00:06:40,275 --> 00:06:41,984
Oh, get me out this
motherfucking death machine

138
00:06:42,110 --> 00:06:43,778
right now!
Oh, no!

139
00:06:43,904 --> 00:06:45,780
Black people ain't meant
to be in the sky!

140
00:06:45,906 --> 00:06:47,948
We ain't meant to be
in the sky, man!

141
00:06:48,075 --> 00:06:51,660
Oh, mama, help me.

142
00:06:51,787 --> 00:06:53,746
Motherfucker.

143
00:06:53,872 --> 00:06:57,458
Oh, mama!
Aah! Oh, shit!

144
00:06:57,584 --> 00:07:00,586
Well, the 405
is fucked up right now.

145
00:07:00,712 --> 00:07:03,672
Ain't nobody going down
this shit. I'm sorry.

146
00:07:03,799 --> 00:07:09,470
I'm sorry.
I'm very sorry, folks.

147
00:07:09,596 --> 00:07:10,930
I'm very sorry
about that,

148
00:07:11,056 --> 00:07:12,765
but I am losing
my shit up here.

149
00:07:12,891 --> 00:07:15,184
- Actually,
you have every right to.

150
00:07:15,310 --> 00:07:16,727
We're about to crash.

151
00:07:16,853 --> 00:07:18,104
- Oh, fuck,
help me!

152
00:07:18,230 --> 00:07:19,438
Shit!
Oh, my god, help!

153
00:07:19,564 --> 00:07:23,651
Mama, please!
Mama! Help!

154
00:07:33,411 --> 00:07:34,870
- So-
so here's something.

155
00:07:34,996 --> 00:07:38,833
Jordan and I love
to watch white guys fight.

156
00:07:38,959 --> 00:07:40,835
- Yes, favorite pastime消遣.
- It's sublime超群的, mm-hmm.

157
00:07:40,961 --> 00:07:42,670
- 2:00 a.m.
Outside of a bar,

158
00:07:42,796 --> 00:07:45,548
any night, any morning,
you can see this, and-

159
00:07:45,674 --> 00:07:47,258
- It's the great-it's-
- Best thing in the world.

160
00:07:47,384 --> 00:07:49,260
- It's like watching Nat Geo国家地理屏道.
- Oh, it's-

161
00:07:49,386 --> 00:07:51,053
- It's the Discovery Channel
for us.

162
00:07:51,179 --> 00:07:52,638
It's Animal Planet.

163
00:07:52,764 --> 00:07:54,723
Well, this is how it happens.
- It's so great.

164
00:07:54,850 --> 00:07:57,059
- White guys-there's-
it starts with phase one.

165
00:07:57,185 --> 00:07:58,978
- Phase one, which is the, uh-
the arm-back phase.

166
00:07:59,104 --> 00:08:00,896
- Hands back, just completely-
- And chest胸部 out and arm back.

167
00:08:01,022 --> 00:08:02,523
- Chest out, arms back.
- And it's lots of,

168
00:08:02,649 --> 00:08:04,108
"You wanna do something?
Let's do something then."

169
00:08:04,234 --> 00:08:05,693
- "What are you doing?
What are you doing, bro?"

170
00:08:05,819 --> 00:08:07,611
- "What are you doing?"
- "You wanna do something?"

171
00:08:07,737 --> 00:08:09,446
- "I'm already doing something."
- I need to keep my arms back

172
00:08:09,573 --> 00:08:12,575
as if they're so dangerous,
I gotta keep them back here.

173
00:08:12,701 --> 00:08:14,910
- Yo, phase two.
- Phase two.

174
00:08:15,036 --> 00:08:16,412
- They l-
they lock horns喇叭.

175
00:08:16,538 --> 00:08:18,205
- "You w-let's go.
Let go of me, dude."

176
00:08:18,331 --> 00:08:19,582
"Let go of me right now."
- "Let go of my shirt."

177
00:08:19,708 --> 00:08:21,542
- "Let go of my shirt.
I'm gonna-"

178
00:08:21,668 --> 00:08:23,335
- "I'm not on your shirt."
- "You're on my shirt, dude.

179
00:08:23,461 --> 00:08:26,213
"Get off of my shirt, man.
Get off my shirt.

180
00:08:26,339 --> 00:08:27,631
Get off of my shirt."
- "Dude."

181
00:08:27,757 --> 00:08:29,925
- "Let go of my shirt."

182
00:08:30,051 --> 00:08:31,719
- Two minutes later.
- Two minutes later.

183
00:08:31,845 --> 00:08:33,846
"I fucking love you!"
- "You're my best friend, dog."

184
00:08:33,972 --> 00:08:35,764
- "You are my brother, dude.
Do you know that?"

185
00:08:35,891 --> 00:08:36,932
- "I would never do
anything to you."

186
00:08:37,058 --> 00:08:38,100
- "I would never do that
to you."

187
00:08:38,226 --> 00:08:39,476
- Fast-forward two minutes.

188
00:08:39,603 --> 00:08:42,062
- "All right, just get off me."
- "What the hell?"

189
00:08:42,189 --> 00:08:43,480
- "Let go of me."
- "You let go of me, dude."

190
00:08:43,607 --> 00:08:45,232
- "Let go of me."
- "You let go of-"

191
00:08:45,358 --> 00:08:47,651
White dudes cannot decide
if they want to fight or not.

192
00:08:47,777 --> 00:08:49,320
- Yeah.
- They can't do it.

193
00:08:49,446 --> 00:08:51,488
- Then phase four comes,
and that's when one of the guys

194
00:08:51,615 --> 00:08:54,074
has the genius idea
to do the "fake-out."

195
00:08:54,201 --> 00:08:56,535
And, uh, so he's gonna
walk away, act like-

196
00:08:56,661 --> 00:08:59,205
and then do a sneak偷偷地做 attack

197
00:08:59,331 --> 00:09:01,582
using karate空手道,
which he does not practice.

198
00:09:01,708 --> 00:09:03,542
- Right, so it's like-
- So it's just like-

199
00:09:03,668 --> 00:09:05,044
- "I don't even care. I don't
even care anymore, dude."

200
00:09:05,170 --> 00:09:06,253
- "I don't care."
- "Forget about that.

201
00:09:06,379 --> 00:09:10,591
You know what?"

202
00:09:10,717 --> 00:09:12,218
- "What are you doing, man?"

203
00:09:12,344 --> 00:09:16,138
And always-always,
there is a drunk陶醉的 white girl

204
00:09:16,264 --> 00:09:17,890
on the sidelines球场边线
just talking about,

205
00:09:18,016 --> 00:09:20,726
"You guys, stop fighting!"

206
00:09:20,852 --> 00:09:24,313
"Megan, shut up."

207
00:09:24,439 --> 00:09:26,899
- "I don't need your help,
Megan."

208
00:09:27,025 --> 00:09:28,817
- "You guys
are best friends!"

209
00:09:28,944 --> 00:09:31,362
- "Megan,
seriously, Megan.

210
00:09:31,488 --> 00:09:32,529
Shut up!"

211
00:09:36,993 --> 00:09:38,160
- I love this place.
- This place is amazing.

212
00:09:38,286 --> 00:09:39,954
- Amazing.
- So hot.

213
00:09:40,080 --> 00:09:41,163
- Got like four stars somewhere.
- I heard it got six stars,

214
00:09:41,289 --> 00:09:42,581
which is, like,
impossible.

215
00:09:42,707 --> 00:09:44,416
- Oh, my god.
Let's take a picture.

216
00:09:44,542 --> 00:09:46,001
- Let's take a picture right
now. Oh, my god, here we go.

217
00:09:46,127 --> 00:09:47,544
- Oh, my god, I'm making a face.

218
00:09:47,671 --> 00:09:49,004
- Oh, my god, I'm, like,
one big nose. Delete it.

219
00:09:49,130 --> 00:09:51,215
Ew, delete it.

220
00:09:52,759 --> 00:09:54,009
- Ew.
- What am I doing? I'm gross粗野的.

221
00:09:54,135 --> 00:09:55,344
- Why do I look
like an orca whale逆戈鲸?

222
00:09:55,470 --> 00:09:57,471
- Delete it.
- Delete it.

223
00:09:57,597 --> 00:09:59,848
- Everybody on the ground!

224
00:09:59,975 --> 00:10:01,517
- Oh, my god.
We're getting robbed.

225
00:10:01,643 --> 00:10:03,352
- Oh, my god, oh my god,
oh, my god, oh, my-

226
00:10:03,478 --> 00:10:05,187
Oh, my god,
what's my eye doing?

227
00:10:05,313 --> 00:10:07,189
- Ew, I look like I'm 40.

228
00:10:07,315 --> 00:10:08,732
Delete it.
- Oh, my god.

229
00:10:08,858 --> 00:10:09,984
- Let's get out of here,
but delete it.

230
00:10:10,110 --> 00:10:13,237
- Get down! Down!

231
00:10:13,363 --> 00:10:15,114
- Oh, my god, I look like
somebody unwrapped打开的 a mummy木乃伊.

232
00:10:15,240 --> 00:10:16,782
- God, I look like
a five-year-old drew me.

233
00:10:16,908 --> 00:10:18,367
- Totally delete it.
- Delete it.

234
00:10:18,493 --> 00:10:19,868
- Delete it.
- Right?

235
00:10:19,995 --> 00:10:22,830
- Police! Drop your weapon!

236
00:10:22,956 --> 00:10:24,498
- Ew, I totally look
like Cloverfield.

237
00:10:24,624 --> 00:10:26,292
- Did I just have
a stroke一画一笔 or something?

238
00:10:26,418 --> 00:10:27,626
Delete it.

239
00:10:27,752 --> 00:10:28,919
- Right?
- Yeah.

240
00:10:30,422 --> 00:10:32,214
- L-ladies,
can you just show me

241
00:10:32,340 --> 00:10:33,674
the picture
you were talking about, please?

242
00:10:33,800 --> 00:10:35,968
- Oh, it's like-
- Yeah, stop.

243
00:10:36,094 --> 00:10:37,970
Great, right there.
That's great.

244
00:10:38,096 --> 00:10:39,763
Grady, we got a clear shot
of the perpetrator犯罪者.

245
00:10:39,889 --> 00:10:41,098
He's holding a shotgun散弹猎枪
and everything.

246
00:10:41,224 --> 00:10:42,891
Wai-wait, wait,
where'd the picture go?

247
00:10:43,018 --> 00:10:44,268
- We deleted it.
- Gross显而易见的.

248
00:10:44,394 --> 00:10:45,894
- What?
- I had childbirth生孩子 face.

249
00:10:46,021 --> 00:10:48,230
- Yeah, I looked like
my grandmother having an orgasm性高潮.

250
00:10:48,356 --> 00:10:50,107
- Delete it.
- Delete it.

251
00:10:50,233 --> 00:10:53,527
- Grady, book these idiots
for destroying evidence.

252
00:10:53,653 --> 00:10:56,322
- Ow.
Ew.

253
00:10:56,448 --> 00:10:57,781
Why are you being
so "manhandly"男子气概?

254
00:10:57,907 --> 00:10:59,116
- I can't believe this.
Why are we even-

255
00:10:59,242 --> 00:11:00,617
we were the heroes
of the evening.

256
00:11:00,744 --> 00:11:02,202
- Ridiculous.
- We stopped the perpetrator.

257
00:11:02,329 --> 00:11:03,662
Now we're being arrested?
- Can we please-

258
00:11:03,788 --> 00:11:05,247
Oh, my god,
these cops should be arrested.

259
00:11:05,373 --> 00:11:06,415
- This is a miscarriage流产
of justice, the entire th-

260
00:11:07,709 --> 00:11:09,251
- What?
No fucking way.

261
00:11:09,377 --> 00:11:10,669
- Oh, my god, you cannot-
- Delete it.

262
00:11:10,795 --> 00:11:12,087
- You cannot arrest me
with that picture.

263
00:11:12,213 --> 00:11:13,464
Are you kidding me?
- Delete it.

264
00:11:13,590 --> 00:11:15,507
- I was blinking.
This is ridiculous.

265
00:11:15,633 --> 00:11:17,009
- Delete it.
- Delete it right now.

266
00:11:17,135 --> 00:11:18,218
Delete it.
You must delete this photo.

267
00:11:18,345 --> 00:11:19,845
- Delete it. No.
- Delete it.

268
00:11:19,971 --> 00:11:21,180
I'm not leaving
until he deletes it.

269
00:11:21,306 --> 00:11:22,848
- He has a picture of us.
Delete it.

270
00:11:22,974 --> 00:11:24,975
- I'm not leaving until he-
you gotta delete it!

271
00:11:25,101 --> 00:11:26,393
You've gotta delete it.
- Just delete it.

272
00:11:26,519 --> 00:11:28,270
- You have to delete this pic-
- Delete it!

273
00:11:31,399 --> 00:11:32,816
- ♪ But I don't need you ♪

274
00:11:32,942 --> 00:11:35,235
- Kee-when Keegan laughs,
when he finds something funny...

275
00:11:35,362 --> 00:11:37,196
- Mm-hmm.
- It's over, you have to move.

276
00:11:37,322 --> 00:11:38,781
- I have to move.

277
00:11:38,907 --> 00:11:39,948
It's out of the question
for me to laugh sitting still.

278
00:11:40,075 --> 00:11:41,617
It doesn't happen.

279
00:11:41,743 --> 00:11:43,410
If I'm watching television
alone, by myself,

280
00:11:43,536 --> 00:11:44,745
in my home,
this will happen.

281
00:11:48,958 --> 00:11:51,043
Um, and a little bit of these.

282
00:11:51,169 --> 00:11:52,252
Yeah.
- Nobody-

283
00:11:52,379 --> 00:11:53,921
nobody even there
to watch it.

284
00:11:54,047 --> 00:11:55,172
- I can't do it.
I can't sit still.

285
00:11:55,298 --> 00:11:56,757
I have to express myself
physically,

286
00:11:56,883 --> 00:11:59,009
and so a really
serious situation for me

287
00:11:59,135 --> 00:12:00,636
is watching a comedy
on a plane.

288
00:12:00,762 --> 00:12:02,221
- Oh, gosh.
- That is a problem.

289
00:12:02,347 --> 00:12:03,931
- Yes, l-I do feel
for your fellow同伴 passengers.

290
00:12:04,057 --> 00:12:07,976
- I was literally on a plane
watching Bridesmaids,

291
00:12:08,103 --> 00:12:09,436
and you remember the scene where
she's got on the wedding dress,

292
00:12:09,562 --> 00:12:10,604
and she poops排便 in the middle
of the street?

293
00:12:10,730 --> 00:12:12,231
- Yeah.

294
00:12:12,357 --> 00:12:13,440
- That scene comes on, and this
is what literally happens to me.

295
00:12:13,566 --> 00:12:14,608
I go-I went like this-
I went,

296
00:12:14,734 --> 00:12:18,404
"Oh, no. Oh, no.
Oh.

297
00:12:18,530 --> 00:12:21,073
Oh!
This-excuse me."

298
00:12:21,199 --> 00:12:22,408
"Oh, shit!"

299
00:12:22,534 --> 00:12:23,951
"Oh, shit!

300
00:12:24,077 --> 00:12:25,327
"Oh, shit!

301
00:12:25,453 --> 00:12:27,704
Oh, drink cart手推车.
Drink cart."

302
00:12:27,831 --> 00:12:32,167
I'M SORRY-
Whoo!

303
00:12:32,293 --> 00:12:34,253
- Ladies and gentlemen,

304
00:12:34,379 --> 00:12:37,297
please turn off your cell phones
and enjoy our production

305
00:12:37,424 --> 00:12:41,051
of Lunch with Greatness.

306
00:12:41,177 --> 00:12:42,803
- My, my, my.

307
00:12:42,929 --> 00:12:45,639
What a beautiful day
In Montgomery, Alabama.

308
00:12:45,765 --> 00:12:49,685
I swear, the sun is so hot,
I nearly melted.

309
00:12:49,811 --> 00:12:51,895
- Indeed, Dr. King,
but I suspect怀疑

310
00:12:52,021 --> 00:12:53,647
we didn't meet here
to talk about the weather.

311
00:12:53,773 --> 00:12:57,067
- Indeed,
brother Malcolm.

312
00:12:57,193 --> 00:12:59,695
Unless, of course,
we articulate清楚地表达

313
00:12:59,821 --> 00:13:02,906
that the sun
in this country

314
00:13:03,032 --> 00:13:06,076
shines brighter on some
than it does on others.

315
00:13:06,202 --> 00:13:10,664
- Mm.
- Yeah.

316
00:13:10,790 --> 00:13:14,209
- Yes, but when the sun
does shine on us,

317
00:13:14,335 --> 00:13:17,087
it is growing
the seeds of revolution.

318
00:13:17,213 --> 00:13:21,300
- Amen.
- That's what I'm talking about.

319
00:13:23,678 --> 00:13:25,596
- I see your point,
brother Malcolm,

320
00:13:25,722 --> 00:13:31,018
but do not those same seeds
need to be watered with love?

321
00:13:33,730 --> 00:13:38,150
- I suppose that is the subject
of some discussion, Dr. King.

322
00:13:41,613 --> 00:13:44,781
- And I am all for discussion,
brother Malcolm.

323
00:13:44,908 --> 00:13:46,950
Unless, of course,
the words devolve转移

324
00:13:47,076 --> 00:13:49,745
into proclamations公告
of hate.

325
00:13:49,871 --> 00:13:53,165
- Yes.
- Right.

326
00:13:53,291 --> 00:13:57,753
- Well, is that not why we
are here today, Dr. King-

327
00:13:57,879 --> 00:14:00,130
to discuss?

328
00:14:06,554 --> 00:14:08,263
- Yes, brother Malcolm.

329
00:14:08,389 --> 00:14:11,683
And I am confident
that as we discuss-

330
00:14:11,809 --> 00:14:12,893
- We are going to discuss it

331
00:14:13,019 --> 00:14:14,937
and discuss it

332
00:14:15,063 --> 00:14:19,858
until I say that

333
00:14:19,984 --> 00:14:24,154
our people
have never resorted诉诸 to violence

334
00:14:24,280 --> 00:14:27,241
before the white man
gave them no choice

335
00:14:27,367 --> 00:14:28,617
but to pay him back

336
00:14:28,743 --> 00:14:30,953
in the only currency货币
he understands.

337
00:14:31,079 --> 00:14:32,287
- Yes.

338
00:14:36,793 --> 00:14:38,585
- Brother Malcolm,

339
00:14:38,711 --> 00:14:41,088
is that what you
really have to say...

340
00:14:41,214 --> 00:14:43,799
at this point...
in our lunch?

341
00:14:43,925 --> 00:14:46,343
- Indeed, Dr. King,

342
00:14:46,469 --> 00:14:50,222
because we didn't land
on Plymouth Rock,

343
00:14:50,348 --> 00:14:52,599
Plymouth Rock
landed on us.

344
00:14:56,729 --> 00:14:59,106
- Amen.
- Mm-hmm.

345
00:14:59,232 --> 00:15:00,774
- Okay, well, yes.
All right.

346
00:15:00,900 --> 00:15:03,318
Yeah, l-I've heard
you say that before.

347
00:15:03,444 --> 00:15:07,197
I... don't know why
we're talking about rocks when,

348
00:15:07,323 --> 00:15:10,075
in fact,
it is the flesh and the bone

349
00:15:10,201 --> 00:15:12,744
and the-the dreams-
l-I have a dream

350
00:15:12,870 --> 00:15:15,622
that someday our children
will be judged

351
00:15:15,748 --> 00:15:17,499
by the content
of their character,

352
00:15:17,625 --> 00:15:19,835
not by the color
of their skin.

353
00:15:23,172 --> 00:15:24,673
- Same for me.
Same for me.

354
00:15:24,799 --> 00:15:27,175
In fact,
I believe that one day,

355
00:15:27,302 --> 00:15:28,885
a black man
will become the president

356
00:15:29,012 --> 00:15:30,637
of the United States
of America.

357
00:15:30,763 --> 00:15:32,723
Yes.

358
00:15:32,849 --> 00:15:34,766
- Hopefully for eight years.

359
00:15:34,892 --> 00:15:36,476
Obama 2012.

360
00:15:40,440 --> 00:15:42,941
- Oh, of course, eight years.
Of course, eight years.

361
00:15:43,067 --> 00:15:45,068
That's a given那是理所当然的, right?
That's a given.

362
00:15:45,194 --> 00:15:47,946
You know? I mean,
but in order to get there,

363
00:15:48,072 --> 00:15:50,157
isn't he going to need
the help

364
00:15:50,283 --> 00:15:53,785
of our strong, beautiful,
black women?

365
00:15:55,079 --> 00:15:56,955
Gotta respect the sisters.

366
00:15:57,081 --> 00:15:59,416
Gotta respect
the sisters!

367
00:15:59,542 --> 00:16:01,251
Gotta respect the sisters.

368
00:16:03,087 --> 00:16:05,422
- As Wesley Snipes said
in Passenger 57,

369
00:16:05,548 --> 00:16:08,175
"Always bet on black."
Yeah.

370
00:16:13,056 --> 00:16:17,517
- ♪ The west side
is the best side ♪

371
00:16:28,696 --> 00:16:30,739
- Okay, so Keegan and l-
we grew up in the '90s.

372
00:16:30,865 --> 00:16:32,616
- Yes.
- Uh, and if you don't know,

373
00:16:32,742 --> 00:16:34,034
the fashion in the '90s
was god-awful糟透的.

374
00:16:34,160 --> 00:16:35,285
- Horrific可怕的.

375
00:16:35,411 --> 00:16:37,120
And the-and the worst-
the worst thing

376
00:16:37,246 --> 00:16:39,456
in all the '90s
were Z Cavariccis.

377
00:16:39,582 --> 00:16:41,249
- See I d-
I don't even know this.

378
00:16:41,376 --> 00:16:44,252
- You know, Z Cavariccis-
They-they were a pant.

379
00:16:44,379 --> 00:16:47,214
And-and the waist腰部 of the pant
was so high, it was like,

380
00:16:47,340 --> 00:16:49,007
"what are you, a toreador西班牙斗牛士?"
- Mm-hmm.

381
00:16:49,133 --> 00:16:52,511
- And they had these pockets
that had these pleats裤褶 on them,

382
00:16:52,637 --> 00:16:55,013
which gave the illusion of hips
on a man.

383
00:16:55,139 --> 00:16:56,640
On a man.
- Oh, yeah.

384
00:16:56,766 --> 00:16:58,475
- The leg looked
like an ice cream cone圆锥形,

385
00:16:58,601 --> 00:17:00,936
Just, whoop!
Tapered渐缩的 in here.

386
00:17:01,062 --> 00:17:03,063
And then at the bottom,
you would take the excess过量

387
00:17:03,189 --> 00:17:05,315
and just go like that
and roll them up

388
00:17:05,441 --> 00:17:08,151
to taper
an already tapered pant!

389
00:17:08,277 --> 00:17:10,987
Why are you tapering
the tapered pant?

390
00:17:11,114 --> 00:17:12,656
- Why-why are you-
why are you getting so mad?

391
00:17:12,782 --> 00:17:14,324
- Because I wore them,
Jordan.

392
00:17:18,705 --> 00:17:20,664
I wore them.

393
00:17:29,424 --> 00:17:34,219
- ♪ Ooh ♪
- ♪ Whoa oh oh ♪

394
00:17:34,345 --> 00:17:38,348
- ♪ Oh ♪
- ♪ Whoa oh oh oh ♪

395
00:17:38,474 --> 00:17:43,812
♪ Ooh, you know
that I been waiting ♪

396
00:17:43,938 --> 00:17:47,190
♪ Had this feeling
for so long ♪

397
00:17:47,316 --> 00:17:49,401
♪ And it feels so right,
baby ♪

398
00:17:49,527 --> 00:17:53,655
♪ How could this feeling
be wrong? ♪

399
00:17:53,781 --> 00:17:57,409
- ♪ Sometimes the thing
that you been searching for ♪

400
00:17:57,535 --> 00:18:00,537
♪ Has been right there
the whole time ♪

401
00:18:00,663 --> 00:18:02,998
♪ So, baby, I'm here
to tell ya ♪

402
00:18:03,124 --> 00:18:07,711
♪ I'm waiting right here
just to make you all mine, you ♪

403
00:18:07,837 --> 00:18:11,339
♪ You're the one
I always hoped for, you ♪

404
00:18:11,466 --> 00:18:14,676
- ♪ It's a feeling
I can't hide, you ♪

405
00:18:14,802 --> 00:18:16,803
- ♪ And I always been
your baby ♪

406
00:18:16,929 --> 00:18:20,891
♪ Standing right here
by your side ♪

407
00:18:21,017 --> 00:18:24,936
♪ Yeah, it's been building up
inside of me ♪

408
00:18:25,062 --> 00:18:27,397
♪ And I just gotta come out ♪

409
00:18:27,523 --> 00:18:30,317
- ♪ And tell you
what I'm feeling, girl ♪

410
00:18:30,443 --> 00:18:34,154
♪ Tell you our love
is what I'm talkin' about ♪

411
00:18:34,280 --> 00:18:37,908
- ♪ I gotta tell you
what my heart knows ♪

412
00:18:38,034 --> 00:18:41,119
♪ And what is has known
for so long ♪

413
00:18:41,245 --> 00:18:43,955
- ♪ I finally need
to show you ♪

414
00:18:44,081 --> 00:18:45,749
♪ My arms
are wide open ♪

415
00:18:45,875 --> 00:18:47,959
♪ That's where you belong,
you ♪

416
00:18:48,085 --> 00:18:50,796
♪ You know I'll make love
to you gently, you ♪

417
00:18:52,507 --> 00:18:55,175
- ♪ The way I've seen it
in my mind, you ♪

418
00:18:55,301 --> 00:18:57,761
- ♪ I will take care
of you, baby ♪

419
00:18:57,887 --> 00:18:59,387
- ♪ There ain't no rush,
baby ♪

420
00:18:59,514 --> 00:19:00,889
♪ Let's take our sweet time ♪

421
00:19:01,015 --> 00:19:02,891
- ♪ I do it
to you slow ♪

422
00:19:03,017 --> 00:19:04,643
♪ The way
you've never done before ♪

423
00:19:04,769 --> 00:19:07,938
- ♪ Open up your mind,
I will open up your door ♪

424
00:19:08,064 --> 00:19:11,775
- ♪ I been searchin' all around
for the one I can call mine ♪

425
00:19:11,901 --> 00:19:13,276
- ♪ Sometimes the thing
you're searching for ♪

426
00:19:13,402 --> 00:19:16,112
♪ Has been there
the whole time, you ♪

427
00:19:16,239 --> 00:19:19,991
- ♪ I want to give you
all I got now, you ♪

428
00:19:20,117 --> 00:19:23,453
- ♪ And I'll take
all you can give, you ♪

429
00:19:23,579 --> 00:19:25,288
♪ And I'll never
turn my back on you ♪

430
00:19:25,414 --> 00:19:27,582
♪ As long as
we both shall live ♪

431
00:19:27,708 --> 00:19:29,584
- ♪ I'm talking 'bout you ♪

432
00:19:29,710 --> 00:19:32,295
- ♪ You, standing
right in front of you ♪

433
00:19:32,421 --> 00:19:34,005
♪ You, whoo ♪

434
00:19:34,131 --> 00:19:35,590
♪ Nothing left to hide ♪

435
00:19:35,716 --> 00:19:38,343
♪ Sugar,
said I'm telling you ♪

436
00:19:38,469 --> 00:19:42,347
♪ Everything
I've kept very deep inside ♪

437
00:19:42,473 --> 00:19:45,600
♪ Oh, rubbing摩擦 up against
your big, bald秃顶的 head ♪

438
00:19:45,726 --> 00:19:47,227
♪ Your big, bald head ♪

439
00:19:47,353 --> 00:19:49,479
♪ I said your big, bald head,
and then I ♪

440
00:19:49,605 --> 00:19:51,565
♪ There's no one else
I could be possibly ♪

441
00:19:51,691 --> 00:19:54,526
♪ Talking about
'cause you are bald ♪

442
00:19:54,652 --> 00:19:56,069
♪ And you are here ♪

443
00:20:09,709 --> 00:20:11,293
♪ You ♪

444
00:20:11,419 --> 00:20:15,005
♪ You're a woman,
I'm a man now ♪

445
00:20:15,131 --> 00:20:17,924
♪ And you know
that's how I like it ♪

446
00:20:18,050 --> 00:20:20,594
♪ And I'm specifically talkin'
'bout you ♪

447
00:20:20,720 --> 00:20:23,805
♪ Because you're a girl,
and I am the opposite ♪

448
00:20:23,931 --> 00:20:27,684
♪ You, woman, woman, woman,
woman, woman ♪

449
00:20:27,810 --> 00:20:31,688
♪ You are a woman,
I am a man ♪

450
00:20:31,814 --> 00:20:34,649
♪ You know I like women ♪

451
00:20:34,775 --> 00:20:37,444
♪ Because I am a man ♪

452
00:20:37,570 --> 00:20:41,948
♪ Never liked a man
in my life before ♪

453
00:20:42,074 --> 00:20:44,117
♪ And I don't like one now ♪

454
00:20:46,913 --> 00:20:48,580
- Thank you so much.
- Thank you.

455
00:20:48,706 --> 00:20:49,873
- Good night, everybody.
- Good night.

456
00:20:49,999 --> 00:20:51,041
- Good night.
What?

457
00:20:51,167 --> 00:20:52,584
You wanna go?
You wanna go?

458
00:20:52,710 --> 00:20:54,294
- You wanna go?
- You wanna do it?

459
00:20:54,420 --> 00:20:56,588
You wanna go do it right now?
Let's go right now.

460
00:20:56,714 --> 00:20:57,756
- Let go of my shirt, dude.
- Let go of my shirt, dude.

461
00:20:57,882 --> 00:20:59,257
- Let go of my-

462
00:20:59,383 --> 00:21:02,260
- ♪ I'm gonna do
my one line here ♪

463
00:21:02,386 --> 00:21:04,387
- Oh, yeah.

464
00:21:07,475 --> 00:21:08,683
- We asked 'em to move
black people down, please.

465
00:21:08,809 --> 00:21:10,644
- Black people in the front.
- We have enough.

466
00:21:10,770 --> 00:21:11,978
I love how, if you put
white people in the back,

467
00:21:12,104 --> 00:21:13,563
all of the sudden,
they become rowdy吵闹的.

468
00:21:13,689 --> 00:21:16,691
What the fuck is that?
"Yeah!"

469
00:21:16,817 --> 00:21:18,610
- See, we didn't really mind
being in the back

470
00:21:18,736 --> 00:21:21,404
all those years.
It's fun as shit back there.

