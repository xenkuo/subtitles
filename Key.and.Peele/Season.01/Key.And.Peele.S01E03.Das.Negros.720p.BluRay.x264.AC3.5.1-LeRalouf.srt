﻿1
00:00:01,001 --> 00:00:02,877
- Where you at, though?
- Look at this.

2
00:00:03,003 --> 00:00:04,462
34, 24, sha-bloimps.
- Blip.

3
00:00:04,588 --> 00:00:05,922
- Blip, blip.

4
00:00:06,048 --> 00:00:07,173
Oh, man, check this out, dog.
- Where, where?

5
00:00:07,299 --> 00:00:08,549
- Look at this fine
piece of ass

6
00:00:08,676 --> 00:00:10,426
all the way
down the street here, dog.

7
00:00:10,552 --> 00:00:11,678
- That's what I'm talkin' about,
though, hey, girl!

8
00:00:11,804 --> 00:00:13,137
- Damn. Ooh!

9
00:00:13,263 --> 00:00:14,389
- Yeah, walk this way
with that fine ass.

10
00:00:14,515 --> 00:00:15,932
Blip!

11
00:00:16,058 --> 00:00:17,725
- Yeah, girl,
why don't you come down here

12
00:00:17,851 --> 00:00:19,811
and put that in my mouth,
'cause daddy want a snack 点心!

13
00:00:19,937 --> 00:00:23,022
- Blip, blap, takin' a bap!
- Why you hurtin' me, girl?

14
00:00:23,148 --> 00:00:24,857
- Brisket 胸部. Briskety!
- You're hurtin' me!

15
00:00:26,360 --> 00:00:29,821
That's deliciousness 可口的, yeah!
Yeah!

16
00:00:29,947 --> 00:00:34,367
Yeah, keep it coming!
Ye-

17
00:00:34,493 --> 00:00:37,829
And-and that is a professional
basketball mascot 吉祥物.

18
00:00:37,955 --> 00:00:41,833
- That is a man in an
orange rhinoceros 犀牛 costume 服装.

19
00:00:52,970 --> 00:00:56,514
- Hell, I'd fuck
that rhino.

20
00:01:23,125 --> 00:01:25,126
- Hello.

21
00:01:25,252 --> 00:01:27,086
- All right.

22
00:01:27,212 --> 00:01:29,422
All right, all right.
- Thank you.

23
00:01:29,548 --> 00:01:31,549
Welcome to the show.
Thank you for coming.

24
00:01:31,675 --> 00:01:33,509
Uh, I am Keegan.
- And I am Jordan. Hello.

25
00:01:33,635 --> 00:01:35,011
- And this is
Key and Peele.

26
00:01:35,137 --> 00:01:37,930
Thank you, thank you.

27
00:01:38,056 --> 00:01:40,475
- And, uh, Africa
is truly fucked up 搞砸了.

28
00:01:40,601 --> 00:01:42,143
- Okay. You just came
outta the gate with that.

29
00:01:42,269 --> 00:01:43,686
- It's fucked up,
Keegan.

30
00:01:43,812 --> 00:01:45,480
- See, and I completely
disagree.

31
00:01:45,606 --> 00:01:46,939
I would go to Africa
right now.

32
00:01:47,065 --> 00:01:48,983
- No, see,
there are flyover 立交桥 states.

33
00:01:49,109 --> 00:01:50,902
For me, Africa,
that's a flyover continent.

34
00:01:51,028 --> 00:01:52,904
- So, you're
seriously telling me

35
00:01:53,030 --> 00:01:55,239
you would not
want to see the Nile 尼罗河?

36
00:01:55,365 --> 00:01:56,866
- No.

37
00:01:56,992 --> 00:01:58,034
- You would not want to see
the plains of the Serengeti

38
00:01:58,160 --> 00:01:59,577
- No.

39
00:01:59,703 --> 00:02:00,745
- You would not want to see,
uh, Mt. Kilimanjaro 乞力马扎罗山.

40
00:02:00,871 --> 00:02:02,205
- Kilimanjaro?

41
00:02:02,331 --> 00:02:03,498
I'm more worried
about Mt. "Kilima-negro."

42
00:02:03,624 --> 00:02:05,708
- No, you didn't.
No, you did not.

43
00:02:05,834 --> 00:02:07,418
You did not just say that.
- I'm saying, look...

44
00:02:07,544 --> 00:02:08,711
slavery was an awful 可怕的 thing.

45
00:02:08,837 --> 00:02:10,588
Awful.

46
00:02:10,714 --> 00:02:12,924
All I'm saying, silver lining,
it got my ass out of Africa.

47
00:02:13,050 --> 00:02:14,425
- Okay.
- I said it.

48
00:02:14,551 --> 00:02:16,093
- But things are
improving there, Jordan.

49
00:02:16,220 --> 00:02:17,261
Things are different.
- It's not-It's not-

50
00:02:17,387 --> 00:02:18,763
It's not different enough.

51
00:02:18,889 --> 00:02:22,225
I'm sorry, no, I refuse
to go to a continent

52
00:02:22,351 --> 00:02:24,644
that's so bad
that people don't even care

53
00:02:24,770 --> 00:02:28,356
if there are flies
on their face.

54
00:02:28,482 --> 00:02:30,274
- Unbelievable.

55
00:02:31,819 --> 00:02:33,319
- All right,
y'all gather round.

56
00:02:33,445 --> 00:02:36,197
Gather round.
Welcome, gentlemen.

57
00:02:36,323 --> 00:02:39,992
What a beautiful and blessed day
for an auction.

58
00:02:40,118 --> 00:02:44,956
All right, y'all,
get on up there.

59
00:02:45,082 --> 00:02:46,457
- Put that whip 鞭子 down
and see what happens, though.

60
00:02:46,583 --> 00:02:47,959
- Straight up直接说.

61
00:02:48,085 --> 00:02:50,127
I don't care what plantation 种植园
I end up on.

62
00:02:50,254 --> 00:02:52,588
I'm straight staging展现 a revolt造反
in this motherfucker.

63
00:02:52,714 --> 00:02:53,840
- Hells yeah.

64
00:02:53,966 --> 00:02:57,718
- We have lot A,
lot B, and lot C.

65
00:02:57,845 --> 00:02:59,887
- Uh, $3 on lot A.
- $4.

66
00:03:00,013 --> 00:03:01,264
- 5!

67
00:03:01,390 --> 00:03:04,684
- $5 going once, twice,
three times, sold.

68
00:03:04,810 --> 00:03:09,814
Lot A goes to the man
in the black hat.

69
00:03:11,358 --> 00:03:13,109
- I mean, good.
- Yeah.

70
00:03:13,235 --> 00:03:15,236
- I'm glad
I didn't get sold,

71
00:03:15,362 --> 00:03:17,905
'cause I don't want to be owned
by another human being.

72
00:03:18,031 --> 00:03:20,241
- Whoever buys me, they better
kill me the first day,

73
00:03:20,367 --> 00:03:22,660
or I'ma go buck-wild
on the whole operation.

74
00:03:22,786 --> 00:03:24,245
- Okaay?

75
00:03:24,371 --> 00:03:25,913
- Next one,
get up on up there, now.

76
00:03:26,039 --> 00:03:28,875
- Oh, this-okay.

77
00:03:31,420 --> 00:03:32,753
- $6 on lot A.

78
00:03:32,880 --> 00:03:34,630
- $7!
- 8.

79
00:03:34,756 --> 00:03:36,340
- 9!

80
00:03:36,466 --> 00:03:38,634
- $9 going once, twice,
three times, sold!

81
00:03:40,345 --> 00:03:42,179
- Okay, well,
you have to buy that dude.

82
00:03:42,306 --> 00:03:43,681
- It's a no-brainer.
- I mean, that guy's huge.

83
00:03:43,807 --> 00:03:45,808
- A massive individual.
- That's two of me.

84
00:03:45,934 --> 00:03:48,144
- Anybody would buy him.
- I'd buy that dude.

85
00:03:48,270 --> 00:03:50,271
- My question is,
how'd they catch him?

86
00:03:50,397 --> 00:03:53,149
- Next!

87
00:03:53,275 --> 00:03:55,276
- Okay. Oh, yeah.
- Yeah.

88
00:03:55,402 --> 00:03:56,944
- $2 on lot A.

89
00:03:57,070 --> 00:03:59,697
- $2 going once, twice,
three times, sold.

90
00:03:59,823 --> 00:04:01,115
- See, now,
that surprises me.

91
00:04:01,241 --> 00:04:03,784
- That is interesting,
to say the least.

92
00:04:03,911 --> 00:04:05,703
- I mean, well, it just seems
like at a certain point,

93
00:04:05,829 --> 00:04:08,122
it's like, do they even know
what they're looking for?

94
00:04:08,248 --> 00:04:09,415
- It's like
the whole criteria 标准/条件

95
00:04:09,541 --> 00:04:10,875
seems just a little
inconsistent 不一致的.

96
00:04:11,001 --> 00:04:12,460
- I mean, at some point,
I want to be on lot A.

97
00:04:12,586 --> 00:04:15,129
- Yeah, which-
Can a brotha get on lot A?

98
00:04:15,255 --> 00:04:18,591
- Next.

99
00:04:18,717 --> 00:04:19,800
- Oh, here we go.
- Here we go.

100
00:04:19,927 --> 00:04:21,052
- Been a pleasure.
- Give 'em hell.

101
00:04:21,178 --> 00:04:22,219
- All right.
- Okay.

102
00:04:22,346 --> 00:04:23,888
- $8 on lot A.

103
00:04:24,014 --> 00:04:25,264
- Going once, twice,
three times, sold!

104
00:04:25,390 --> 00:04:26,599
- How does that happen?

105
00:04:26,725 --> 00:04:27,767
- Nope, not true.
- What you just said-

106
00:04:27,893 --> 00:04:29,560
that's gobbledygook 官样文章, okay?

107
00:04:29,686 --> 00:04:30,811
That can't be true.
'Cause what can this dude do?

108
00:04:30,938 --> 00:04:32,605
Look at him.
What could he pick?

109
00:04:32,731 --> 00:04:34,106
A cotton plant is,
like, this tall.

110
00:04:34,232 --> 00:04:35,524
- Yes.
- No offense, brotha,

111
00:04:35,651 --> 00:04:37,151
I'm just saying.
- Offense taken.

112
00:04:37,277 --> 00:04:38,694
- Wha-

113
00:04:38,820 --> 00:04:41,280
Am I wrong? Is he not short?
He's short.

114
00:04:41,406 --> 00:04:45,451
But you are actually short
in real life, in the world.

115
00:04:45,577 --> 00:04:47,286
- You're good, man.
- Enough.

116
00:04:47,412 --> 00:04:49,830
I will not have
my reputation tainted 污染的,

117
00:04:49,957 --> 00:04:53,042
sellin' superficial 浅薄的,
bigoted 心胸狭窄的 slaves.

118
00:04:53,168 --> 00:04:56,045
- Superficial? Did that really
just come out of your mouth?

119
00:04:56,171 --> 00:04:58,255
- That's it!
This auction's over!

120
00:04:58,382 --> 00:04:59,966
- Auction's over?
- Whoa, whoa, whoa.

121
00:05:00,092 --> 00:05:02,426
No, it's-it ain't over.
It's not over!

122
00:05:02,552 --> 00:05:03,970
I'm strong, y'all!

123
00:05:04,096 --> 00:05:05,888
I'm very str-
I can sleep in a bucket 桶.

124
00:05:06,014 --> 00:05:08,933
- I'm fast, I got stamina耐力,
and I know magic.

125
00:05:09,059 --> 00:05:11,060
- My worst quality
is that I'm a perfectionist 完美主义者.

126
00:05:11,186 --> 00:05:13,229
- Let me men-
Have I mentioned this? Docile.

127
00:05:13,355 --> 00:05:15,731
I am agreeable
to a fault.

128
00:05:15,857 --> 00:05:17,942
You should have seen the dude
who asked me to get on the boat

129
00:05:18,068 --> 00:05:19,193
when we came over here.

130
00:05:19,319 --> 00:05:20,695
- Not a violent bone 骨头
in my body.

131
00:05:20,821 --> 00:05:22,196
- I just walked right on,
no big deal.

132
00:05:22,322 --> 00:05:23,572
Never seen a boat
in my life.

133
00:05:30,080 --> 00:05:31,372
- 'Scuse me.
I think we're ready to order.

134
00:05:31,498 --> 00:05:33,582
- Well, um, I'm sorry.

135
00:05:33,709 --> 00:05:35,876
That's not how things work
in this establishment 企业.

136
00:05:36,003 --> 00:05:37,545
Someone will come to you.

137
00:05:37,671 --> 00:05:40,548
- Yes, we're sorry.
No-no problem.

138
00:05:40,674 --> 00:05:43,009
- What was that?
- What?

139
00:05:43,135 --> 00:05:44,760
- Well, where was
black Jeff?

140
00:05:44,886 --> 00:05:46,178
- "Black Jeff"?

141
00:05:46,304 --> 00:05:49,348
- Yeah, black Jeff.

142
00:05:49,474 --> 00:05:51,183
I read somewhere that
when you date a biracial guy,

143
00:05:51,309 --> 00:05:53,019
you're supposed to get
the best of both worlds,

144
00:05:53,145 --> 00:05:54,770
so there are
white-Jeff situations,

145
00:05:54,896 --> 00:05:56,313
and there are
black-Jeff situations,

146
00:05:56,440 --> 00:05:58,315
and that was definitely
a black-Jeff situation.

147
00:05:58,442 --> 00:06:00,317
- Oh. Okay.

148
00:06:00,444 --> 00:06:02,361
- Hi, folks,
I'm so sorry for the wait.

149
00:06:02,487 --> 00:06:05,364
Can I get you bottled
or tap water 自来水 for this evening?

150
00:06:05,490 --> 00:06:07,199
- How 'bout I bottle your ass
and kick it

151
00:06:07,325 --> 00:06:08,701
down the stairs 楼梯, man?
We've been waitin' half an hour

152
00:06:08,827 --> 00:06:10,619
for one o' you
motherfuckers to show up!

153
00:06:10,746 --> 00:06:12,705
- God, I'm so-I'm so sorry.
I'll get you a bottle

154
00:06:12,831 --> 00:06:14,874
of our finest premium water 优质水,
on the house.

155
00:06:15,000 --> 00:06:18,669
- That's what I said, bitch!
- What are you doing?

156
00:06:18,795 --> 00:06:19,920
- Black Jeff.
That was black Jeff.

157
00:06:20,047 --> 00:06:21,338
- But he was
just trying to help us.

158
00:06:21,465 --> 00:06:24,341
- Oh, so, white Jeff?
- Yes.

159
00:06:24,468 --> 00:06:26,552
- Good evening.
I'm sorry to bother you two.

160
00:06:26,678 --> 00:06:29,221
I am the maitre 总管 d'
here at Chez Henri 亨利家.

161
00:06:29,347 --> 00:06:30,514
- Oh, yes, uh,
what is it?

162
00:06:30,640 --> 00:06:33,642
- I am so sorry
to disturb you both,

163
00:06:33,769 --> 00:06:36,353
but our establishment
has a certain dress code,

164
00:06:36,480 --> 00:06:40,191
and madame's, um, decolletage 低领
is inappropriate.

165
00:06:40,317 --> 00:06:42,193
- Oh, my God.
We're-we're very sorry.

166
00:06:42,319 --> 00:06:44,653
We didn't know
there was a dress code.

167
00:06:44,780 --> 00:06:46,781
We'll certainly remember
for the next time.

168
00:06:46,907 --> 00:06:49,408
But there ain't gonna be
no next time for you!

169
00:06:49,534 --> 00:06:51,827
Lookin' at my woman,
and her-her ch-

170
00:06:51,953 --> 00:06:55,081
deco-chocka-"chockalage,"
or whatever you said, man!

171
00:06:55,207 --> 00:06:56,624
- Is there
a problem here?

172
00:06:56,750 --> 00:06:57,792
- Okay, see, it was really
a simple misunderstanding 误解.

173
00:06:57,918 --> 00:06:59,210
It all started

174
00:06:59,336 --> 00:07:00,961
with that motherfucker
right there,

175
00:07:01,088 --> 00:07:02,838
who was disrespectin' 不尊重
myself and my girl,

176
00:07:02,964 --> 00:07:04,673
but we really
love this guy,

177
00:07:04,800 --> 00:07:06,217
and he gave us
exceptional service.

178
00:07:06,343 --> 00:07:07,384
- We're gonna have to
ask you to leave.

179
00:07:07,511 --> 00:07:09,095
- You have to
ask me to leave?

180
00:07:09,221 --> 00:07:10,596
Oh, you gonna have to ask
the black man to leave? Huh?

181
00:07:10,722 --> 00:07:12,223
Actually, you don't
have to ask us to leave.

182
00:07:12,349 --> 00:07:13,766
'Cause we're going to see
ourselves out.

183
00:07:13,892 --> 00:07:15,184
And we ain't never
comin' back again!

184
00:07:15,310 --> 00:07:17,478
Though we really
appreciate your help.

185
00:07:19,147 --> 00:07:20,981
Oh, lo siento mucho.
Watch yo ass, man!

186
00:07:21,108 --> 00:07:22,858
Let us reimburse 偿还 you
for that breakage 破坏, okay?

187
00:07:22,984 --> 00:07:24,693
Right after I burn
this whole place

188
00:07:24,820 --> 00:07:27,363
to the motherfuckin' ground
with everybody inside of it!

189
00:07:27,489 --> 00:07:28,531
By which I mean
you're going to get

190
00:07:28,657 --> 00:07:29,865
a very negative review
on Yelp.

191
00:07:34,830 --> 00:07:36,497
- All right,
so we get most of our

192
00:07:36,623 --> 00:07:38,374
knowledge of history
from movies.

193
00:07:38,500 --> 00:07:41,127
- Yeah, and we have noticed
that, you know,

194
00:07:41,253 --> 00:07:43,337
not a lot of, uh,
black movies about war.

195
00:07:43,463 --> 00:07:44,588
- Right.

196
00:07:44,714 --> 00:07:45,756
- And that's because
the military

197
00:07:45,882 --> 00:07:47,049
was integrated 综合的 so late.

198
00:07:47,175 --> 00:07:48,801
So not a lot
of movies about it.

199
00:07:48,927 --> 00:07:50,511
- But actually the military was
integrated before anything else.

200
00:07:50,637 --> 00:07:52,263
- Oh, absolutely,
before anything else.

201
00:07:52,389 --> 00:07:54,849
- And, by the way, white people,
not the equality 平等 we had in mind.

202
00:07:54,975 --> 00:07:56,559
- Yeah.

203
00:07:56,685 --> 00:07:59,395
It's like, "Hey, boy,
you want a drink of water?

204
00:07:59,521 --> 00:08:00,604
"Go back
around the corner.

205
00:08:00,730 --> 00:08:01,814
"You want to die
for your country?

206
00:08:01,940 --> 00:08:03,107
Stand right here
in front of me."

207
00:08:06,778 --> 00:08:09,071
- Well, there was
that one movie Glory.

208
00:08:09,197 --> 00:08:10,823
- Yes, of course Glory.
- Glory's fucked up itself.

209
00:08:10,949 --> 00:08:12,616
I mean, they just threw
a bunch of black dudes at a fort 堡垒

210
00:08:12,742 --> 00:08:14,618
to see if it was
okay to attack it.

211
00:08:14,744 --> 00:08:15,911
- Yeah.

212
00:08:16,037 --> 00:08:17,121
And then a dearth 缺乏
of movies

213
00:08:17,247 --> 00:08:19,165
when it came
to World War II.

214
00:08:19,291 --> 00:08:20,833
No black people.
- No black-

215
00:08:20,959 --> 00:08:22,376
I mean, there's that one scene
in Pearl Harbor 珍珠港.

216
00:08:22,502 --> 00:08:24,211
- Yeah.
- Where just-

217
00:08:24,337 --> 00:08:28,007
Cuba Gooding, Jr., falls out
of a kitchen all of a sudden,

218
00:08:28,133 --> 00:08:29,592
grabs an
anti-aircraft gun 防空炮.

219
00:08:29,718 --> 00:08:32,553
Talk about,
"Show me the money!"

220
00:08:32,679 --> 00:08:34,430
What is that?

221
00:08:34,556 --> 00:08:35,806
- And then, apparently, white
people figured something out,

222
00:08:35,932 --> 00:08:37,183
because during Vietnam 越南,

223
00:08:37,309 --> 00:08:38,434
they sent
all our asses over there.

224
00:08:38,560 --> 00:08:39,602
- Oh, yeah, Vietnam.
- Everybody.

225
00:08:39,728 --> 00:08:40,769
- Hello, all of my uncles.
- Yes.

226
00:08:40,896 --> 00:08:42,438
- Welcome to the jungle.

227
00:08:42,564 --> 00:08:44,648
- What is your name? Jackson?
This way to the draft草稿.

228
00:08:44,774 --> 00:08:46,567
- There you go.

229
00:08:46,693 --> 00:08:49,028
I mean, again,
not the equality we had in mind!

230
00:08:49,154 --> 00:08:50,821
- Yeah. Crazy.

231
00:08:50,947 --> 00:08:51,989
All we wanted to do was look
at white women's asses

232
00:08:52,115 --> 00:08:53,157
without getting murdered.

233
00:08:53,283 --> 00:08:55,659
That's all
we wanted to do.

234
00:08:55,785 --> 00:08:56,869
- That's all.

235
00:09:09,257 --> 00:09:10,507
Heil Hitler.

236
00:09:10,634 --> 00:09:14,762
My name is colonel 陆军上将
Hans Mueller of the S.S.

237
00:09:14,888 --> 00:09:19,391
As you know, we were
combing 梳理 the area for Jews.

238
00:09:19,517 --> 00:09:23,562
But it has come to our attention
that two negroes have escaped,

239
00:09:23,688 --> 00:09:27,316
and are hiding out
in this area as well.

240
00:09:27,442 --> 00:09:29,318
You wouldn't happen to know

241
00:09:29,444 --> 00:09:32,738
anything about this,
now, would you?

242
00:09:32,864 --> 00:09:36,825
- Uh, no.
No negroes here. Mm-mm.

243
00:09:36,952 --> 00:09:40,496
- Uh, negroes? Ew.

244
00:09:40,622 --> 00:09:41,705
Hell no.

245
00:09:41,831 --> 00:09:45,000
- And, um,
what are your names?

246
00:09:45,126 --> 00:09:46,877
- Leroy...

247
00:09:47,003 --> 00:09:48,504
Heimer.

248
00:09:48,630 --> 00:09:50,631
Leroyheimer
is my last name,

249
00:09:50,757 --> 00:09:53,509
and my first name
is very German,

250
00:09:53,635 --> 00:09:55,678
and that is because
it is Heinrich.

251
00:09:55,804 --> 00:09:59,556
So, my name
is Heinrich Leroyheimer.

252
00:09:59,683 --> 00:10:04,561
- And I am Baron Helmut...

253
00:10:04,688 --> 00:10:06,772
Schnitzelnazi.

254
00:10:06,898 --> 00:10:09,775
- Well.

255
00:10:09,901 --> 00:10:11,652
Are you going to
invite me in?

256
00:10:11,778 --> 00:10:14,071
- Of course!
- Yeah, come on in!

257
00:10:14,197 --> 00:10:15,948
Yeah, yeah, yeah,
absolutely.

258
00:10:24,082 --> 00:10:27,793
- Please, gentlemen,
sit down.

259
00:10:27,919 --> 00:10:30,212
Now...

260
00:10:30,338 --> 00:10:35,217
as you know, the negro,
not unlike the Jew,

261
00:10:35,343 --> 00:10:39,513
can be a very
tricky狡猾的 creature.

262
00:10:39,639 --> 00:10:42,766
Oh, that's fun.

263
00:10:42,892 --> 00:10:45,019
We have developed
many tests to determine

264
00:10:45,145 --> 00:10:48,439
if an individual
is, in fact,

265
00:10:48,565 --> 00:10:52,776
an actual negro.

266
00:10:52,902 --> 00:10:55,321
It's very scientific.

267
00:10:55,447 --> 00:10:57,614
Like when we throw the beans
up against the homosexuals 同性恋

268
00:10:57,741 --> 00:10:59,700
to see if
the beans explode 爆炸.

269
00:10:59,826 --> 00:11:02,328
- Yeah, uh...

270
00:11:02,454 --> 00:11:04,496
what kind of tests?

271
00:11:04,622 --> 00:11:07,374
- It's interesting
that you would ask me that.

272
00:11:07,500 --> 00:11:11,545
Would you mind if I...

273
00:11:11,671 --> 00:11:13,422
removed your hat?

274
00:11:13,548 --> 00:11:15,758
So that I can
measure your head.

275
00:11:15,884 --> 00:11:17,426
The negro head,
interestingly enough,

276
00:11:17,552 --> 00:11:20,763
only comes
in the half sizes.

277
00:11:20,889 --> 00:11:25,017
It's one of the many interesting
things about the negro head.

278
00:11:25,143 --> 00:11:27,728
Hmm.

279
00:11:27,854 --> 00:11:29,480
Everything
seems in order.

280
00:11:31,066 --> 00:11:33,275
- Well,
no negro heads here.

281
00:11:33,401 --> 00:11:34,693
- I'll tell you what,
though.

282
00:11:34,819 --> 00:11:36,445
If a negro head
came in here,

283
00:11:36,571 --> 00:11:39,156
it would find itself
detached 分离的 from its negro body.

284
00:11:39,282 --> 00:11:40,324
I'll tell you that,
right here.

285
00:11:40,450 --> 00:11:41,909
Am I right?

286
00:11:42,035 --> 00:11:43,660
- This is very,
very good.

287
00:11:43,787 --> 00:11:46,372
But there are so many
exceptions to the rule,

288
00:11:46,498 --> 00:11:48,832
which is why
it's so important

289
00:11:48,958 --> 00:11:52,711
that we have a test
which is fool-proof.

290
00:11:52,837 --> 00:11:55,130
- Uh, m-m-m-more tests?

291
00:11:55,256 --> 00:11:59,093
- You know,
hunting the negroes all day,

292
00:11:59,219 --> 00:12:01,887
you really build up an appetite 欲望,
do you know what I'm saying?

293
00:12:02,013 --> 00:12:05,391
Can I interest you two

294
00:12:05,517 --> 00:12:08,894
in some delicious beets 甜菜?

295
00:12:09,020 --> 00:12:11,313
Interesting.

296
00:12:11,439 --> 00:12:13,565
The negro
cannot resist the beet.

297
00:12:13,691 --> 00:12:16,777
It is drawn to 被吸引到 it
like flies to scheisse 狗屎.

298
00:12:16,903 --> 00:12:20,447
Well, gentlemen.

299
00:12:20,573 --> 00:12:23,325
It appears I have
wasted your time.

300
00:12:23,451 --> 00:12:24,493
I'll be on my way.

301
00:12:25,787 --> 00:12:27,162
- Just one more thing.

302
00:12:27,288 --> 00:12:31,500
I'm curious as to
how you feel about...

303
00:12:32,669 --> 00:12:34,795
this cat toy!

304
00:12:34,921 --> 00:12:37,297
Jiggledy, jiggledy!

305
00:12:37,424 --> 00:12:39,174
Nothing?

306
00:12:39,300 --> 00:12:40,551
Well, that's embarrassing.

307
00:12:40,677 --> 00:12:44,221
I'm sorry
for bothering 打扰 you.

308
00:12:44,347 --> 00:12:46,223
You two are obviously
not negroes.

309
00:12:47,684 --> 00:12:49,226
- Of course
we're not negroes.

310
00:12:49,352 --> 00:12:50,936
- This is a no-negro zone.

311
00:12:51,062 --> 00:12:52,479
- Thank you, gentlemen.

312
00:12:52,605 --> 00:12:53,730
- Ain't no thang.

313
00:12:53,857 --> 00:12:56,942
- Herr Leroyheimer.

314
00:12:57,068 --> 00:12:59,278
Oh, that's new.

315
00:12:59,404 --> 00:13:01,947
Baron Schnitzelnazi.

316
00:13:02,073 --> 00:13:03,699
- What's goin' on?
Oh, heil Hitler.

317
00:13:03,825 --> 00:13:06,076
- I'll wait for you here.
There we go. All right.

318
00:13:06,202 --> 00:13:08,078
By the way...

319
00:13:08,204 --> 00:13:09,455
Are you, by chance,

320
00:13:09,581 --> 00:13:12,291
related to the Dusseldorf
Schnitzelnazis?

321
00:13:12,417 --> 00:13:14,751
- Yes, I am.

322
00:13:14,878 --> 00:13:16,086
- How's your Aunt 伯母 Frida?

323
00:13:16,212 --> 00:13:18,464
- Aunt Frida? She...

324
00:13:18,590 --> 00:13:21,133
is still...

325
00:13:21,259 --> 00:13:22,634
fat?

326
00:13:22,760 --> 00:13:25,971
- I don't recall
her being fat.

327
00:13:26,097 --> 00:13:28,348
But who looks like they did
in college anyway, am I right?

328
00:13:28,475 --> 00:13:30,601
- Not me. Not me. Not me.
- Not this one, either.

329
00:13:30,727 --> 00:13:33,145
- 'Cause I was a-
You know what I'm talking about?

330
00:13:33,271 --> 00:13:34,771
- Well, guten tag.
- All right.

331
00:13:34,898 --> 00:13:37,399
- Oh, gutenberg!

332
00:13:44,032 --> 00:13:47,451
- Whew.
Man, that was close.

333
00:13:47,577 --> 00:13:49,912
Whoo. We gotta
get outta here, man.

334
00:13:50,038 --> 00:13:51,705
We-

335
00:13:51,831 --> 00:13:54,208
- He left the cat toy, man!
- Uh-uh.

336
00:13:54,334 --> 00:13:56,043
- He left it!
- Uh-uh.

337
00:13:56,169 --> 00:13:58,170
- Oh!

338
00:13:58,296 --> 00:14:00,214
Oh!

339
00:14:07,096 --> 00:14:08,722
- So here's the thing.

340
00:14:08,848 --> 00:14:11,475
As black actors in Hollywood,
Jordan and I

341
00:14:11,601 --> 00:14:14,102
keep going out
for this one particular role

342
00:14:14,229 --> 00:14:15,896
over and over again.

343
00:14:16,022 --> 00:14:20,359
- We don't get called out
for the average black role.

344
00:14:20,485 --> 00:14:22,653
You know, like the gangster
who gets killed,

345
00:14:22,779 --> 00:14:25,030
or the pimp 皮条客
who gets killed,

346
00:14:25,156 --> 00:14:27,533
or the black dude in Star Trek
who gets evaporated 脱水的.

347
00:14:27,659 --> 00:14:29,201
We don't go out for those.

348
00:14:29,327 --> 00:14:30,911
We don't.

349
00:14:31,037 --> 00:14:32,496
- It's always the same role
over and over again.

350
00:14:32,622 --> 00:14:34,164
Tell 'em the role.

351
00:14:34,290 --> 00:14:35,707
- Well, maybe 'cause
we're not intimidating 威胁,

352
00:14:35,833 --> 00:14:37,834
but we always go out
for the black best friend.

353
00:14:37,961 --> 00:14:40,546
- Always, always.
Because-

354
00:14:40,672 --> 00:14:42,673
Always.

355
00:14:42,799 --> 00:14:44,049
Because you guys
understand

356
00:14:44,175 --> 00:14:46,176
that all groups
of three white men

357
00:14:46,302 --> 00:14:47,427
have to have
one black friend.

358
00:14:47,554 --> 00:14:48,595
- They have to.

359
00:14:48,721 --> 00:14:50,138
- That's a rule.

360
00:14:50,265 --> 00:14:52,975
- Well, he's cool,
it makes them cool.

361
00:14:53,101 --> 00:14:54,351
- Right, right.
- It makes them not racist.

362
00:14:54,477 --> 00:14:56,436
And, oh-
and the black best friend

363
00:14:56,563 --> 00:14:58,939
always has the same lines.

364
00:14:59,065 --> 00:15:02,693
I'm talking about, "Damn!"
- Mm-hmm.

365
00:15:02,819 --> 00:15:05,445
Or like, "No, he didn't!"
- Uh-huh.

366
00:15:05,572 --> 00:15:07,197
- And the ubiquitous 无处不在的...

367
00:15:07,323 --> 00:15:10,659
"Oh, hell no."

368
00:15:10,785 --> 00:15:12,411
- It's in every script
we ever see.

369
00:15:12,537 --> 00:15:13,745
And then, of course,
there are also

370
00:15:13,871 --> 00:15:15,581
phrases 短语 in every script
we always see.

371
00:15:15,707 --> 00:15:16,873
Here's one of 'em.

372
00:15:17,000 --> 00:15:20,877
"Now, ya all
clearly like each other.

373
00:15:21,004 --> 00:15:23,797
So go back over there
and get her digits 手指/数字, dog."

374
00:15:23,923 --> 00:15:25,257
- Mm-hmm.

375
00:15:25,383 --> 00:15:26,633
What about this one?
What about this one?

376
00:15:26,759 --> 00:15:31,054
"Man, you lucky
your pops 爸爸 cares about you.

377
00:15:34,058 --> 00:15:35,392
You lucky!"

378
00:15:35,518 --> 00:15:38,437
- It's ridiculous.

379
00:15:38,563 --> 00:15:41,231
Not every black person
is wise and reasonable.

380
00:15:41,357 --> 00:15:44,109
- Mm-mm, in fact,
for every Morgan Freeman,

381
00:15:44,235 --> 00:15:45,736
there's, like,
five Flavor Flavs.

382
00:15:45,862 --> 00:15:47,404
- That's true.

383
00:15:52,869 --> 00:15:54,870
Mm-mm. Mm-mm.

384
00:15:54,996 --> 00:15:56,038
Come on, man,
what you doin', man?

385
00:15:56,164 --> 00:15:57,831
That's stupid.

386
00:15:57,957 --> 00:16:00,250
That's stupid, man,
don't do that, man. Come on.

387
00:16:00,376 --> 00:16:01,877
- Are you kidding me?

388
00:16:02,003 --> 00:16:04,379
- Come on, man,
don't go in there!

389
00:16:04,505 --> 00:16:07,299
Do not go into
a crane shot 起重机镜头 right now!

390
00:16:07,425 --> 00:16:08,467
You kidding me?

391
00:16:08,593 --> 00:16:10,052
- Yeah, man, I hear y'all.

392
00:16:10,178 --> 00:16:13,639
This movie's got
a inconsistent visual language!

393
00:16:13,765 --> 00:16:15,682
- Half the time, this nigga
just shootin' all hand-held 手提式的

394
00:16:15,808 --> 00:16:17,809
like he
a dogma 教条 filmmaker!

395
00:16:17,935 --> 00:16:18,977
That's funny,

396
00:16:19,103 --> 00:16:20,687
since dogma
clearly forbids 禁止

397
00:16:20,813 --> 00:16:22,439
temporal 暂存的 and geographical 地理的
alienation 离间!

398
00:16:23,941 --> 00:16:26,318
- Oh, I loved that shot
the first time.

399
00:16:26,444 --> 00:16:28,695
When it was in Nosferatu!

400
00:16:28,821 --> 00:16:30,238
That's right.

401
00:16:30,365 --> 00:16:31,698
- I mean, this nigga
tryin' to do some homage 效忠

402
00:16:31,824 --> 00:16:33,700
to the German expressionists 表现主义的
or some shit!

403
00:16:33,826 --> 00:16:35,494
- It's a visual
medium 媒介, man!

404
00:16:35,620 --> 00:16:38,830
Enough with this My Dinner
with Andre bullshit!

405
00:16:38,956 --> 00:16:40,624
- I mean, has this dude
even heard of mise-en-scene 舞台布置?

406
00:16:40,750 --> 00:16:43,335
Put some information
up in the frame, bitch!

407
00:16:43,461 --> 00:16:46,088
- Oh, I get it. Yeah.
I'ma overexpose 使感光过度 the film.

408
00:16:46,214 --> 00:16:47,631
- Mm-hmm, mm-hmm.
- Get an ethereal 天上的 look.

409
00:16:47,757 --> 00:16:48,799
Man, fuck that shit!

410
00:16:48,925 --> 00:16:50,592
- Can we just go?

411
00:16:50,718 --> 00:16:53,845
- Hold on, honey, they're
actually making good points.

412
00:16:58,059 --> 00:17:01,311
Hey, has this guy watched too
much Quentin Tarantino 昆汀 塔伦蒂诺 or what?

413
00:17:01,437 --> 00:17:03,188
- Sir, we're gonna need you
to lower your voice.

414
00:17:03,314 --> 00:17:07,776
Too much talking, okay?
Thank you.

415
00:17:09,904 --> 00:17:11,488
I talked to him, sir.

416
00:17:11,614 --> 00:17:13,657
He's not gonna bother you
anymore.

417
00:17:13,783 --> 00:17:16,368
- Thank you.

418
00:17:23,167 --> 00:17:25,919
- So, I am a pacifist 和平主义者.
I'm against violence.

419
00:17:26,045 --> 00:17:28,630
- Well, except-with
the exception of video games.

420
00:17:28,756 --> 00:17:30,382
- Yes, with the exception
of video games.

421
00:17:30,508 --> 00:17:32,467
- And MMA.
- Oh, yeah.

422
00:17:32,593 --> 00:17:34,803
- And, uh, Game of Thrones.
- Oh, yeah.

423
00:17:34,929 --> 00:17:37,431
- And The Wire.
- Oh, yeah!

424
00:17:37,557 --> 00:17:39,099
- And slasher movies 暴力恐怖电影.
- Uh-huh.

425
00:17:39,225 --> 00:17:40,434
All right, let me rephrase 改述 it.
Let me rephrase it.

426
00:17:40,560 --> 00:17:42,436
I am against violence
involving me.

427
00:17:42,562 --> 00:17:43,603
- That's-that's clear.
- No, on-

428
00:17:43,730 --> 00:17:45,105
yeah, on TV and movies,

429
00:17:45,231 --> 00:17:46,356
that's where
it's supposed to be.

430
00:17:46,482 --> 00:17:47,858
Where no actual person
gets hurt.

431
00:17:47,984 --> 00:17:49,568
- I agree with that.
I agree with that.

432
00:17:49,694 --> 00:17:51,319
- Which is why I will not
slap you high five anymore.

433
00:17:51,446 --> 00:17:53,280
- Why-why?
- Because.

434
00:17:53,406 --> 00:17:55,490
There's that one time.

435
00:17:55,616 --> 00:17:57,325
He slapped my hand
so hard,

436
00:17:57,452 --> 00:17:59,077
my shit just turned
into a pink mist 薄雾.

437
00:18:01,164 --> 00:18:05,333
- It was-
We had done-

438
00:18:05,460 --> 00:18:06,960
We had done
something comedically

439
00:18:07,086 --> 00:18:08,837
that worked really well,
and I got excited.

440
00:18:08,963 --> 00:18:11,006
I came-I had to
get in there!

441
00:18:11,132 --> 00:18:12,674
- No, you came at-
- I had to get in there.

442
00:18:12,800 --> 00:18:16,136
- Show-just show-
Show me how you slap high five.

443
00:18:16,262 --> 00:18:17,596
- Okay, I'll do it
in slow motion.

444
00:18:17,722 --> 00:18:19,431
What happened
was I came up in here-

445
00:18:19,557 --> 00:18:21,016
I got my plant foot down.

446
00:18:21,142 --> 00:18:22,976
- See, don't-no plant foot.
No plant foot.

447
00:18:23,102 --> 00:18:24,561
- Just got
my plant foot down,

448
00:18:24,687 --> 00:18:26,271
and then I got
some torque 转力矩 in my hips 臀部.

449
00:18:26,397 --> 00:18:28,690
- See, what's the-
Why are you turning back here?

450
00:18:28,816 --> 00:18:30,400
- 'Cause I had to
get that shit!

451
00:18:30,526 --> 00:18:31,777
- No, man.
- I had-

452
00:18:31,903 --> 00:18:33,487
I got some torque,
like, in my hips...

453
00:18:33,613 --> 00:18:34,696
- No, this is-
none of this.

454
00:18:34,822 --> 00:18:35,989
- Went straight
Tiger Woods on it.

455
00:18:36,115 --> 00:18:37,407
- No, dude.
- Got the hips around.

456
00:18:37,533 --> 00:18:38,867
- Why you gotta be-
- I got the hips around.

457
00:18:38,993 --> 00:18:40,368
And then I just let-

458
00:18:40,495 --> 00:18:42,329
This shit was a whip.
Just a straight whip.

459
00:18:42,455 --> 00:18:43,997
And then...
- Aah!

460
00:18:44,123 --> 00:18:46,041
- And got in there.

461
00:18:46,167 --> 00:18:47,334
I got in there,
and then l-

462
00:18:47,460 --> 00:18:48,794
I know you need to run,
but come here.

463
00:18:48,920 --> 00:18:50,629
And then I got him here,

464
00:18:50,755 --> 00:18:52,547
and I had, like,
a follow-through like that.

465
00:18:52,673 --> 00:18:54,132
And I got through
like that.

466
00:19:15,863 --> 00:19:17,489
- You ready, homie 家庭似的?
- Hells yeah.

467
00:19:17,615 --> 00:19:19,616
- Let's take
these motherfuckers out.

468
00:19:19,742 --> 00:19:22,244
- Hold up.

469
00:19:22,370 --> 00:19:24,162
Nah...

470
00:19:24,288 --> 00:19:25,622
- What?

471
00:19:25,748 --> 00:19:27,749
- We gotta
go back home, man.

472
00:19:27,875 --> 00:19:29,167
- Why?

473
00:19:29,293 --> 00:19:31,545
- I pooped my pants.

474
00:19:31,671 --> 00:19:33,463
- What?
- Just now.

475
00:19:33,589 --> 00:19:35,757
I just pooped my pants.

476
00:19:35,883 --> 00:19:36,925
- You-you got diarrhea 腹泻?

477
00:19:37,051 --> 00:19:38,760
- Hell no, man,
I ain't gay.

478
00:19:38,886 --> 00:19:42,097
We gotta go home right now,
man, let me change my drawers 内裤.

479
00:19:42,223 --> 00:19:43,849
- Man, the dudes we trying
to smoke is right there.

480
00:19:43,975 --> 00:19:46,309
- Man, how you want me
to smoke somebody

481
00:19:46,435 --> 00:19:50,689
when I got poop
in my pants?

482
00:19:50,815 --> 00:19:51,982
- Are you sure
you pooped 'em?

483
00:19:52,108 --> 00:19:55,569
- Am I sure, nigga, am I sure?
- Yeah.

484
00:19:55,695 --> 00:19:56,736
- Yes.

485
00:19:56,863 --> 00:19:58,405
That is poop in my pants.

486
00:19:58,531 --> 00:20:00,740
- That's nasty 肮脏的, man.
- It's natural, man.

487
00:20:00,867 --> 00:20:03,743
- For a three-year-old.
Not for a hardcore gangsta.

488
00:20:03,870 --> 00:20:05,328
- See, if you were
my real friend, though,

489
00:20:05,454 --> 00:20:07,122
you wouldn't make
such a big deal out of it.

490
00:20:07,248 --> 00:20:08,874
- Wouldn't make
such a big deal out of it?

491
00:20:09,000 --> 00:20:13,461
Nigga, you a grown-ass man
who just shit in his drawers.

492
00:20:13,588 --> 00:20:15,380
- Oh, snap.

493
00:20:15,506 --> 00:20:17,549
- No, you didn't.

494
00:20:17,675 --> 00:20:20,010
Nigga, you just shit
your pants again.

495
00:20:20,136 --> 00:20:21,928
- Don't judge.

496
00:20:22,054 --> 00:20:23,263
- You need to get outta my car
and go to the hospital.

497
00:20:23,389 --> 00:20:25,307
- Okay, I see how it is.

498
00:20:25,433 --> 00:20:27,434
See, I wouldn't do that
to you, though.

499
00:20:27,560 --> 00:20:29,936
- You never would
have to do that to me,

500
00:20:30,062 --> 00:20:32,772
because under no circumstances
would I ever shit my pants.

501
00:20:34,066 --> 00:20:38,945
Uh-

502
00:20:39,071 --> 00:20:40,655
I'm sorry I judged you.

503
00:20:40,781 --> 00:20:43,700
- Apology accepted.

504
00:20:46,329 --> 00:20:49,039
- Thank you. Thank you.

505
00:20:49,165 --> 00:20:52,751
You're a champ.
Awesome show.

506
00:20:55,880 --> 00:20:57,964
Good night, everybody!
Thanks for coming out!

507
00:20:58,090 --> 00:21:00,967
- ♪ I'm gonna do
my one line here ♪

508
00:21:01,093 --> 00:21:03,595
- Oh, yeah.

509
00:21:05,932 --> 00:21:08,642
- Not gonna give me a high five?
- Ain't gonna happen, Jack.

510
00:21:08,768 --> 00:21:10,560
Why don't people call each other
Jack out of nowhere?

511
00:21:10,686 --> 00:21:13,229
- I miss Jack,
and I miss the word "say."

512
00:21:13,356 --> 00:21:15,649
Say, Rich, where you goin'?
I miss that.

513
00:21:15,775 --> 00:21:17,317
- That's just '70s shit...
"Say, Rich."

514
00:21:17,443 --> 00:21:18,526
- Say, Jack,
where you goin', man?

515
00:21:18,653 --> 00:21:19,694
- What's up, Jack?

