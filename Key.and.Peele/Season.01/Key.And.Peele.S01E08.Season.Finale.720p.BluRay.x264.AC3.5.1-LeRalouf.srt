﻿1
00:00:02,419 --> 00:00:03,836
- All right,
you're doing fine, sweetie.

2
00:00:03,962 --> 00:00:05,129
Now, Malia, let's just
go easy on the gas here

3
00:00:05,255 --> 00:00:06,756
and come to a complete stop.

4
00:00:06,882 --> 00:00:08,549
Hey!

5
00:00:08,676 --> 00:00:10,635
Okay, so in the future,
you want to try

6
00:00:10,761 --> 00:00:12,470
not to go through
stop signs.

7
00:00:15,057 --> 00:00:18,059
Okay, just remain calm now,

8
00:00:18,185 --> 00:00:20,019
and what you're gonna
want to do is pull up

9
00:00:20,145 --> 00:00:22,021
right here
next to the curb 边石.

10
00:00:22,147 --> 00:00:26,192
This is-this is why
we want to stay in the lines

11
00:00:26,318 --> 00:00:28,361
and we want to
follow rules, okay?

12
00:00:28,487 --> 00:00:29,570
Remember this.

13
00:00:32,950 --> 00:00:36,035
- License
and registration.

14
00:00:36,161 --> 00:00:38,663
Holy...
Mr. President!

15
00:00:38,789 --> 00:00:41,958
- Yeah, yeah, just, uh,
teaching Malia to drive here.

16
00:00:42,084 --> 00:00:44,210
- Well, you guys
ran a stop sign back there,

17
00:00:44,336 --> 00:00:46,379
but that's okay.
You can go.

18
00:00:46,505 --> 00:00:47,880
- Oh, no, no, no.
Malia's gotta learn

19
00:00:48,006 --> 00:00:49,424
what it's like to drive
in the real world here.

20
00:00:49,550 --> 00:00:51,426
This is called
consequences, Malia.

21
00:00:51,552 --> 00:00:54,178
Now, I want you to go ahead
and treat us like you would

22
00:00:54,304 --> 00:00:58,099
if I weren't
the President.

23
00:00:59,476 --> 00:01:01,310
Not exactly
what I had in mind.

24
00:01:27,171 --> 00:01:28,212
- Wow.

25
00:01:28,338 --> 00:01:29,464
Thank you.
- Thank you.

26
00:01:29,590 --> 00:01:32,008
Thank you so much.

27
00:01:32,134 --> 00:01:33,926
- All right.
- Welcome to the show.

28
00:01:34,052 --> 00:01:35,303
- What is up?
I'm Jordan.

29
00:01:35,429 --> 00:01:36,804
- I am Keegan,
and this is Key & Peele.

30
00:01:36,930 --> 00:01:38,681
- Yes.

31
00:01:38,807 --> 00:01:40,516
Thank you.

32
00:01:40,642 --> 00:01:43,478
- So Jordan and I have known
each other for quite some time,

33
00:01:43,604 --> 00:01:45,730
and there was a period of time
when we lived together

34
00:01:45,856 --> 00:01:49,025
for about seven months,
uh, in his apartment.

35
00:01:49,151 --> 00:01:51,611
How do l- What is the most
diplomatic 圆滑的 way

36
00:01:51,737 --> 00:01:52,779
to describe your apartment?
- Just say it.

37
00:01:52,905 --> 00:01:55,031
- Raw compost 混合肥料.
- Okay.

38
00:01:58,035 --> 00:02:00,077
It's not that bad.
- It's-it is-

39
00:02:00,204 --> 00:02:01,788
Dude, his kitchen-

40
00:02:01,914 --> 00:02:04,624
Your kitchen and your bathroom
are health hazards 危害.

41
00:02:04,750 --> 00:02:06,584
Once-Jordan, one time
I was in there wa-

42
00:02:06,710 --> 00:02:08,461
I went into the kitchen,
I said, "You know what?

43
00:02:08,587 --> 00:02:10,129
I'm gonna wash the dishes."

44
00:02:10,255 --> 00:02:12,965
And l-oh.
- Okay.

45
00:02:13,091 --> 00:02:14,425
- I, like, threw up
a little bit in my mouth

46
00:02:14,551 --> 00:02:15,593
right now just thinking
about your kitchen.

47
00:02:15,719 --> 00:02:17,929
- All right, no, I...

48
00:02:18,055 --> 00:02:23,476
I operate in
a very distinct ecosystem, okay?

49
00:02:23,602 --> 00:02:25,937
Yes, there is
a little bit of mold 霉菌,

50
00:02:26,063 --> 00:02:28,064
but the roaches 蟑螂
eat the mold, so...

51
00:02:28,190 --> 00:02:30,525
and...

52
00:02:30,651 --> 00:02:34,070
The mice eat the roaches.
I eat the mice.

53
00:02:34,196 --> 00:02:35,571
Nants ingonya-
Circle of life, bitch.

54
00:02:35,697 --> 00:02:38,074
That's all it is.

55
00:02:38,200 --> 00:02:39,867
That's my house.

56
00:02:39,993 --> 00:02:41,869
- Ooh, and that's halftime.

57
00:02:41,995 --> 00:02:43,871
- Oh!
- What?

58
00:02:43,997 --> 00:02:45,873
- Oh, my gosh!
- What a half!

59
00:02:45,999 --> 00:02:47,750
- That is crazy!
- Oh, my god.

60
00:02:47,876 --> 00:02:49,836
- Oh, my gosh.
- Hey, uh...

61
00:02:49,962 --> 00:02:51,796
forgot to tell ya,
congratulations

62
00:02:51,922 --> 00:02:53,548
on getting the new job.
- Oh, thanks, man.

63
00:02:53,674 --> 00:02:55,132
I appreciate it.
- Awesome, that's awesome.

64
00:02:55,259 --> 00:02:56,467
- Yeah, and thank you
for inviting me over here

65
00:02:56,593 --> 00:02:58,177
to the game today.
- Oh, no problem.

66
00:02:58,303 --> 00:02:59,846
I mean,
you're my best friend.

67
00:02:59,972 --> 00:03:01,055
- Wow, thanks, man.
Hey, you know what?

68
00:03:01,181 --> 00:03:02,223
You're my best friend too.

69
00:03:02,349 --> 00:03:03,432
- Cool.
- Yeah, yeah, yeah.

70
00:03:03,559 --> 00:03:06,561
Wrote a song about it.

71
00:03:06,687 --> 00:03:08,938
- Not gonna sing it or anything,
'cause that would be gay.

72
00:03:09,064 --> 00:03:10,857
That's pretty gay.

73
00:03:10,983 --> 00:03:12,233
- All right,
I'll give it a shot 试试.

74
00:03:18,240 --> 00:03:21,993
♪ Wally,
you're my best friend ♪

75
00:03:22,119 --> 00:03:25,246
♪ Together we can
change the world ♪

76
00:03:25,372 --> 00:03:28,124
♪ We've been together
through thick and thin ♪

77
00:03:28,250 --> 00:03:30,751
♪ Playing ball
and talking to girls ♪

78
00:03:30,878 --> 00:03:34,589
♪ Wally,
I'm so proud of you ♪

79
00:03:34,715 --> 00:03:38,384
♪ You know you've taken
all of my advice ♪

80
00:03:38,510 --> 00:03:40,386
- Wait, stop, stop, stop, stop.
What advice?

81
00:03:40,512 --> 00:03:44,765
- Oh, um...
it's just a song, dude.

82
00:03:44,892 --> 00:03:45,975
I mean, like,
can I finish?

83
00:03:46,101 --> 00:03:47,143
- Yep.
- And then we can?

84
00:03:47,269 --> 00:03:48,311
- O-okay.
- All right.

85
00:03:50,355 --> 00:03:53,149
♪ You've done everything
I've asked of you ♪

86
00:03:53,275 --> 00:03:56,569
♪ And you never,
ever questioned twice ♪

87
00:03:56,695 --> 00:03:58,154
- What are you
talking about?

88
00:03:58,280 --> 00:04:03,826
♪ Thank you for doing
what I told you to ♪

89
00:04:03,952 --> 00:04:08,956
♪ Your achievements
are also mine ♪

90
00:04:09,082 --> 00:04:10,625
♪ My padawan ♪

91
00:04:10,751 --> 00:04:13,169
♪ You make
your sensei proud ♪

92
00:04:13,295 --> 00:04:15,463
♪ Living your life
living your life ♪

93
00:04:15,589 --> 00:04:16,881
Two, three, four...

94
00:04:17,007 --> 00:04:21,802
♪ Living your life
to my design ♪

95
00:04:21,929 --> 00:04:24,931
- That was the weirdest 怪诞的 song
I've ever heard in my life, so-

96
00:04:25,057 --> 00:04:27,642
- ♪ You have listened
to your elders ♪

97
00:04:27,768 --> 00:04:29,226
- I'm older than you!
- ♪ Never forget ♪

98
00:04:29,353 --> 00:04:31,604
♪ The words I say ♪
- Okay, I, uh,

99
00:04:31,730 --> 00:04:33,022
don't like your song,
so I'm gonna bounce.

100
00:04:33,148 --> 00:04:34,941
- ♪ You are a product
of my creation ♪

101
00:04:35,067 --> 00:04:37,944
- What? No I'm not.
- ♪ I have shown you the way ♪

102
00:04:38,070 --> 00:04:40,112
- Except for that you haven't.
You haven't.

103
00:04:40,238 --> 00:04:41,864
- ♪ Modulate 调节 ♪

104
00:04:41,990 --> 00:04:43,407
- You just say
what you're doing?

105
00:04:43,533 --> 00:04:44,992
- ♪ Like the white wolf
guides his cubs 幼崽 ♪

106
00:04:45,118 --> 00:04:46,911
♪ Through the plains 平原
of the caribou 驯鹿 ♪

107
00:04:47,037 --> 00:04:50,039
- Shut up!

108
00:04:51,458 --> 00:04:55,294
- ♪ I have done
all I can ♪

109
00:04:55,420 --> 00:05:01,050
♪ The rest is up to ♪

110
00:05:01,176 --> 00:05:04,220
♪ You ♪

111
00:05:04,346 --> 00:05:05,930
My wolf cub
is all grown up.

112
00:05:11,812 --> 00:05:13,062
- Come on,
this is some-

113
00:05:13,188 --> 00:05:14,438
My man's been up there
ten minutes.

114
00:05:14,564 --> 00:05:16,023
- Okay, clear.

115
00:05:16,149 --> 00:05:18,025
Okay, the number pressing now.

116
00:05:18,151 --> 00:05:21,654
No, just hold on a second,
Malia, all right?

117
00:05:21,780 --> 00:05:24,490
Just gotta...

118
00:05:24,616 --> 00:05:27,368
Sorry, everyone,
it's taking so long.

119
00:05:27,494 --> 00:05:30,079
- Oh, my god, Obama!

120
00:05:30,205 --> 00:05:32,999
Obama!
How you doing, sir?

121
00:05:33,125 --> 00:05:34,500
- It's me.

122
00:05:34,626 --> 00:05:37,461
Just showing my daughter
how to use the ATM...

123
00:05:37,587 --> 00:05:38,879
having a little trouble
with the machine.

124
00:05:39,006 --> 00:05:40,172
- Oh, you having trouble
with the machine?

125
00:05:40,298 --> 00:05:41,424
Oh, man,
we'll chip in!

126
00:05:41,550 --> 00:05:42,591
We'll chip in
right now, man,

127
00:05:42,718 --> 00:05:43,759
then you can
be on your way!

128
00:05:43,885 --> 00:05:45,636
- No, no,
no, no, no.

129
00:05:45,762 --> 00:05:47,179
I'm trying to show Malia
what life is like

130
00:05:47,305 --> 00:05:48,973
in the real world here,
all right?

131
00:05:49,099 --> 00:05:50,683
So just do me a favor,
treat me like you would

132
00:05:50,809 --> 00:05:52,685
anyone else in this situation.
- Okay. Yes, sir.

133
00:05:52,811 --> 00:05:55,479
Yes, sir.
All right.

134
00:05:55,605 --> 00:05:57,481
Hurry up, man!
Is your tie 领结 stuck

135
00:05:57,607 --> 00:05:59,316
in the damn machine?
What are you doing, man?

136
00:05:59,443 --> 00:06:01,610
People trying
to get they money up here!

137
00:06:01,737 --> 00:06:03,696
- Not exactly
what I had in mind.

138
00:06:05,449 --> 00:06:07,199
- Go on then!

139
00:06:07,325 --> 00:06:09,118
Didn't even get your money.
- Dad! Dad!

140
00:06:09,244 --> 00:06:10,327
Dad! Dad!

141
00:06:12,706 --> 00:06:13,748
- Come on, girl,
hurry up, man.

142
00:06:13,874 --> 00:06:15,207
You got seven- Oh, okay, no.

143
00:06:15,333 --> 00:06:16,584
Take your time.
Take your time.

144
00:06:16,710 --> 00:06:18,210
Take your time.

145
00:06:24,134 --> 00:06:27,511
- Now, uh, Keegan and I always
have a fun time

146
00:06:27,637 --> 00:06:29,472
discussing the crappy 糟糕的 apartments
we've lived in...

147
00:06:29,598 --> 00:06:30,973
- Yes.
- In our lives.

148
00:06:31,099 --> 00:06:32,516
- Right, and I was
just telling you recently

149
00:06:32,642 --> 00:06:35,936
about how in '97
I moved into an apartment

150
00:06:36,063 --> 00:06:38,731
with two friends
in my hometown of Detroit.

151
00:06:38,857 --> 00:06:44,612
And, uh, we got broken into
five times in six weeks.

152
00:06:44,738 --> 00:06:46,113
It's gotta be a record.

153
00:06:46,239 --> 00:06:47,782
- I don't see
how that's possible.

154
00:06:47,908 --> 00:06:50,451
- Yeah, uh, it's Detroit,
so...

155
00:06:50,577 --> 00:06:52,453
And the first time
we got broken into...

156
00:06:52,579 --> 00:06:53,829
- Mm-hmm.
- I mean, the cops came in,

157
00:06:53,955 --> 00:06:56,123
they're examining
the apartment,

158
00:06:56,249 --> 00:06:57,833
and they're making their way
out of the apartment.

159
00:06:57,959 --> 00:07:02,838
There is a bloody fingerprint
on the wall.

160
00:07:02,964 --> 00:07:05,466
And so I said to the cop,
"Um, excuse me, you know,

161
00:07:05,592 --> 00:07:07,718
"I just wanted to point out
that there's a fingerprint here

162
00:07:07,844 --> 00:07:09,595
if maybe you wanted
to dust that or something."

163
00:07:09,721 --> 00:07:11,180
- Mm-hmm, straight evidence.
- Yeah.

164
00:07:11,306 --> 00:07:13,682
This is what he says to me.
He goes, "Aw, naw, man.

165
00:07:13,809 --> 00:07:15,142
That-that's just on TV."

166
00:07:17,938 --> 00:07:20,940
What?

167
00:07:23,777 --> 00:07:25,402
- What are we doing
in this neighborhood?

168
00:07:25,529 --> 00:07:27,988
- Come on, this place
is in our price range.

169
00:07:28,115 --> 00:07:29,657
Besides, I mean,
people say the neighborhood's

170
00:07:29,783 --> 00:07:33,577
turning around.
- Who the fuck here?

171
00:07:33,703 --> 00:07:35,121
It's Jordan and Keira.

172
00:07:35,247 --> 00:07:40,251
We're here to view
the apartment at 3:00.

173
00:07:40,377 --> 00:07:43,629
Oh, shit, my bad.

174
00:07:43,755 --> 00:07:46,340
I had this homeless crackhead
trying to set up house

175
00:07:46,466 --> 00:07:49,510
in here yesterday.
Almost had to beat his ass, man.

176
00:07:49,636 --> 00:07:50,970
But y'all ain't got nothing
to worry about,

177
00:07:51,096 --> 00:07:52,596
'cause anybody
try to get up in here,

178
00:07:52,722 --> 00:07:54,765
I will murder them.

179
00:07:54,891 --> 00:07:56,684
Let me give
y'all the tour.

180
00:07:56,810 --> 00:07:58,894
All right, so we got
these vintage 古香古色的 windows.

181
00:07:59,020 --> 00:08:00,855
They been here as long as
the building's been here.

182
00:08:00,981 --> 00:08:02,523
Oh, shit!

183
00:08:02,649 --> 00:08:04,316
Get away from the window!
Get away from the window!

184
00:08:04,442 --> 00:08:06,026
Hey! Hey!

185
00:08:06,153 --> 00:08:08,112
You, motherfucker!
I see you out there!

186
00:08:08,238 --> 00:08:10,489
You get out that car,
and I will murder you!

187
00:08:11,783 --> 00:08:13,033
That's what I thought!

188
00:08:13,160 --> 00:08:15,119
So roll on,
little nigga!

189
00:08:15,245 --> 00:08:18,122
Roll on!

190
00:08:18,248 --> 00:08:20,374
My nephew, Craig.

191
00:08:20,500 --> 00:08:22,585
He a good kid.

192
00:08:22,711 --> 00:08:26,172
He smoke crack, though.

193
00:08:28,133 --> 00:08:29,758
Just, uh-Just in case
he come back.

194
00:08:29,885 --> 00:08:31,177
- Uh...
- And I filed the serial number

195
00:08:31,303 --> 00:08:32,970
off that,
so if you gotta shoot anybody,

196
00:08:33,096 --> 00:08:34,513
just make sure
to wipe your fingerprints

197
00:08:34,639 --> 00:08:37,057
off this motherfucker.

198
00:08:37,184 --> 00:08:40,102
Well, come on, y'all.
I ain't gonna bite you.

199
00:08:40,228 --> 00:08:42,021
Brand-new cupboards 柜橱.

200
00:08:42,147 --> 00:08:44,231
I put the contact paper
in myself here

201
00:08:44,357 --> 00:08:46,108
so y'all don't get no germs.

202
00:08:46,234 --> 00:08:47,276
Get down!

203
00:08:52,741 --> 00:08:54,283
Nah, ain't no thing.

204
00:08:54,409 --> 00:08:56,577
I thought Craig
was rolling back.

205
00:08:56,703 --> 00:08:59,413
I ain't getting shot by
this motherfucker twice, damn.

206
00:08:59,539 --> 00:09:02,499
While we down here,
check this out.

207
00:09:02,626 --> 00:09:03,959
Original hardwood 硬木 floor

208
00:09:04,085 --> 00:09:05,586
from when the building
was built.

209
00:09:05,712 --> 00:09:07,671
Check out
this grain 纹理, brother.

210
00:09:07,797 --> 00:09:09,798
Whoo!
That is craftsmanship 技巧!

211
00:09:09,925 --> 00:09:10,966
Anyway, come on.

212
00:09:14,304 --> 00:09:17,973
Now, this paint
is all new, and-

213
00:09:18,099 --> 00:09:20,517
Aw, shit.

214
00:09:20,644 --> 00:09:23,312
Okay, this-this happened
after I had painted.

215
00:09:23,438 --> 00:09:25,981
Funny story, we got crackheads
in this neighborhood,

216
00:09:26,107 --> 00:09:28,442
and one of them lived
right next door here, right?

217
00:09:28,568 --> 00:09:30,903
And one day, he was smoking
crack, just went looney 发狂的 tunes 曲调

218
00:09:31,029 --> 00:09:33,113
and mistook his 9mm-

219
00:09:33,240 --> 00:09:35,199
that's a pistol, honey-
for the remote control.

220
00:09:35,325 --> 00:09:37,243
But don't worry, you ain't
never gonna see him again.

221
00:09:37,369 --> 00:09:40,955
Ain't nobody ever...

222
00:09:41,081 --> 00:09:43,457
gonna see him...

223
00:09:43,583 --> 00:09:46,752
again.

224
00:09:46,878 --> 00:09:48,879
Plus, I'm gonna
put a fancy 奇特的 little hook in there

225
00:09:49,005 --> 00:09:50,589
and turn it
into a hat rack 行李架.

226
00:09:50,715 --> 00:09:53,676
No let's take a look
at this bedroom.

227
00:09:53,802 --> 00:09:55,094
What the fuck?

228
00:09:55,220 --> 00:09:56,720
- I found this place first!

229
00:09:56,846 --> 00:09:58,847
- Found it?
Man, I own this place!

230
00:09:58,974 --> 00:10:00,391
Can y'all give me a second?

231
00:10:00,517 --> 00:10:03,560
Aah!

232
00:10:03,687 --> 00:10:05,145
You gonna bite me,
motherfucker?

233
00:10:05,272 --> 00:10:06,897
I will cut you
with my sword cane 手帐!

234
00:10:07,023 --> 00:10:10,192
Aah!

235
00:10:13,446 --> 00:10:15,864
- So that's gonna be first month
and last month's rent

236
00:10:15,991 --> 00:10:17,157
and one-month
security deposit.

237
00:10:17,284 --> 00:10:20,911
I'll get
the paperwork started.

238
00:10:23,790 --> 00:10:27,376
- Okay, baby,
let's hit this rock real quick.

239
00:10:29,713 --> 00:10:32,881
- And now we have
a special treat.

240
00:10:33,008 --> 00:10:35,926
Hold on
to your yarmulkes 圆顶小帽, okay?

241
00:10:36,052 --> 00:10:39,346
"From east to west,
you know they're the best.

242
00:10:39,472 --> 00:10:41,974
Getting you in the zone
for your milestone."

243
00:10:42,100 --> 00:10:43,392
Is that cute?

244
00:10:43,518 --> 00:10:47,771
Please welcome Gafilta Fresh
and Dr. Dreidel!

245
00:10:47,897 --> 00:10:52,067
- Yeah!

246
00:10:52,193 --> 00:10:54,653
What's up,
Temple Beth Israel?

247
00:10:54,779 --> 00:10:56,864
- Yo, is there
a Daniel Rosenblum

248
00:10:56,990 --> 00:10:58,365
in the hizzouse?

249
00:10:58,491 --> 00:10:59,742
- Oh, yeah!
- What's up, man?

250
00:10:59,868 --> 00:11:01,535
- Aw, yeah!
What's up, my man?

251
00:11:01,661 --> 00:11:03,370
Yeah, all right!
- All right!

252
00:11:06,666 --> 00:11:08,459
- When you see
black people at a bar mitzvah,

253
00:11:08,585 --> 00:11:09,626
it's very exciting.

254
00:11:09,753 --> 00:11:11,211
It's like a scary 吓人的 ride.

255
00:11:11,338 --> 00:11:12,796
And the kids just love it.

256
00:11:12,922 --> 00:11:14,423
- Right now,
we are the number-one

257
00:11:14,549 --> 00:11:17,551
bar/bat mitzvah party motivators
in all of Nassau County.

258
00:11:17,677 --> 00:11:20,846
- I mean, you got
the Bat Mizzles, you know?

259
00:11:20,972 --> 00:11:23,349
Oys to Men, you know?
All of them coming through

260
00:11:23,475 --> 00:11:25,642
like

261
00:11:25,769 --> 00:11:27,269
With the recycled dreck 废物.

262
00:11:27,395 --> 00:11:28,854
- And I mean,
the chutzpah 肆无忌惮 of these guys

263
00:11:28,980 --> 00:11:30,773
with their fakakta shtick 滑稽场面.

264
00:11:30,899 --> 00:11:32,858
It's a real shanda 盛大
on what it's supposed to be.

265
00:11:32,984 --> 00:11:34,526
- A simcha.
- Yeah.

266
00:11:34,652 --> 00:11:36,487
- It's supposed to be a simcha.
- Mm-hmm.

267
00:11:36,613 --> 00:11:38,322
- You just really
can't put a price

268
00:11:38,448 --> 00:11:39,948
on the look
on your child's face

269
00:11:40,075 --> 00:11:42,159
when they see a black person
for the first time.

270
00:11:42,285 --> 00:11:44,745
It's...

271
00:11:44,871 --> 00:11:48,665
It's just magical, really.

272
00:11:48,792 --> 00:11:50,250
Is he getting back up or what?

273
00:11:50,377 --> 00:11:51,668
- We are not
technically chosen.

274
00:11:51,795 --> 00:11:53,379
- We're chosen
by the chosen.

275
00:11:53,505 --> 00:11:55,964
- Which is kind of better
than being just the chosen.

276
00:11:56,091 --> 00:11:58,592
- That's right.
It's less responsibility.

277
00:11:58,718 --> 00:11:59,760
- Jews 犹太人 are like black people,

278
00:11:59,886 --> 00:12:01,011
but we were black people

279
00:12:01,137 --> 00:12:02,930
before there were
black people.

280
00:12:03,056 --> 00:12:04,556
Um... well, I guess
that's not true.

281
00:12:04,682 --> 00:12:05,849
They were just in a...

282
00:12:05,975 --> 00:12:07,184
Different part of,
uh, the world.

283
00:12:07,310 --> 00:12:09,520
Oh, black people
coming to get me! Help!

284
00:12:14,067 --> 00:12:15,692
- Yeah, you know it!

285
00:12:19,406 --> 00:12:22,032
We find it
spiritually 在精神上地 intoxicating 令人陶醉的,

286
00:12:22,158 --> 00:12:24,910
we find it
culturally fascinating 迷人的,

287
00:12:25,036 --> 00:12:27,621
and we find it
monetarily 财政上 exhilarating 令人振奋的.

288
00:12:27,747 --> 00:12:30,457
♪ When we say Sarah
you say Schwartzman ♪

289
00:12:30,583 --> 00:12:31,667
- Sarah!
- Schwartzman.

290
00:12:31,793 --> 00:12:32,835
- Sarah!
- Schwartzman.

291
00:12:32,961 --> 00:12:34,503
- Yeah!
- There you go, yeah!

292
00:12:34,629 --> 00:12:36,380
- I mean, the fact
that we're willing to put forth 向前

293
00:12:36,506 --> 00:12:38,715
the extra time
is what sets us apart.

294
00:12:38,842 --> 00:12:40,926
- I mean, how often
is a 13-year-old

295
00:12:41,052 --> 00:12:43,345
gonna hear their own name?

296
00:12:43,471 --> 00:12:46,515
- We learn that name.
We rehearse 排练 it.

297
00:12:46,641 --> 00:12:49,810
Go, Daniel!
Go, Daniel!

298
00:12:49,936 --> 00:12:53,188
- Judaism 犹太教 can be
a very somber 忧郁的 religion 宗教.

299
00:12:53,314 --> 00:12:54,731
- That's right.
You know, something

300
00:12:54,858 --> 00:12:58,318
we learned along the way
is that the word "ghetto" 犹太人区

301
00:12:58,445 --> 00:13:01,947
means something very different
to these people.

302
00:13:02,073 --> 00:13:03,657
- To tell you the truth,
I was a little nervous

303
00:13:03,783 --> 00:13:05,826
about the whole thing.
No, not because they were black,

304
00:13:05,952 --> 00:13:07,786
but because
you never really know

305
00:13:07,912 --> 00:13:09,872
what those kind of people
are gonna do.

306
00:13:09,998 --> 00:13:13,417
- If we can add some fun,
some life, some color

307
00:13:13,543 --> 00:13:17,713
to a Jewish person's life
when they're still young-

308
00:13:17,839 --> 00:13:19,339
- They might not grow up
and run away from black people

309
00:13:19,466 --> 00:13:22,217
when they see them.
- No, maybe they won't...

310
00:13:22,343 --> 00:13:25,888
- Or, uh...
- Fire people so willy-nilly 随意地

311
00:13:26,014 --> 00:13:28,223
when they're older.

312
00:13:28,349 --> 00:13:32,186
- We provide a service
to these people.

313
00:13:34,522 --> 00:13:35,606
Yeah!

314
00:13:35,732 --> 00:13:37,691
We motivate parties.

315
00:13:37,817 --> 00:13:39,276
- That's we do.

316
00:13:39,402 --> 00:13:42,070
- That's we do.
We motivate parties.

317
00:13:52,957 --> 00:13:55,459
- So we're fascinated
with movies these days,

318
00:13:55,585 --> 00:13:57,044
because movies
have gone crazy.

319
00:13:57,170 --> 00:13:58,712
- Oh, yeah.
- They have the 3-D,

320
00:13:58,838 --> 00:14:00,589
amazing effects,
amazing sound.

321
00:14:00,715 --> 00:14:02,633
They spend so much money,
and it looks so good.

322
00:14:02,759 --> 00:14:05,928
Why can't they just get the
simple shit right, though?

323
00:14:06,054 --> 00:14:07,137
You know what
I'm talking about?

324
00:14:07,263 --> 00:14:09,306
- The next time
you go to a movie,

325
00:14:09,432 --> 00:14:10,766
when you watch
somebody on the phone,

326
00:14:10,892 --> 00:14:13,101
I guarantee you
they will not say good-bye.

327
00:14:13,228 --> 00:14:15,729
- Yes.
- It won't happen.

328
00:14:15,855 --> 00:14:19,358
"Honey, I promise you
that I will be home

329
00:14:19,484 --> 00:14:21,860
for you and the kids
on Thanksgiving."

330
00:14:24,531 --> 00:14:26,073
- You can't...

331
00:14:26,199 --> 00:14:28,116
you can't
get away with that.

332
00:14:28,243 --> 00:14:29,910
You never see the scene
where somebody calls back

333
00:14:30,036 --> 00:14:31,119
talking about...

334
00:14:31,246 --> 00:14:32,287
"Why'd you hang up on me?"

335
00:14:32,413 --> 00:14:33,789
- "Oh, I was just... what?

336
00:14:33,915 --> 00:14:35,082
I didn't...
what are you talking about?"

337
00:14:37,210 --> 00:14:40,963
- Or my favorite is after
a shower, or love-making,

338
00:14:41,089 --> 00:14:43,799
how come every person
in the film, when they get out,

339
00:14:43,925 --> 00:14:46,134
just immediately put this...
- Towel 毛巾, towel.

340
00:14:46,261 --> 00:14:48,554
Talking about
in your own house by yourself!

341
00:14:48,680 --> 00:14:49,972
No.

342
00:14:50,098 --> 00:14:51,682
I get out of the shower,
I'm ass-naked

343
00:14:51,808 --> 00:14:52,849
all over the place.
- Yes!

344
00:14:52,976 --> 00:14:54,351
Oh, dude, if I'm home alone,

345
00:14:54,477 --> 00:14:56,311
I will put my balls
on a bookshelf.

346
00:14:56,437 --> 00:14:57,854
I don't care.

347
00:14:57,981 --> 00:15:01,191
I'll just make something up
because I can.

348
00:15:07,115 --> 00:15:08,407
Please! No!

349
00:15:08,533 --> 00:15:12,411
No!

350
00:15:23,965 --> 00:15:25,757
- What-what's the joke?

351
00:15:25,883 --> 00:15:26,925
- What?

352
00:15:27,051 --> 00:15:28,594
- L-I don't get it.

353
00:15:28,720 --> 00:15:30,429
- Man, you shot
that damn fool!

354
00:15:34,934 --> 00:15:37,603
- Oh...

355
00:15:37,729 --> 00:15:39,313
yeah, no.
I still don't get it.

356
00:15:39,439 --> 00:15:40,606
- Hey, man,
ain't nothing to get.

357
00:15:40,732 --> 00:15:43,358
Shit is just funny, man.

358
00:15:43,484 --> 00:15:44,776
Dude can't snitch 告密 now.

359
00:15:52,493 --> 00:15:53,535
- Man, don't fake laugh.

360
00:15:53,661 --> 00:15:54,995
- Huh?

361
00:15:55,121 --> 00:15:57,623
- That's not real.
I can tell.

362
00:15:57,749 --> 00:15:59,708
- No, I think
it's really funny.

363
00:15:59,834 --> 00:16:01,168
- No, you don't.

364
00:16:01,294 --> 00:16:04,379
Look at him!
Sitting there all dead.

365
00:16:09,677 --> 00:16:13,347
He was 20,
and now he's dead.

366
00:16:13,473 --> 00:16:15,849
Ain't never gonna have
a wife or kids now.

367
00:16:15,975 --> 00:16:18,602
- Man, what are you doing?
Wife and kids?

368
00:16:18,728 --> 00:16:20,187
That shit ain't funny.

369
00:16:22,899 --> 00:16:26,234
His mom and dad ain't
never gonna see him again.

370
00:16:26,361 --> 00:16:29,112
- No. No.

371
00:16:29,238 --> 00:16:30,822
You're ruining 毁灭 it for me.

372
00:16:30,948 --> 00:16:33,283
- Okay, well, I guess
I just don't get it then.

373
00:16:33,409 --> 00:16:34,785
- It's just
that you shot him.

374
00:16:34,911 --> 00:16:36,286
That's all!
That's the whole thing!

375
00:16:36,412 --> 00:16:37,954
- Oh, okay, so...

376
00:16:38,081 --> 00:16:39,373
- Oh!

377
00:16:39,499 --> 00:16:41,041
The hell
is wrong with you?

378
00:16:41,167 --> 00:16:42,417
You don't shoot a dude
that's already dead!

379
00:16:42,543 --> 00:16:43,585
That's not funny!

380
00:16:43,711 --> 00:16:46,755
- Oh.
Aah!

381
00:16:46,881 --> 00:16:50,676
Oww!

382
00:16:50,802 --> 00:16:53,095
- Yeah, I guess
I just don't get it.

383
00:16:58,393 --> 00:17:01,353
- So you have to tell
every human being in the world

384
00:17:01,479 --> 00:17:04,481
that their baby is cute
whether it is true or not.

385
00:17:04,607 --> 00:17:06,983
You just have to.

386
00:17:07,110 --> 00:17:08,735
- Keegan Michael-Key,
nicest guy in the world,

387
00:17:08,861 --> 00:17:10,320
just outed himself
as somebody who doesn't

388
00:17:10,446 --> 00:17:11,697
find all babies cute.

389
00:17:11,823 --> 00:17:12,864
- Well, I mean,
what am I supposed to do?

390
00:17:12,990 --> 00:17:14,032
Go down the street,
you know,

391
00:17:14,158 --> 00:17:15,200
you see a lady
with her stroller 婴儿车.

392
00:17:15,326 --> 00:17:16,576
"Oh, hi."
And you peer in,

393
00:17:16,703 --> 00:17:18,412
and there's just, like,
an infant 婴儿 body

394
00:17:18,538 --> 00:17:20,205
and then Steve Buscemi's head.
- Yeah, you don't...

395
00:17:23,751 --> 00:17:24,835
- That's rough.
You can't go,

396
00:17:24,961 --> 00:17:26,420
"whoa!"

397
00:17:26,546 --> 00:17:28,213
- No, yeah.
- That's mean.

398
00:17:28,339 --> 00:17:30,382
- Yeah, you don't want to
be picking up a little baby

399
00:17:30,508 --> 00:17:31,883
with the head
of Newt Gingrich,

400
00:17:32,009 --> 00:17:33,802
'cause that's
just unattractive.

401
00:17:33,928 --> 00:17:35,512
You don't want to see that.

402
00:17:35,638 --> 00:17:38,098
- I don't want to see
a lady breastfeeding 哺乳, you know,

403
00:17:38,224 --> 00:17:39,808
just right here...

404
00:17:39,934 --> 00:17:40,976
Danny Devito.
- Yeah.

405
00:17:42,979 --> 00:17:45,147
- There's nothing cute about
a 3-foot-tall Forest Whitaker.

406
00:17:45,273 --> 00:17:47,607
- No, there isn't.

407
00:17:47,734 --> 00:17:49,109
No, there's not.

408
00:17:49,235 --> 00:17:52,612
- Talking about
"Ga-ga goo-goo."

409
00:17:54,949 --> 00:17:57,909
"Ga-ga goo-goo."

410
00:17:58,035 --> 00:18:00,579
- Mrs. Whitaker, I have
been babysitting for 20 years.

411
00:18:00,705 --> 00:18:01,997
I've seen it all.

412
00:18:02,123 --> 00:18:05,751
- The thing is,
Forest is a unique child.

413
00:18:05,877 --> 00:18:07,127
- Everything
is gonna be fine.

414
00:18:07,253 --> 00:18:08,587
I want you to have
a good time tonight.

415
00:18:08,713 --> 00:18:10,046
You deserve it.

416
00:18:10,173 --> 00:18:12,424
- Okay, um...

417
00:18:12,550 --> 00:18:16,386
good luck.
- Thank you.

418
00:18:29,025 --> 00:18:34,905
- Goo-goo ga-ga.

419
00:18:37,450 --> 00:18:39,951
Hey there, Forest.

420
00:18:40,077 --> 00:18:41,828
- Goo-goo ga-ga.

421
00:18:41,954 --> 00:18:43,914
I want...

422
00:18:44,040 --> 00:18:45,791
I want milk.

423
00:18:45,917 --> 00:18:47,793
- You want me to get you
your bottle, buddy?

424
00:18:47,919 --> 00:18:51,463
- I want milk!
- Okay.

425
00:18:55,218 --> 00:18:58,136
- This is a ba-ba.

426
00:18:58,262 --> 00:19:00,222
I want mama's milk.

427
00:19:00,348 --> 00:19:03,141
Why can't I have
mama's milk?

428
00:19:03,267 --> 00:19:04,518
It's a simple request.

429
00:19:04,644 --> 00:19:06,353
- Well, yeah, well, uh,

430
00:19:06,479 --> 00:19:08,897
y-you can't have mama's milk
right now, buddy.

431
00:19:09,023 --> 00:19:10,315
Um, how about we play?

432
00:19:10,441 --> 00:19:12,150
- Okay.

433
00:19:12,276 --> 00:19:13,819
I want...

434
00:19:13,945 --> 00:19:15,946
I want-I wanna play
with my Legos.

435
00:19:16,072 --> 00:19:17,447
- Okay, yeah.

436
00:19:17,573 --> 00:19:21,117
Okay, buddy, let's, uh,
get your Legos then.

437
00:19:21,244 --> 00:19:22,661
- I wanna...

438
00:19:22,787 --> 00:19:25,497
I wanna put
the smallest Lego in my mouth.

439
00:19:25,623 --> 00:19:26,998
That's what I'm gonna do.
- No, no, no.

440
00:19:27,124 --> 00:19:29,376
You can't-you can't do that.
- What?

441
00:19:29,502 --> 00:19:31,753
First you tell me
I can't have mama's milk.

442
00:19:31,879 --> 00:19:35,507
Now I can't have
the smallest Lego in my mouth?

443
00:19:35,633 --> 00:19:37,717
What a night.

444
00:19:37,844 --> 00:19:39,761
This is a bad start.

445
00:19:39,887 --> 00:19:41,972
- Wait, but Forest,
it's dangerous.

446
00:19:42,098 --> 00:19:43,348
- Let me get this straight.

447
00:19:43,474 --> 00:19:46,768
You're in charge of me?
In my house?

448
00:19:46,894 --> 00:19:48,228
- All right, F-Forest,
i-it's time for bed.

449
00:19:48,354 --> 00:19:51,273
- I guess then
you gonna sing me a song.

450
00:19:51,399 --> 00:19:52,941
- What?

451
00:19:53,067 --> 00:19:55,944
- Mama always sings me a song
before I go to sleep.

452
00:19:56,070 --> 00:19:57,779
Are you gonna
do that for me?

453
00:19:57,905 --> 00:19:59,656
- Okay, okay.
Yeah.

454
00:19:59,782 --> 00:20:02,492
Yes, I will.

455
00:20:02,618 --> 00:20:06,538
♪ Hush, little baby
don't say a word ♪

456
00:20:06,664 --> 00:20:09,082
- Keep-keep singing.

457
00:20:09,208 --> 00:20:12,627
- ♪ Mama's gonna
buy you a mockingbird ♪

458
00:20:12,753 --> 00:20:15,046
- Put my name in it.

459
00:20:15,172 --> 00:20:16,882
I like-I like it personal.

460
00:20:17,008 --> 00:20:21,970
- ♪ Hush, baby Forest Whitaker
don't say a word ♪

461
00:20:22,096 --> 00:20:23,513
- Goo-goo ga-ga.

462
00:20:23,639 --> 00:20:27,100
- ♪ Mama's gonna
buy you a mockingbird ♪

463
00:20:27,226 --> 00:20:28,727
- Good babysitter.

464
00:20:28,853 --> 00:20:32,063
- ♪ And if that mockingbird
don't sing ♪

465
00:20:32,189 --> 00:20:34,274
- Have a good cry.

466
00:20:34,400 --> 00:20:36,234
♪ Mama's gonna buy you ♪

467
00:20:36,360 --> 00:20:38,111
♪ A diamond ring ♪

468
00:20:44,869 --> 00:20:46,786
- We have had
a great show tonight.

469
00:20:46,913 --> 00:20:48,121
- Yes.
- Uh...

470
00:20:48,247 --> 00:20:49,915
so great that
I wrote a song about it.

471
00:20:50,041 --> 00:20:51,207
- Nope. Nope.
No, no, no, no.

472
00:20:51,334 --> 00:20:54,002
Thank you.
Good night, everybody!

473
00:20:54,128 --> 00:20:55,378
- You don't want to hear it?

474
00:20:55,504 --> 00:20:57,672
- I don't want
to hear the song.

475
00:20:57,798 --> 00:21:00,675
- ♪ I'm gonna do
my one line here ♪

476
00:21:00,801 --> 00:21:03,345
- Oh, yeah.

477
00:21:05,765 --> 00:21:07,265
- We got broken into
four more times after that.

478
00:21:07,391 --> 00:21:08,475
- Basically, every junkie
in the neighborhood

479
00:21:08,601 --> 00:21:09,643
was coming on.
- Oh, no, no, no.

480
00:21:09,769 --> 00:21:12,062
Same guy each time.

481
00:21:12,188 --> 00:21:14,522
It was the same guy
five times in a row.

482
00:21:14,649 --> 00:21:16,274
- Basically,
you were this guy's Best Buy.

483
00:21:16,400 --> 00:21:19,694
- Yeah, mm-hmm. Yeah.
- I love it.

