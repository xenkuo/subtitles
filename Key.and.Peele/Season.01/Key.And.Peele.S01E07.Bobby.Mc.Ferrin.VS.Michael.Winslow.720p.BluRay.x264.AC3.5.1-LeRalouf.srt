﻿1
00:00:01,001 --> 00:00:02,585
- It's like a low-risk
opportunity.

2
00:00:02,711 --> 00:00:04,379
So I'm looking forward
to it, we'll see.

3
00:00:04,505 --> 00:00:06,589
All right, gentlemen.
Take care. See you soon.

4
00:00:06,715 --> 00:00:08,216
Good looking out.
Take care.

5
00:00:08,342 --> 00:00:09,926
All right, man.
All right, take care.

6
00:00:10,052 --> 00:00:12,470
See you.
Ooh.

7
00:00:17,101 --> 00:00:19,602
I'm a lefty 左撇子, so-

8
00:00:43,127 --> 00:00:45,545
Yeah!

9
00:00:45,671 --> 00:00:46,879
- Whoo!

10
00:00:47,005 --> 00:00:50,675
- All right, yeah.
- All right!

11
00:00:51,885 --> 00:00:54,303
- Thank you.

12
00:00:54,430 --> 00:00:56,973
- Thank you!

13
00:00:57,099 --> 00:00:59,559
Thanks so much for coming out.
I am Keegan.

14
00:00:59,685 --> 00:01:01,394
- And I am Jordan.
- And this is Key and Peele.

15
00:01:01,520 --> 00:01:04,772
- Welcome to the show.
- Welcome.

16
00:01:04,898 --> 00:01:06,357
- All right.

17
00:01:06,483 --> 00:01:08,693
- So Jordan and I
have this ongoing competition

18
00:01:08,819 --> 00:01:10,486
about which one of us
is blacker, yes.

19
00:01:10,612 --> 00:01:14,741
- Haha.
Clearly me, clearly me.

20
00:01:14,867 --> 00:01:16,409
- No, no, how is that
even possible?

21
00:01:16,535 --> 00:01:18,745
I'm from one of the blackest
parts of the planet.

22
00:01:18,871 --> 00:01:21,748
- Michigan?
- Yeah.

23
00:01:21,874 --> 00:01:23,624
I'm from Detroit, okay?

24
00:01:23,751 --> 00:01:26,669
Detroit's black enough
for the whole state, all right?

25
00:01:26,795 --> 00:01:29,005
We had a mayor 市长 who used to have
a nameplate

26
00:01:29,131 --> 00:01:31,466
on his desk
that said "HNIC."

27
00:01:31,592 --> 00:01:32,884
For those of you who don't
know what that means,

28
00:01:33,010 --> 00:01:34,969
it means,
"Head Nigga In Charge."

29
00:01:35,095 --> 00:01:36,804
The mayor.
- Okay, okay.

30
00:01:36,930 --> 00:01:39,182
- Whatever, man.
I'm from NYC, baby, NYC.

31
00:01:39,308 --> 00:01:40,600
- Oh, okay.

32
00:01:40,726 --> 00:01:42,852
- Straight up.

33
00:01:42,978 --> 00:01:46,397
Ah ha ha!

34
00:01:46,523 --> 00:01:47,940
- No, no, no, no.

35
00:01:48,066 --> 00:01:49,859
- It's the blackest town
in the world, baby.

36
00:01:49,985 --> 00:01:51,235
- Jordan, why don't-
do you care to tell

37
00:01:51,361 --> 00:01:52,612
the audience
where you are from in NYC?

38
00:01:52,738 --> 00:01:54,155
- W-okay.

39
00:01:54,281 --> 00:01:55,698
- Tell them where you're from
in NYC, exactly.

40
00:01:55,824 --> 00:01:57,366
The exact kinda location
of where you're from.

41
00:01:57,493 --> 00:01:58,910
- Manhattan. I'm from Manhattan.
- No, no, no, no.

42
00:01:59,036 --> 00:02:00,119
Where exactly in Manhattan
do you live, Jordan?

43
00:02:00,245 --> 00:02:01,454
Where in Manhattan
did you grow up?

44
00:02:01,580 --> 00:02:05,541
Where you grew up?
Where you grew up?

45
00:02:05,667 --> 00:02:07,919
- Upper west side,
but still.

46
00:02:08,045 --> 00:02:09,921
Yo, check it.
All right, all right.

47
00:02:10,047 --> 00:02:12,215
Dude, NYC, baby.
- Uh-huh.

48
00:02:12,341 --> 00:02:15,134
- Home of Alvin Ailey,
the Harlem renaissanc 哈莱姆文艺复兴e.

49
00:02:15,260 --> 00:02:17,345
J-
- Joy Behar!

50
00:02:17,471 --> 00:02:18,930
- Okay...

51
00:02:19,056 --> 00:02:21,307
first of all, Joy Behar
will shoot you in the face.

52
00:02:21,433 --> 00:02:23,017
So watch your shit.

53
00:02:23,143 --> 00:02:25,770
Secondly, I don't understand
how you could possibly

54
00:02:25,896 --> 00:02:27,146
think you're blacker than me.
- I am.

55
00:02:27,272 --> 00:02:30,900
- Motherfucker,
you drive a Prius.

56
00:02:31,026 --> 00:02:36,072
You saw Warhorse.

57
00:02:36,198 --> 00:02:38,491
Plus, you married
a white woman.

58
00:02:38,617 --> 00:02:41,619
Oh, actually you win.
'Cause that-

59
00:02:45,040 --> 00:02:47,375
So glad we could bring this
project to this neighborhood 地区.

60
00:02:47,501 --> 00:02:49,836
- It's so rich
in history.

61
00:02:49,962 --> 00:02:51,504
- I grew up in a neighborhood
like this.

62
00:02:51,630 --> 00:02:52,964
- Oh, yeah, I grew up
in a neighborhood

63
00:02:53,090 --> 00:02:55,550
exactly like this.

64
00:02:55,676 --> 00:02:56,968
- All right,
welcome to Mama Sugarback's.

65
00:02:57,094 --> 00:02:59,262
Y'all ready to order?
- Yeah.

66
00:02:59,388 --> 00:03:03,099
Can I have a chicken fried 油炸的 steak 牛排
with gravy 肉汁 and a cola 可乐?

67
00:03:03,225 --> 00:03:05,268
- Okay.

68
00:03:05,394 --> 00:03:07,186
- I will have the baked 烤的 beef 牛肉
short ribs 排骨 with collard 羽衣甘蓝叶 greens,

69
00:03:07,312 --> 00:03:08,729
and throw down
some of that cornbread 玉米饼.

70
00:03:08,856 --> 00:03:11,482
- All right, babe.

71
00:03:11,608 --> 00:03:13,818
- You know what?
Hold up a second.

72
00:03:13,944 --> 00:03:15,111
I'm also gonna have
some of them

73
00:03:15,237 --> 00:03:17,363
collard greens and cornbread
as well,

74
00:03:17,489 --> 00:03:18,614
but hook a brother up
with some of them hot links.

75
00:03:18,740 --> 00:03:20,908
- All right.
- All right.

76
00:03:21,034 --> 00:03:24,620
- You know what?
Why am I trying to front?

77
00:03:24,746 --> 00:03:26,205
Scratch 划 all that.

78
00:03:26,331 --> 00:03:29,292
Give me some okra 秋葵
and some fried red snapper 鲷鱼.

79
00:03:29,418 --> 00:03:30,626
And, girl, you know I want
some chitlins 猪肠.

80
00:03:30,752 --> 00:03:32,545
- All right now.

81
00:03:32,671 --> 00:03:34,881
- Y'all got ham 火腿 hocks 关节?

82
00:03:35,007 --> 00:03:36,841
- Of course.
- Well, that's what I want.

83
00:03:36,967 --> 00:03:39,176
I want a plate 盘子 of ham hocks,
deep-fried, blackened 熏制的,

84
00:03:39,303 --> 00:03:40,428
and served on a bed
of mustard 芥末 greens.

85
00:03:40,554 --> 00:03:41,971
- Pig feet.

86
00:03:42,097 --> 00:03:45,975
I want some pig feet
and four pounds of grits 粗玉米粉.

87
00:03:46,101 --> 00:03:47,727
And oh, oh!
And you know what else?

88
00:03:47,853 --> 00:03:49,937
Give me a little Dixie cup
full of lard 猪油, all right?

89
00:03:50,063 --> 00:03:52,815
- I just remembered what I want.
A bowl 碗 of mosquitoes 蚊子.

90
00:03:52,941 --> 00:03:55,318
None of them tiny ones either,
give me them big motherfuckers

91
00:03:55,444 --> 00:03:57,612
you find
down at the swamps 沼泽.

92
00:03:57,738 --> 00:04:00,197
- Sister, could you please
hook a brother up 帮我一个忙

93
00:04:00,324 --> 00:04:02,199
with a rusty bucket
full of fish heads

94
00:04:02,326 --> 00:04:04,285
wrapped 包裹 in razor wire?

95
00:04:04,411 --> 00:04:06,078
- Donkey 驴子 teeth.
- Donkey teeth?

96
00:04:06,204 --> 00:04:08,122
- Straight out
a donkey's mouth.

97
00:04:08,248 --> 00:04:10,833
You know what? Fuck it.
Any animal tooth will do.

98
00:04:10,959 --> 00:04:12,168
I want you to stick it
in some honey glaze 釉面,

99
00:04:12,294 --> 00:04:13,544
fry it with fat back,

100
00:04:13,670 --> 00:04:15,254
and serve it
in an old tin coffee can.

101
00:04:15,380 --> 00:04:17,381
- Forget everything I said
up to this point right now.

102
00:04:17,507 --> 00:04:20,176
Bring me some dandelion 蒲公英 greens,
a cow 母牛 hip 臀部, and a dog face.

103
00:04:20,302 --> 00:04:22,261
Wrap that whole mess
in an old Ebony magazine,

104
00:04:22,387 --> 00:04:23,763
and serve it to me
in a shoebox.

105
00:04:23,889 --> 00:04:25,973
- Okay, I want a platter
of stork ankles 脚踝,

106
00:04:26,099 --> 00:04:28,684
an old cellar 地窖 door,
a possum 负鼠 spine 脊柱,

107
00:04:28,810 --> 00:04:32,521
and a human foot.

108
00:04:36,902 --> 00:04:39,654
- You want a human foot?

109
00:04:39,780 --> 00:04:43,074
- Mm-hmm.

110
00:04:43,200 --> 00:04:47,078
- I got just one question
for y'all.

111
00:04:50,207 --> 00:04:52,041
- You want gravy 肉汁
on that cellar door?

112
00:04:52,167 --> 00:04:53,584
- Oh, definitely.

113
00:04:53,710 --> 00:04:55,127
- Oh, yeah, yeah.
You gotta put gravy on that.

114
00:04:55,253 --> 00:04:56,295
- What's a cellar door
without gravy?

115
00:04:56,421 --> 00:04:58,589
- It's not food.

116
00:05:03,220 --> 00:05:06,180
- Mmm.

117
00:05:06,306 --> 00:05:08,391
- It's really-

118
00:05:08,517 --> 00:05:09,642
- Ugh.

119
00:05:11,561 --> 00:05:14,230
- Ah, man, I really had
to get out of the house.

120
00:05:14,356 --> 00:05:15,606
- I was glad you came out.
- I appreciate it.

121
00:05:15,732 --> 00:05:16,941
- For real.

122
00:05:17,067 --> 00:05:18,109
- Look here,
you check this girl out, man.

123
00:05:18,235 --> 00:05:21,862
She is fine.

124
00:05:21,989 --> 00:05:24,782
- She all right.
- Really?

125
00:05:24,908 --> 00:05:26,242
- Hey, man, I am going to go
to the bathroom.

126
00:05:26,368 --> 00:05:30,538
- Okay, man.
See you in a minute.

127
00:05:30,664 --> 00:05:32,415
- You know what?

128
00:05:32,541 --> 00:05:33,916
I told myself this morning
I wasn't gonna fall in love.

129
00:05:34,042 --> 00:05:38,879
You went and made me break
my only rule.

130
00:05:39,006 --> 00:05:40,589
I'm gonna take that
right quick-

131
00:05:40,716 --> 00:05:43,342
I am going to take that.
I'm going to use that.

132
00:05:43,468 --> 00:05:45,177
Okay.

133
00:05:45,303 --> 00:05:48,014
- Oh, hey, what's up, man?
You just got her number, huh?

134
00:05:48,140 --> 00:05:50,474
- Yeah, Cheyenne.
- Oh, is that her name?

135
00:05:50,600 --> 00:05:51,934
- Yeah, she was fly-y.

136
00:05:52,060 --> 00:05:54,562
- That's the girl
I was talking about.

137
00:05:54,688 --> 00:05:56,856
- Oh, snap. Are you serious?
- Yeah, dude.

138
00:05:56,982 --> 00:05:58,315
- Oh, I'm sorry.
- It ain't no thing, man.

139
00:05:58,442 --> 00:05:59,734
Forget it.
There's plenty of hotties 辣妹 here.

140
00:05:59,860 --> 00:06:00,901
- I just thought you
were talking about someone else.

141
00:06:01,028 --> 00:06:03,404
- Check her out.

142
00:06:07,451 --> 00:06:08,826
- I'm not a sports man,
but...

143
00:06:08,952 --> 00:06:11,245
think me and you
could touch down later,

144
00:06:11,371 --> 00:06:12,872
if you know
what I'm saying.

145
00:06:12,998 --> 00:06:15,458
Am I being stupid?
But for real though.

146
00:06:15,584 --> 00:06:17,835
Just give me your number,
whatever.

147
00:06:17,961 --> 00:06:20,129
Thank you, girl.
I'ma take that from you.

148
00:06:20,255 --> 00:06:22,131
- How did you get
over here so fast?

149
00:06:22,257 --> 00:06:24,258
- Huh?

150
00:06:24,384 --> 00:06:28,846
- Nothing.
It just seemed like you-

151
00:06:28,972 --> 00:06:34,602
- Oh, damn.
That reporter is smokin' hot.

152
00:06:36,897 --> 00:06:39,356
What the fuck?

153
00:06:47,032 --> 00:06:49,408
No.

154
00:06:49,534 --> 00:06:53,871
No!

155
00:07:01,546 --> 00:07:03,547
Huh?
Yeah.

156
00:07:08,678 --> 00:07:11,138
No.
No!

157
00:07:11,264 --> 00:07:13,682
- Hey, baby.
- How are you-

158
00:07:13,809 --> 00:07:15,684
are you doing that?
- Honey, what's wrong?

159
00:07:15,811 --> 00:07:18,020
- Oh, baby, I love you.

160
00:07:18,146 --> 00:07:19,522
I'm sorry.

161
00:07:19,648 --> 00:07:21,273
I was out, I was looking
at other women tonight.

162
00:07:21,399 --> 00:07:23,400
But you are all there is.

163
00:07:23,527 --> 00:07:27,738
All right,
I love you.

164
00:07:36,581 --> 00:07:38,332
- All right,
so Keegan has two dogs,

165
00:07:38,458 --> 00:07:39,625
which he has no control over
whatsoever.

166
00:07:39,751 --> 00:07:40,793
- That's not true.
That's not true.

167
00:07:40,919 --> 00:07:42,419
- That is true.

168
00:07:42,546 --> 00:07:43,796
- The little one
I've got some control over.

169
00:07:43,922 --> 00:07:46,632
The big one's an asshole.
- No.

170
00:07:46,758 --> 00:07:48,259
They don't listen to him.

171
00:07:48,385 --> 00:07:49,844
I don't understand
why you talk to them like that.

172
00:07:49,970 --> 00:07:51,846
My man will be like,
"Hey, Levi, would you stay away

173
00:07:51,972 --> 00:07:54,265
from that Architectural Digest
magazine?" What's that?

174
00:07:54,391 --> 00:07:56,058
- Because-what's that?
He understands me.

175
00:07:56,184 --> 00:07:57,309
It's because he understands me.
- No, he does not.

176
00:07:57,435 --> 00:07:58,561
- Yes, he does.
- And by the way...

177
00:07:58,687 --> 00:08:01,147
I'm allergic 过敏的 to dogs, okay?

178
00:08:01,273 --> 00:08:03,524
Most people keep their dog
in a bathroom when I come over.

179
00:08:03,650 --> 00:08:05,484
When I go over his house,
his dogs talking about-

180
00:08:05,610 --> 00:08:08,487
Comes right up to me.

181
00:08:08,613 --> 00:08:11,031
And what do you say?
- I say "Levi,

182
00:08:11,158 --> 00:08:13,284
"what are you do-
Get outta here.

183
00:08:13,410 --> 00:08:15,327
"What are you doing?
What are you doing?

184
00:08:15,453 --> 00:08:17,246
"What are you doing?

185
00:08:17,372 --> 00:08:20,166
Are you doing it right now?
What are you doing right now?"

186
00:08:20,292 --> 00:08:22,877
- This is his dog.
- "Get away from him. Levi!

187
00:08:23,003 --> 00:08:25,087
"Get away from him.
Would you get away from him?

188
00:08:25,213 --> 00:08:28,132
"Get away from him!
Get away from him!

189
00:08:28,258 --> 00:08:29,758
"Get awa-
Get away!

190
00:08:29,885 --> 00:08:32,469
"Are you still standing there?
Get away from him!

191
00:08:32,596 --> 00:08:34,555
"Would you get away
from Jordan, please?

192
00:08:34,681 --> 00:08:36,557
Get away!"
- He's not listening to you.

193
00:08:36,683 --> 00:08:39,810
- And he-'Cause eventually
he leaves after the 50th time.

194
00:08:39,936 --> 00:08:42,396
He leaves.
He leaves.

195
00:08:42,522 --> 00:08:45,816
- That's boredom 厌倦, my friend.
That is boredom.

196
00:08:47,736 --> 00:08:49,445
- When she does talk,
like, you're listening.

197
00:08:49,571 --> 00:08:50,821
- Yeah, oh, my God.
Last time, I was like,

198
00:08:50,947 --> 00:08:52,072
"Who are you?
You're brilliant."

199
00:08:52,199 --> 00:08:54,533
- I know.
- Oh, my God!

200
00:08:54,659 --> 00:08:56,202
Oh, my God.

201
00:08:56,328 --> 00:08:58,787
- Oh, my God!
- Amy, look at that one!

202
00:08:58,914 --> 00:09:01,165
- Oh, Megan!
- He is so cute.

203
00:09:01,291 --> 00:09:02,499
- That is
the cutest effing 该死的 puppy

204
00:09:02,626 --> 00:09:03,876
I've ever seen
in my entire life.

205
00:09:04,002 --> 00:09:05,544
- Oh, my God,
I'm just gonna take him,

206
00:09:05,670 --> 00:09:08,214
and I just wanna bite
his little ears off.

207
00:09:08,340 --> 00:09:09,715
- I just wanna take
his little legs,

208
00:09:09,841 --> 00:09:11,967
and I could snack on 'em
like chicken drumettes.

209
00:09:12,093 --> 00:09:13,719
- Yeah, I just wanna take
his fuzzy little perfect head.

210
00:09:13,845 --> 00:09:15,638
I wanna put it in my mouth
and squeeze 挤压 down on it hard

211
00:09:15,764 --> 00:09:17,556
until I have a puppy face
diamond in my mouth.

212
00:09:17,682 --> 00:09:19,642
- Mm, I feel like I have
to wrap his face in a towel 毛巾,

213
00:09:19,768 --> 00:09:21,560
and then beat it
with a tire iron 撬胎棒.

214
00:09:21,686 --> 00:09:24,563
- I'm going to buy that dog,
I'm going drive him to the vet 兽医,

215
00:09:24,689 --> 00:09:26,482
and I'm going to have him
put down immediately.

216
00:09:26,608 --> 00:09:27,900
Oh, my God.

217
00:09:28,026 --> 00:09:29,485
You what I'm gonna have to do?
- What?

218
00:09:29,611 --> 00:09:30,653
- I'm gonna have to take
that scruffy 肮脏的 little nose...

219
00:09:30,779 --> 00:09:31,820
- Yes.
- And just kick it.

220
00:09:31,947 --> 00:09:33,405
I just have to-
- Yes.

221
00:09:33,531 --> 00:09:34,615
- I would kick it
until it comes up on my shoes

222
00:09:34,741 --> 00:09:36,408
like little puppy
snout slippers 拖鞋.

223
00:09:36,534 --> 00:09:37,952
And then I'm gonna go
to a party and everyone's like,

224
00:09:38,078 --> 00:09:39,286
"Oh, my God, where'd you
get those slippers?"

225
00:09:39,412 --> 00:09:40,663
I'll be like, "These are
my face slippers

226
00:09:40,789 --> 00:09:42,164
from my adorable,
dumb 傻傻的 little puppy."

227
00:09:42,290 --> 00:09:43,332
- That's exactly what
I was going to say!

228
00:09:43,458 --> 00:09:44,750
- ESP!

229
00:09:44,876 --> 00:09:46,168
- Oh, my God, I just wanna
take his head.

230
00:09:46,294 --> 00:09:47,586
I wanna peel 剥落 the skin
off of his skull 头盖骨.

231
00:09:47,712 --> 00:09:49,171
- I know where you're going.

232
00:09:49,297 --> 00:09:50,547
- I wanna just start throwing it
up like pizza dough 面团

233
00:09:50,674 --> 00:09:52,341
until it's long, flat,
and round.

234
00:09:52,467 --> 00:09:55,135
I wanna take it to a picnic 野餐 and
toss 投掷 it around like a Frisbee 飞盘

235
00:09:55,262 --> 00:09:57,179
at a puppy-face-Frisbee
picnic!

236
00:09:57,305 --> 00:10:00,266
- Let's get him!
- Yeah!

237
00:10:02,894 --> 00:10:05,562
- So cute.
- So cute.

238
00:10:05,689 --> 00:10:07,064
Oh, my God,
is it me,

239
00:10:07,190 --> 00:10:09,191
or is it like totally
mosquito-y out here?

240
00:10:09,317 --> 00:10:10,359
- Oh, my God,
remember this one?

241
00:10:10,485 --> 00:10:12,528
He was so cute.
- So cute.

242
00:10:37,220 --> 00:10:39,346
- And now,
ladies and gentlemen,

243
00:10:39,472 --> 00:10:43,100
the man you've been waiting for,
Mr. Michael Winslow!

244
00:10:43,226 --> 00:10:45,060
- Go get 'em, Winslow.

245
00:10:45,186 --> 00:10:48,981
Bring the noise!
Bring the noise.

246
00:10:49,107 --> 00:10:52,443
Another sold-out 售罄的 night
for the Kings of Mouth Noise.

247
00:10:52,569 --> 00:10:54,069
I tell you though,

248
00:10:54,195 --> 00:10:55,571
we wouldn't have this audience
without Winslow.

249
00:10:55,697 --> 00:10:57,156
Lucky for you, huh?

250
00:10:57,282 --> 00:11:00,242
Those Police Academy 研究院 fans.

251
00:11:00,368 --> 00:11:01,952
Look at all the tee shirts.

252
00:11:02,078 --> 00:11:04,580
Six sequels 续集.
Can you believe it?

253
00:11:04,706 --> 00:11:06,665
Every sound effect
this guy makes

254
00:11:06,791 --> 00:11:10,461
is like a little, tiny Mona Lisa 蒙娜丽莎
popping out of his mouth.

255
00:11:10,587 --> 00:11:12,629
Have you ever heard him do

256
00:11:12,756 --> 00:11:14,089
the walking
through the leaves

257
00:11:14,215 --> 00:11:16,842
in the fall
while eating an apple-

258
00:12:23,576 --> 00:12:24,618
Whew!

259
00:12:29,624 --> 00:12:31,333
Whew!

260
00:12:31,459 --> 00:12:33,710
- Shing.

261
00:12:33,837 --> 00:12:37,339
Ting, ting, ting.

262
00:12:37,465 --> 00:12:38,757
- Ting, ting, ting.

263
00:12:40,593 --> 00:12:43,178
Shing!

264
00:12:43,304 --> 00:12:45,180
- Shing!

265
00:12:45,306 --> 00:12:47,599
- Whap!

266
00:12:53,648 --> 00:12:55,899
Pa-chhh!

267
00:12:56,025 --> 00:12:59,695
Chh!

268
00:14:01,799 --> 00:14:03,759
- All right, so one of the worst
places to be a black man...

269
00:14:03,885 --> 00:14:05,469
- Yes?

270
00:14:05,595 --> 00:14:07,221
- Is at a party where there's
only white people there.

271
00:14:07,347 --> 00:14:09,097
- Oh, yeah.

272
00:14:09,224 --> 00:14:11,600
It's just way to much pressure
to provide all of the fun.

273
00:14:11,726 --> 00:14:14,603
- Yes!

274
00:14:14,729 --> 00:14:17,606
Oh, you can't-
- Way too much pressure.

275
00:14:17,732 --> 00:14:19,733
- You cannot cross
the dance floor at all.

276
00:14:19,859 --> 00:14:21,735
If I need a drink
at one of these parties,

277
00:14:21,861 --> 00:14:24,154
I will go around the perimeter 周边
of the party.

278
00:14:24,280 --> 00:14:26,615
'Cause you know
if you go through this,

279
00:14:26,741 --> 00:14:28,909
you will have a group
of white people around you

280
00:14:29,035 --> 00:14:32,079
talking about, "Go, Jordan!
Go, Jordan! Go, Jordan!"

281
00:14:32,205 --> 00:14:35,040
- That's true.

282
00:14:35,166 --> 00:14:36,959
And that used to happen to me

283
00:14:37,085 --> 00:14:38,752
all the time
when I was in school.

284
00:14:38,878 --> 00:14:42,047
I never even noticed until one
of my friends-a white friend-

285
00:14:42,173 --> 00:14:44,508
I was, like, 40 minutes late
for a party one time.

286
00:14:44,634 --> 00:14:46,301
And I show up,
I come in the front door,

287
00:14:46,427 --> 00:14:47,886
my friend's like,
"Oh, my God,

288
00:14:48,012 --> 00:14:52,933
thank God you're here."

289
00:14:53,059 --> 00:14:54,601
There was, like, a group
of white people sitting around

290
00:14:54,727 --> 00:14:58,939
for 40 minutes waiting
for the black guy to show up.

291
00:15:03,111 --> 00:15:06,196
- Just got your check.
- Yeah.

292
00:15:06,322 --> 00:15:08,073
Damn.

293
00:15:08,199 --> 00:15:10,158
Look how much I lost
in taxes.

294
00:15:10,285 --> 00:15:12,869
Damn government!
Look at that.

295
00:15:12,996 --> 00:15:14,788
Might as well join
the Tea Party.

296
00:15:14,914 --> 00:15:17,416
- Excuse me.
What did you just say?

297
00:15:17,542 --> 00:15:19,710
- "Might as well join
the Tea Party."

298
00:15:19,836 --> 00:15:22,504
- Come with me.
- Whoa! What's going on?

299
00:15:22,630 --> 00:15:24,089
- You are the chosen one.

300
00:15:24,215 --> 00:15:25,882
- What the hell
are you talking about, man?

301
00:15:26,009 --> 00:15:28,093
- We've been waiting for you.
- Waiting for me? What?

302
00:15:28,219 --> 00:15:29,595
What the hell is this?
- I found him!

303
00:15:31,264 --> 00:15:33,849
- Tell them what you were
telling your friend.

304
00:15:33,975 --> 00:15:35,017
- I hate taxes?

305
00:15:35,143 --> 00:15:36,351
- What do you find appealing 呼吁/上诉

306
00:15:36,477 --> 00:15:38,020
about the politics
of the Tea Party?

307
00:15:38,146 --> 00:15:41,106
- I'm not a politician.
- A real man of the people.

308
00:15:41,232 --> 00:15:42,733
- How do you feel
about the Second Amendment 修正案?

309
00:15:42,859 --> 00:15:44,318
- I don't want this.

310
00:15:44,444 --> 00:15:46,945
- Taken away from you,
just like the rest of us.

311
00:15:47,071 --> 00:15:50,032
One of many issues
you're passionate 强烈的 about.

312
00:15:50,158 --> 00:15:52,159
Oh!
Smile.

313
00:15:52,285 --> 00:15:53,827
- Oh!

314
00:15:53,953 --> 00:15:55,579
- Now, come meet your public.
- Public?

315
00:15:55,705 --> 00:15:57,247
What are you talking about?
I ain't got no public.

316
00:15:57,373 --> 00:15:58,749
What is this?

317
00:15:58,875 --> 00:16:00,167
- Ladies and gentlemen,

318
00:16:00,293 --> 00:16:02,044
the new leader
of the Minnesota Tea Party

319
00:16:02,170 --> 00:16:05,255
and the next president
of the United States.

320
00:16:05,381 --> 00:16:07,507
What's your name?
- Terrell Jackson.

321
00:16:07,634 --> 00:16:10,177
- Perfect.
Terrell Jackson!

322
00:16:10,303 --> 00:16:12,804
- Ah!

323
00:16:12,930 --> 00:16:15,932
Look, look, I don't want
any part of this, all right?

324
00:16:16,059 --> 00:16:18,602
- You hate the government.
That's great!

325
00:16:18,728 --> 00:16:21,021
- Show us your gun again!

326
00:16:21,147 --> 00:16:24,858
- Look, y'all are just
too creepy 令人毛骨悚然的.

327
00:16:24,984 --> 00:16:26,568
You can have this back.

328
00:16:26,694 --> 00:16:28,945
- Ah!
- Oh!

329
00:16:29,072 --> 00:16:33,158
- Great! Where are we gonna find
another black guy?

330
00:16:40,667 --> 00:16:42,125
- Hey, baby.
- Mm.

331
00:16:42,251 --> 00:16:43,335
- Wake up.

332
00:16:43,461 --> 00:16:46,546
- Mm,
what time is it?

333
00:16:49,217 --> 00:16:51,176
- What are you doing?
- Good morning.

334
00:16:51,302 --> 00:16:53,011
- Hey!

335
00:16:53,137 --> 00:16:55,389
- Come on, girl.
It's time to get up.

336
00:16:55,515 --> 00:16:56,973
Let's go!

337
00:16:57,100 --> 00:17:00,852
- Delete the picture,
motherfucker!

338
00:17:03,773 --> 00:17:06,108
- Okay, um, uh,
delete.

339
00:17:06,234 --> 00:17:10,070
Deleting it.
Um, delete photo.

340
00:17:10,196 --> 00:17:12,072
Okay.

341
00:17:12,198 --> 00:17:13,490
It's gone.
It's all gone.

342
00:17:17,036 --> 00:17:19,329
- Thanks, baby.
Mm.

343
00:17:20,873 --> 00:17:23,875
How do you want your eggs?

344
00:17:27,380 --> 00:17:29,381
- Scrambled 攀登/黄油炒蛋.

345
00:17:34,137 --> 00:17:35,971
- One of the hardest jobs
in the military

346
00:17:36,097 --> 00:17:38,390
has to be
military recruiter 招聘人员.

347
00:17:38,516 --> 00:17:40,434
That is tough.
- That's a tough one all right.

348
00:17:40,560 --> 00:17:42,728
Because you have to look
somebody in the eye

349
00:17:42,854 --> 00:17:45,147
and tell them their first job
out of high school

350
00:17:45,273 --> 00:17:46,648
is getting shot at.
- Right.

351
00:17:46,774 --> 00:17:48,734
- You know, that's-
that is hard.

352
00:17:48,860 --> 00:17:51,653
- Yeah, or, like,
to appeal to kids,

353
00:17:51,779 --> 00:17:54,030
they made that one commercial
where there's, like, the marine

354
00:17:54,157 --> 00:17:56,408
going, and he's going through
a futuristic  obstacle course 未来主义障碍课程.

355
00:17:56,534 --> 00:17:58,034
- And then he gets
past the pendulums 钟摆,

356
00:17:58,161 --> 00:17:59,327
and then there's a rock

357
00:17:59,454 --> 00:18:01,163
with what appears to be
Excalibur 亚瑟王的神剑 in it.

358
00:18:01,289 --> 00:18:02,914
- Oh, and he's going
to need Excalibur

359
00:18:03,040 --> 00:18:04,583
because he's about to fight
a lava demon 熔岩恶魔.

360
00:18:04,709 --> 00:18:06,001
- Mm-hmm.

361
00:18:06,127 --> 00:18:07,502
- With a fire whip!

362
00:18:07,628 --> 00:18:08,670
- Yes.

363
00:18:08,796 --> 00:18:10,589
Yes.

364
00:18:10,715 --> 00:18:11,965
But I understand it
a little bit

365
00:18:12,091 --> 00:18:13,425
because you have to cater 迎合
the commercials

366
00:18:13,551 --> 00:18:15,093
to certain demographics 人口特征.

367
00:18:15,219 --> 00:18:16,928
And right now,
with the recent-

368
00:18:17,054 --> 00:18:18,930
the repeal 废止
of don't ask, don't tell,

369
00:18:19,056 --> 00:18:20,599
there's a whole new
demographic out there

370
00:18:20,725 --> 00:18:22,309
that they can recruit.
- Sure, sure.

371
00:18:22,435 --> 00:18:24,144
- A whole lot of flowers
to be plucked 摘.

372
00:18:24,270 --> 00:18:26,980
- Yes.

373
00:18:33,237 --> 00:18:35,030
- Sergeant Crandall,
United States Army recruiter.

374
00:18:35,156 --> 00:18:36,490
How y'all doing?

375
00:18:36,616 --> 00:18:38,700
This is my associate
Sergeant Graham Packs.

376
00:18:38,826 --> 00:18:39,868
Yeah!

377
00:18:39,994 --> 00:18:41,161
- Huh-huh!
Huh, huh, huh-huh!

378
00:18:41,287 --> 00:18:42,579
Ha, ha, ha.
Nice beat, gentlemen.

379
00:18:42,705 --> 00:18:44,289
- We wanna talk to you
about your future,

380
00:18:44,415 --> 00:18:47,083
and where you fit in
the United States Army, y'all!

381
00:18:47,210 --> 00:18:48,460
- Yeah, in case
you haven't heard,

382
00:18:48,586 --> 00:18:49,836
we are asking,
and you're telling.

383
00:18:49,962 --> 00:18:51,213
- That's right,
no restrictions, man.

384
00:18:51,339 --> 00:18:52,964
You gonna be surrounded
by dudes all day.

385
00:18:53,090 --> 00:18:55,133
Y'all can exercise together.
You can shower together.

386
00:18:55,259 --> 00:18:57,302
- Talking 'bout soap, water-
the works!

387
00:18:57,428 --> 00:18:58,470
- Now I know what you're
thinking.

388
00:18:58,596 --> 00:19:00,180
"The army's dangerous."

389
00:19:00,306 --> 00:19:02,098
But the fact of the matter is
everything's dangerous.

390
00:19:02,225 --> 00:19:04,017
- All depends on
what your definition

391
00:19:04,143 --> 00:19:05,852
of the word "danger" is.

392
00:19:05,978 --> 00:19:09,481
- Oh, yeah!
Oh, here we go!

393
00:19:09,607 --> 00:19:12,442
- We got a friend.
All right.

394
00:19:12,568 --> 00:19:14,945
- Hey!
Navy already got him.

395
00:19:15,071 --> 00:19:16,238
But you're better off
in the army

396
00:19:16,364 --> 00:19:17,405
'cause we gonna get you
in the best shape

397
00:19:17,532 --> 00:19:18,698
in your whole life!

398
00:19:18,825 --> 00:19:20,075
- Whoo!
Talking 'bout a six pack.

399
00:19:20,201 --> 00:19:21,618
- Blip!
- Gonna give you an eight pack.

400
00:19:21,744 --> 00:19:22,786
- Sclip!
- Hey, we'll even give you

401
00:19:22,912 --> 00:19:24,454
a jet pack.

402
00:19:24,580 --> 00:19:25,789
- That's a joke, man.
He a joke, he a funny guy.

403
00:19:25,915 --> 00:19:27,165
We ain't got jet packs.
- Oh!

404
00:19:27,291 --> 00:19:28,416
- Oh, that's my cut, man!

405
00:19:28,543 --> 00:19:30,168
- Oh, what what?

406
00:19:30,294 --> 00:19:33,004
- Go ahead, go ahead.

407
00:19:33,130 --> 00:19:35,340
- You got to isolate,
then isolate,

408
00:19:35,466 --> 00:19:37,217
then i-so-late.
- Oh, yeah!

409
00:19:37,343 --> 00:19:38,844
Tell you something
right now, man.

410
00:19:38,970 --> 00:19:41,429
Combat 战斗 is just a bunch of men
in a tight space

411
00:19:41,556 --> 00:19:42,889
with flashing lights
and loud noises.

412
00:19:43,015 --> 00:19:44,558
- Like-

413
00:19:44,684 --> 00:19:45,892
- Just like this.

414
00:19:46,018 --> 00:19:47,519
- Yeah, you'll fit right in
at home.

415
00:19:47,645 --> 00:19:49,646
- It's gonna be great.
Oh, here it is.

416
00:19:49,772 --> 00:19:52,566
Oh, can't stop it.
Can't stop it.

417
00:19:52,692 --> 00:19:54,150
- Go, Crandall.
Sergeant Crandall.

418
00:19:54,277 --> 00:19:55,318
Crandall, Crandall, Crandall.
And hold it.

419
00:19:55,444 --> 00:19:56,486
- What?
- Hold it.

420
00:19:56,612 --> 00:19:57,696
- Gotta do?

421
00:19:57,822 --> 00:20:00,115
- And hold it right there.
- Whoo!

422
00:20:00,241 --> 00:20:01,867
- I'm a robot.
- Robot.

423
00:20:01,993 --> 00:20:04,327
Excuse me there,
young man.

424
00:20:04,453 --> 00:20:05,871
Have you ever thought about
joining the United States Army?

425
00:20:05,997 --> 00:20:07,747
It's-
Oh, oh, excuse me.

426
00:20:07,874 --> 00:20:10,208
I will, uh, talk to you two
later-pardon me.

427
00:20:17,341 --> 00:20:18,925
- What's all this?

428
00:20:19,051 --> 00:20:22,137
- This is your lucky night.

429
00:20:22,263 --> 00:20:25,599
I wanna be your fantasy 幻想.

430
00:20:25,725 --> 00:20:27,934
Whatever you want me to do.

431
00:20:28,060 --> 00:20:29,895
Whatever you wanna do to me.

432
00:20:30,021 --> 00:20:32,981
You just tell me
your naughtiest fantasies, Phil,

433
00:20:33,107 --> 00:20:36,610
and you'll get it.

434
00:20:36,736 --> 00:20:37,986
- Seriously?

435
00:20:38,112 --> 00:20:40,030
- Seriously.

436
00:20:40,156 --> 00:20:43,825
- Like, for realsies?
- For realsies.

437
00:20:43,951 --> 00:20:49,456
- Well, how about we call up
your friend Erica-

438
00:20:49,582 --> 00:20:51,583
Aah!

439
00:20:54,378 --> 00:20:56,338
- Good night, everybody!
Good night.

440
00:20:56,464 --> 00:20:58,089
Thank you for coming out.

441
00:20:58,215 --> 00:21:01,092
- ♪ I'm gonna do
my one line here ♪

442
00:21:01,218 --> 00:21:03,386
- Oh, yeah.

443
00:21:06,182 --> 00:21:07,390
- Finally, the dog licks 舔 me,
by the way. What did you say?

444
00:21:07,516 --> 00:21:09,684
- "Levi, get outta here.
You broke the contract!"

445
00:21:09,810 --> 00:21:11,811
- Verbal 口头的 contract
with the animal.

446
00:21:11,938 --> 00:21:14,481
What is that? You could just
lock him in the bathroom!

447
00:21:14,607 --> 00:21:16,483
- I couldn't do that 'cause it
was a training moment for him.

448
00:21:16,609 --> 00:21:17,943
- It was a training moment
for me too.

449
00:21:18,069 --> 00:21:19,819
I learned never to go
to your house again.

