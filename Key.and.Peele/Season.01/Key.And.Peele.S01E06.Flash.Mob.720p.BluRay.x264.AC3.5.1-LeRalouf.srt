 1
00:00:25,234 --> 00:00:26,651
- Race war!

2
00:00:26,777 --> 00:00:30,613
It's a goddamn
race war!

3
00:00:38,539 --> 00:00:40,248
- Flash mob-
flash mob canceled.

4
00:00:40,374 --> 00:00:41,874
The flash mob is canceled.

5
00:01:07,192 --> 00:01:10,737
- Oh, my goodness.
Thank you.

6
00:01:10,863 --> 00:01:14,240
Thank you.

7
00:01:14,366 --> 00:01:15,491
That's plenty 够多了.

8
00:01:15,617 --> 00:01:16,909
- Thank you so much
for coming out.

9
00:01:17,035 --> 00:01:18,661
Welcome to the show.
I am Keegan.

10
00:01:18,787 --> 00:01:20,329
- I am Jordan.
- And this is Key & Peele.

11
00:01:20,456 --> 00:01:24,375
- Yep.
- Thank you for coming.

12
00:01:24,501 --> 00:01:28,337
- And we have had a fantastic
time on our first season.

13
00:01:28,464 --> 00:01:30,506
The first season
of Key & Peele, our brand-

14
00:01:30,632 --> 00:01:33,509
Mm-hmm, nope.

15
00:01:33,635 --> 00:01:36,137
And, uh,
this is happening, okay.

16
00:01:36,263 --> 00:01:38,014
- Yep, just destroyed
all the muscles

17
00:01:38,140 --> 00:01:39,474
in this pectoral 胸部 area.

18
00:01:39,600 --> 00:01:41,142
- Nope, did not.
- Yes, I did.

19
00:01:41,268 --> 00:01:42,643
- Actually did not
do that.

20
00:01:42,770 --> 00:01:44,020
He plays these games
while we're trying

21
00:01:44,146 --> 00:01:45,646
to actually
get something done.

22
00:01:45,773 --> 00:01:48,649
Can't acknowledge it.

23
00:01:48,776 --> 00:01:50,359
Don't know.

24
00:01:50,486 --> 00:01:51,861
- I don't even understand
how he's still talking.

25
00:01:51,987 --> 00:01:53,404
I just paralyzed 使麻痹 him
with my kung fu fingers,

26
00:01:53,530 --> 00:01:54,989
so I don't know
how he's speaking.

27
00:01:55,115 --> 00:01:58,117
- I am talking, so clearly
wasn't paralyzed.

28
00:01:58,243 --> 00:02:00,453
Doesn't hurt,
doesn't-yeah.

29
00:02:00,579 --> 00:02:02,205
- Right there.
- Nope, didn't do anything.

30
00:02:02,331 --> 00:02:03,998
- His eye's going to pop
out of his head right now, so-

31
00:02:04,124 --> 00:02:05,541
- You want to ignore this.
- Pressure points.

32
00:02:05,667 --> 00:02:07,001
- If you ignore it,
it will stop eventually.

33
00:02:07,127 --> 00:02:08,419
Really, you can't sink 下沉
to his level.

34
00:02:08,545 --> 00:02:10,171
That's the main key.

35
00:02:10,297 --> 00:02:14,175
You can't-
you can't cave in 坍塌 投降,

36
00:02:14,301 --> 00:02:16,928
because after a while-

37
00:02:17,054 --> 00:02:18,513
after a while,
he's just going to-

38
00:02:18,639 --> 00:02:22,266
You know, the best
thing to do-

39
00:02:22,392 --> 00:02:24,227
Sometimes you just
gotta walk away.

40
00:02:24,353 --> 00:02:26,437
And your scrotum 阴囊
exploded.

41
00:02:26,563 --> 00:02:30,650
- Oh! Oh-ho-ho!

42
00:02:30,776 --> 00:02:34,070
Coolio, man.

43
00:02:34,196 --> 00:02:35,780
Oh, yeah, "soitainly."

44
00:02:35,906 --> 00:02:37,865
Oh, hey,
I gotta go.

45
00:02:37,991 --> 00:02:39,450
Yeah, Terry out.

46
00:02:39,576 --> 00:02:40,993
- Terry,
how you doing, man?

47
00:02:41,119 --> 00:02:42,245
- Hey, pretty good,
Gabe.

48
00:02:42,371 --> 00:02:44,330
Oh, got something
on your shirt.

49
00:02:45,457 --> 00:02:47,458
- Not today, Terry.

50
00:02:50,337 --> 00:02:54,632
- Looks like Gabe's
come out to play.

51
00:02:54,758 --> 00:02:56,259
You do have something
on your shirt though.

52
00:02:56,385 --> 00:02:58,261
- No, I don't.
- Yeah, you kind of do.

53
00:02:58,387 --> 00:02:59,929
- No, I'm pretty sure I don't.
- Got a clean shirt at my desk

54
00:03:00,055 --> 00:03:01,264
if you want me
to get it for you.

55
00:03:01,390 --> 00:03:02,890
- Don't see why
that would be necessary.

56
00:03:03,016 --> 00:03:04,308
- Really? Because the shirt
you have on right now,

57
00:03:04,434 --> 00:03:05,893
it's not clean.
- Except for that it is.

58
00:03:06,019 --> 00:03:07,436
- Me, I have a clean shirt.
- Oh.

59
00:03:07,563 --> 00:03:09,355
- You, you have some schmutz 污物
on your jammy-jam.

60
00:03:09,481 --> 00:03:13,317
- No, there is no schmutz
on my jammy-jam.

61
00:03:13,443 --> 00:03:15,820
- You know what?
Why don't I just get it for you?

62
00:03:15,946 --> 00:03:18,114
- Yeah, why don't you do that?
- Yeah.

63
00:03:21,034 --> 00:03:22,952
- And there we go.

64
00:03:23,078 --> 00:03:24,453
- Did you get it?

65
00:03:29,293 --> 00:03:31,335
I almost got you.
- Yeah.

66
00:03:31,461 --> 00:03:33,713
- I was that close.
- Almost, yeah.

67
00:03:33,839 --> 00:03:36,048
- I mean, I'm thinking,
"Damn, he's good."

68
00:03:37,801 --> 00:03:38,926
But you know what
the funny thing is?

69
00:03:39,052 --> 00:03:40,177
- What's that?

70
00:03:40,304 --> 00:03:42,221
- Now I guess
from when I dabbed 抹擦

71
00:03:42,347 --> 00:03:43,973
your shirt
with the paper towel 毛巾,

72
00:03:44,099 --> 00:03:46,183
and I'm not playing
the game right now,

73
00:03:46,310 --> 00:03:48,227
but I actually-I must have got
some paper-towel fuzz 绒毛

74
00:03:48,353 --> 00:03:50,563
on your shirt, so-

75
00:04:02,868 --> 00:04:05,870
- Hey, how long has Karen been
wearing her hair up like that?

76
00:04:30,938 --> 00:04:32,396
- Shh.

77
00:04:52,918 --> 00:04:54,418
I don't want you
to have to go through that.

78
00:04:54,544 --> 00:04:56,295
And I'm sorry
it happened.

79
00:04:56,421 --> 00:04:58,130
- Hey, Terry,
what's going on?

80
00:04:59,508 --> 00:05:01,634
- Uh, nothing.
- Having a good morning?

81
00:05:01,760 --> 00:05:03,135
- Ye-
- I'm having a fuckin'

82
00:05:03,261 --> 00:05:05,054
terrific morning.

83
00:05:05,180 --> 00:05:07,098
- Great.

84
00:05:07,224 --> 00:05:08,599
- Hope there's nothing
on my shirt.

85
00:05:08,725 --> 00:05:12,061
'Cause I can't
look down.

86
00:05:12,187 --> 00:05:16,399
- Oh, no, dude, we're not doing
that right now, okay?

87
00:05:16,525 --> 00:05:17,733
There's nothing
on your shirt.

88
00:05:17,859 --> 00:05:19,151
No one cares
about your shirt.

89
00:05:19,277 --> 00:05:21,445
Everything's not
about you.

90
00:05:21,571 --> 00:05:23,364
I'm trying to have
a conversation

91
00:05:23,490 --> 00:05:25,324
with Karen
right now, so-

92
00:05:32,165 --> 00:05:35,292
- You okay, Terry?
- It just pisses me off 把我惹恼了.

93
00:06:07,826 --> 00:06:09,827
I got it.
I got it.

94
00:06:09,953 --> 00:06:12,580
There's no way
you're gonna do it right now.

95
00:06:12,706 --> 00:06:15,041
Oh, yeah, yeah,
I do have schmutz.

96
00:06:15,167 --> 00:06:17,710
There's schmutz
all over me now.

97
00:06:17,836 --> 00:06:19,211
He can't deny that.

98
00:06:19,337 --> 00:06:20,838
Terry's not going to know
what hit him.

99
00:06:20,964 --> 00:06:23,466
I will win! I win!

100
00:06:39,232 --> 00:06:40,691
What-
what happened?

101
00:06:40,817 --> 00:06:43,277
- Terry died.

102
00:07:00,629 --> 00:07:03,839
- Be right back.

103
00:07:08,345 --> 00:07:11,097
Hey, Terry.

104
00:07:11,223 --> 00:07:14,391
It's Gabe.

105
00:07:14,518 --> 00:07:19,146
I, um, just wanted
to let you know that, um,

106
00:07:21,817 --> 00:07:24,110
that you didn't get me,
Terry.

107
00:07:24,236 --> 00:07:25,611
You didn't get me,

108
00:07:25,737 --> 00:07:27,363
because on the day
that you died,

109
00:07:27,489 --> 00:07:30,116
there was schmutz
on my shirt.

110
00:07:30,242 --> 00:07:32,201
So guess who wins
the game, Terry.

111
00:07:32,327 --> 00:07:35,538
I do. I win.

112
00:07:35,664 --> 00:07:37,581
You lose!

113
00:07:37,707 --> 00:07:38,791
Aah!

114
00:07:53,598 --> 00:07:57,143
Oh, Terry.

115
00:07:57,269 --> 00:07:59,353
He flicked 轻打 me.

116
00:08:07,404 --> 00:08:10,030
- You ever not tell somebody
that you haven't seen a film

117
00:08:10,157 --> 00:08:11,699
because of how they react

118
00:08:11,825 --> 00:08:13,325
when they find out
you haven't seen it?

119
00:08:13,451 --> 00:08:14,702
- Yes, yes.
- Yeah.

120
00:08:14,828 --> 00:08:16,287
You know
what I'm talking about.

121
00:08:16,413 --> 00:08:18,289
- I've done it.
- Yeah.

122
00:08:18,415 --> 00:08:19,915
People completely
flip out,

123
00:08:20,041 --> 00:08:22,418
and they freak out 崩溃
and everything about the movie.

124
00:08:22,544 --> 00:08:23,794
- Dude, you do it.
- It's like, it's just a movie.

125
00:08:23,920 --> 00:08:25,337
- You do it.
- I don't do it.

126
00:08:25,463 --> 00:08:26,881
- You've done it to me.
- When have I done it?

127
00:08:27,007 --> 00:08:28,632
- The Wiz.

128
00:08:28,758 --> 00:08:31,135
- How have you not seen
The Wiz?

129
00:08:31,261 --> 00:08:32,511
- This is what I'm talking
about, by the way.

130
00:08:32,637 --> 00:08:34,388
- How haven't you seen
The Wiz?

131
00:08:34,514 --> 00:08:36,056
Everybody has seen
The Wiz.

132
00:08:36,183 --> 00:08:38,517
- I haven't seen it.
Why do I have to see that?

133
00:08:38,643 --> 00:08:40,686
- Why?
Because you are black.

134
00:08:40,812 --> 00:08:42,479
- Okay.

135
00:08:42,606 --> 00:08:44,565
- You see, there's five
movies you have to see.

136
00:08:44,691 --> 00:08:46,984
It's very simple, Jordan,
they're all one-word titles-

137
00:08:47,110 --> 00:08:51,071
Shaft, Wiz, Friday,
Juice, and Roots.

138
00:08:58,788 --> 00:09:01,081
- Isn't Roots, like,
ten hours long?

139
00:09:01,208 --> 00:09:02,791
- No, you didn't-
no, you're telling me

140
00:09:02,918 --> 00:09:05,294
you haven't seen Roots.

141
00:09:05,420 --> 00:09:08,130
- Dude, this is what
I'm talking about.

142
00:09:17,015 --> 00:09:19,808
Oop, uh, 'scuse,
pardon me.

143
00:09:21,728 --> 00:09:23,395
- Hey, player.
- How you doing, man?

144
00:09:23,521 --> 00:09:25,439
- Hey, can I make a request?
- For sure, man.

145
00:09:25,565 --> 00:09:27,399
What are you looking for?
- Hey, man.

146
00:09:27,525 --> 00:09:29,693
Hook a brother up
with some old school.

147
00:09:29,819 --> 00:09:31,153
- Oh, yeah,
all right, word.

148
00:09:31,279 --> 00:09:33,364
Okay, what you looking for-
Biggie 大亨, Tupac?

149
00:09:33,490 --> 00:09:35,491
- Biggie, Tupac?

150
00:09:35,617 --> 00:09:37,451
Come on, man,
that's too new.

151
00:09:37,577 --> 00:09:40,079
I'm talking
about that old school, man.

152
00:09:40,205 --> 00:09:41,705
You know
what I'm talking about.

153
00:09:41,831 --> 00:09:44,124
- I thought I did, man,
but, like, uh, Wu-Tang Clan?

154
00:09:44,251 --> 00:09:46,293
- Pshh.
Wu-Tang Clan?

155
00:09:46,419 --> 00:09:48,879
Come on, man,
those fools just came out.

156
00:09:49,005 --> 00:09:51,465
Hook a brother up
with some of that old school.

157
00:09:51,591 --> 00:09:55,386
- Okay, I don't know what
you looking for, man. N.W. A?

158
00:09:55,512 --> 00:09:58,305
- N.W. A?

159
00:09:58,431 --> 00:10:01,392
Come on, man, what do
I look like, a kid out there?

160
00:10:01,518 --> 00:10:04,395
Talking about the N.W.A.,
fo' shizzle, my nizzle,

161
00:10:04,521 --> 00:10:06,647
criss-cross 十字形图案, applesauce 苹果酱 胡说,
Who's the Boss?

162
00:10:06,773 --> 00:10:08,315
Tony Danza at a loss.

163
00:10:08,441 --> 00:10:10,025
I don't know it, all right?

164
00:10:10,151 --> 00:10:12,444
I'm talking
about that old school.

165
00:10:12,570 --> 00:10:14,154
- Okay, you know what?

166
00:10:14,281 --> 00:10:15,698
That's my bad,
old man, all right?

167
00:10:15,824 --> 00:10:17,408
We gonna take it back,
all right?

168
00:10:17,534 --> 00:10:19,118
I'm gonna bust out some Run-DMC
for you right now.

169
00:10:19,244 --> 00:10:21,412
- Run-DMC?

170
00:10:26,334 --> 00:10:30,087
Come on, man, I don't know
about none of this Run-DMC,

171
00:10:30,213 --> 00:10:32,381
wouldn't want to be a,
onomatopoeia,

172
00:10:32,507 --> 00:10:35,217
grab a donkey by the toe,
give him gonorrhea.

173
00:10:35,343 --> 00:10:36,677
I don't know it,
all right?

174
00:10:36,803 --> 00:10:38,429
All I know
is some old school.

175
00:10:38,555 --> 00:10:40,014
And if you can't give it to me,
you can't give it to me.

176
00:10:40,140 --> 00:10:42,224
- Come on, man,
Run-DMC is old school.

177
00:10:42,350 --> 00:10:43,809
- Ahh.

178
00:10:43,935 --> 00:10:46,186
- Well, can you name a group?
- You the DJ, man.

179
00:10:46,313 --> 00:10:47,771
I ain't trying to tell you
how to do your job.

180
00:10:47,897 --> 00:10:51,108
- Grandmaster Flash.
- Oh, man, Grandmaster Fla-

181
00:10:51,234 --> 00:10:52,526
you sounding like
a kid yourself,

182
00:10:52,652 --> 00:10:54,778
talking about
Grandmaster Flash to the-

183
00:10:57,240 --> 00:10:59,408
Don't stop the boogie 摇滚乐 to the
bang bang boogie to upchuck-

184
00:10:59,534 --> 00:11:01,660
Why people talking
about upchuck 呕吐?

185
00:11:01,786 --> 00:11:03,203
- Sugarhill Gang.

186
00:11:03,330 --> 00:11:04,830
- Brother, how we end up
in Candyland?

187
00:11:04,956 --> 00:11:06,457
- You know what, man?

188
00:11:06,583 --> 00:11:08,834
If you want some old school,
then you come back here

189
00:11:08,960 --> 00:11:10,586
and find it yourself.

190
00:11:10,712 --> 00:11:13,964
I'm going on break.

191
00:11:14,090 --> 00:11:16,175
- All right, then, well,
get myself back here right here.

192
00:11:16,301 --> 00:11:17,551
See what's going on
back here.

193
00:11:17,677 --> 00:11:19,136
What we got?
What we got?

194
00:11:19,262 --> 00:11:20,763
Let's see-
nope, nope, nope, nope,

195
00:11:20,889 --> 00:11:23,307
nope, nope, nope, nope,
nope, nope, nope, nope, nope.

196
00:11:23,433 --> 00:11:26,560
Ah, there,
now we talking.

197
00:11:31,358 --> 00:11:33,776
Aw, yeah, yeah,
yeah, yeah.

198
00:11:33,902 --> 00:11:36,111
Ah, yeah.

199
00:11:36,237 --> 00:11:38,155
Daddy.

200
00:11:38,281 --> 00:11:40,115
Got to pull it.
They got to pull it.

201
00:11:40,241 --> 00:11:41,533
They got to pull it.
They got to pull it.

202
00:11:41,659 --> 00:11:42,701
They got to pull it.

203
00:11:42,827 --> 00:11:46,330
- What the fuck
is happening?

204
00:11:54,339 --> 00:11:56,006
- There he is.
- Hey, hey, hey, what's up?

205
00:11:56,132 --> 00:11:57,800
- Hey, man.
What up, man?

206
00:11:57,926 --> 00:11:59,593
- Hey, here's the deal.

207
00:11:59,719 --> 00:12:04,306
100%, grass-fed 吃草的 Japanese
Kobe beef, $35 a pound.

208
00:12:04,432 --> 00:12:05,474
- That's what
I'm talking about.

209
00:12:05,600 --> 00:12:06,809
- Mm-hmm.
- All right.

210
00:12:06,935 --> 00:12:08,852
Let's grill 烧烤
these little homeys up.

211
00:12:08,978 --> 00:12:11,855
- Right, all right. Oh, man.
Is that ground chuck 地摊货, man?

212
00:12:11,981 --> 00:12:13,982
No offense, I ain't
trying to park my Bentley

213
00:12:14,109 --> 00:12:16,402
next to your Toyota,
you know what I'm saying?

214
00:12:16,528 --> 00:12:18,237
- Come on, man.
- Oh, you just putting them

215
00:12:18,363 --> 00:12:19,738
right next
to the frozen patties 冷冻馅饼.

216
00:12:19,864 --> 00:12:21,323
Yeah, all right.

217
00:12:21,449 --> 00:12:22,908
- Hey, relax, man.
I'm gonna cook them good.

218
00:12:23,034 --> 00:12:24,326
- Okay, yeah.

219
00:12:24,452 --> 00:12:26,370
- All right, so how's
Teresa doing?

220
00:12:26,496 --> 00:12:27,955
- She's good. She's good.
She's sad she couldn't be here.

221
00:12:28,081 --> 00:12:29,790
You know,
she's got a closing today.

222
00:12:29,916 --> 00:12:31,750
You want to flash-cook those
because they're real lean 倾斜的.

223
00:12:31,876 --> 00:12:33,585
You know,
they're grass-fed.

224
00:12:33,711 --> 00:12:35,754
So you just want to probably
flip them right-right now.

225
00:12:35,880 --> 00:12:37,798
All right.

226
00:12:37,924 --> 00:12:39,174
- Hey, who wants cheese
on their burger?

227
00:12:39,300 --> 00:12:40,717
- Nobody, nobody.
Nobody wants cheese.

228
00:12:40,844 --> 00:12:42,261
- Anybody want
a double burger?

229
00:12:42,387 --> 00:12:43,971
- No, nobody wants
a double burger.

230
00:12:44,097 --> 00:12:46,473
That's like putting
a diamond ring on a pig hoof 猪蹄.

231
00:12:46,599 --> 00:12:48,517
I'm sorry, but the grass
that these cows ate

232
00:12:48,643 --> 00:12:51,854
costs more than this house.
- All right, no double burgers.

233
00:12:51,980 --> 00:12:53,856
- No double burgers-
- Hey, what the hell?

234
00:12:53,982 --> 00:12:55,232
- You may as well
just poop on that.

235
00:12:55,358 --> 00:12:56,483
- What?

236
00:12:56,609 --> 00:12:58,777
- You just ketchup 蕃茄酱-raped
my Kobe burger 神户汉堡.

237
00:12:58,903 --> 00:13:02,030
This was a mistake.
This-no, no.

238
00:13:02,157 --> 00:13:03,824
No, no. Uh-uh. No, no, no,
no, no, no, no.

239
00:13:03,950 --> 00:13:06,160
See, this meat
came all the way over here

240
00:13:06,286 --> 00:13:09,872
from the Pacific Ocean
by boat

241
00:13:09,998 --> 00:13:14,251
because the air pressure
wouldn't ruin its integrity.

242
00:13:14,377 --> 00:13:15,627
Give me that.

243
00:13:15,753 --> 00:13:19,131
You are not worthy.
Gimme that burger.

244
00:13:19,257 --> 00:13:20,924
This is ridiculous.
This is 126-oh!

245
00:13:21,050 --> 00:13:23,719
This little mother-

246
00:13:23,845 --> 00:13:27,097
$126 worth of beef.

247
00:13:27,223 --> 00:13:30,225
Y'all are not worthy.

248
00:13:30,351 --> 00:13:32,728
- Did he take my dog?

249
00:13:36,774 --> 00:13:38,525
So you know what
white people hate?

250
00:13:38,651 --> 00:13:40,903
Being called racist.
- Yes.

251
00:13:41,029 --> 00:13:43,280
White people flip out 晕倒...

252
00:13:43,406 --> 00:13:44,448
- They hate it.
- When you call them a racist.

253
00:13:44,574 --> 00:13:46,241
- It's like, "I am not."

254
00:13:46,367 --> 00:13:48,619
- "I am not a racist."

255
00:13:48,745 --> 00:13:51,580
Racist is the "N" word
for white people.

256
00:13:51,706 --> 00:13:54,625
- Yes, it is.

257
00:13:54,751 --> 00:13:56,710
- Nothing else works.

258
00:13:56,836 --> 00:13:58,253
Cracker didn't work.
- Nope.

259
00:13:58,379 --> 00:14:00,172
- Honky 白鬼子 didn't work.
- Sounds too fun.

260
00:14:00,298 --> 00:14:01,882
- Old fang, whatever that
means, didn't work.

261
00:14:02,008 --> 00:14:03,634
- I've never
even heard that.

262
00:14:03,760 --> 00:14:05,552
But we're good at, like,
figuring out how to use that.

263
00:14:05,678 --> 00:14:07,513
It's like a Jedi mind trick.
It goes like this.

264
00:14:07,639 --> 00:14:09,473
Be a white guy.
Say something white.

265
00:14:09,599 --> 00:14:10,891
- Okay, all right.
- Okay.

266
00:14:11,017 --> 00:14:12,559
- Excuse me, sir,
you can't bring

267
00:14:12,685 --> 00:14:14,478
an outside beverage 饮料
into the theater.

268
00:14:14,604 --> 00:14:15,646
- Racist.
- Right this way.

269
00:14:18,483 --> 00:14:21,026
Just like that.

270
00:14:21,152 --> 00:14:22,319
So I just finished
episode 插画 four.

271
00:14:22,445 --> 00:14:24,863
- Okay, so the Lannisters...
- Yep.

272
00:14:24,989 --> 00:14:27,074
- Are the blonde 白皙的 people.
- Mm-hmm, mm-hmm.

273
00:14:27,200 --> 00:14:28,784
- And the female Lannister
is married

274
00:14:28,910 --> 00:14:31,537
to Robert Barantheon,
who is the king.

275
00:14:31,663 --> 00:14:33,247
- Is it Barantheon
or Baratheon?

276
00:14:33,373 --> 00:14:34,665
- Right, right.

277
00:14:34,791 --> 00:14:38,710
- And then the other
blonde girl is the, um-

278
00:14:38,836 --> 00:14:40,671
- Can I get you something?

279
00:14:40,797 --> 00:14:42,422
- Oh, I don't know, um,

280
00:14:42,549 --> 00:14:45,801
three kamikaze shots,
whatever.

281
00:14:45,927 --> 00:14:48,679
Oh, my God, I can't believe
he just did that.

282
00:14:48,805 --> 00:14:50,138
- What's that?

283
00:14:50,265 --> 00:14:51,848
- Served me
before you guys.

284
00:14:51,975 --> 00:14:54,935
I'm really against that.
I'm so sorry.

285
00:14:55,061 --> 00:14:57,521
- No, it's okay.
We, uh-we've got beers.

286
00:14:57,647 --> 00:15:02,150
- No, I mean, I'm sorry
about everything.

287
00:15:02,277 --> 00:15:03,986
- Oh, that's fine.
It's fine.

288
00:15:04,112 --> 00:15:07,406
- No, no, it's been
100 years of not fine.

289
00:15:07,532 --> 00:15:09,116
I mean,
20 years ago,

290
00:15:09,242 --> 00:15:11,785
you guys wouldn't have even
been allowed in here.

291
00:15:11,911 --> 00:15:14,788
- 20 years-
that would be 1992.

292
00:15:17,667 --> 00:15:21,128
- You're both
really beautiful.

293
00:15:21,254 --> 00:15:22,588
- Oh, she's done.

294
00:15:22,714 --> 00:15:24,381
- That just happened,
didn't it?

295
00:15:24,507 --> 00:15:26,925
- Mm-hmm.
- Yeah, amazing.

296
00:15:27,051 --> 00:15:28,427
Where were we?
- So the Lannisters-

297
00:15:28,553 --> 00:15:29,886
- Okay.
- She's the queen.

298
00:15:30,013 --> 00:15:31,763
- Jaime Lannister
is the other one.

299
00:15:31,889 --> 00:15:33,390
- Jaime and-
- Yo, yo, yo, what up, man?

300
00:15:33,516 --> 00:15:35,767
Hey, give me another lager,
you know what I mean?

301
00:15:35,893 --> 00:15:39,021
Spilled 使溢出 my last one on my Tribe
shirt-can you believe that?

302
00:15:39,147 --> 00:15:41,815
But whatevs,
got this thing in '93-

303
00:15:41,941 --> 00:15:44,610
Midnight Marauders tour.
Check that out.

304
00:15:44,736 --> 00:15:46,320
It's everywhere. Oh, man.
- Mm-hmm.

305
00:15:46,446 --> 00:15:49,906
- Hey, uh, who's your
favorite member of Tribe 部落?

306
00:15:50,033 --> 00:15:52,993
- Smok-smoke-Smokey.
Know what I mean?

307
00:15:53,119 --> 00:15:56,788
Love him, dude, more behind
the scenes, but he was, like-

308
00:15:56,914 --> 00:15:58,790
All right, good.
See you guys around though.

309
00:15:58,916 --> 00:16:00,500
- Mm-hmm. Okay.
- Cool.

310
00:16:00,627 --> 00:16:02,628
- One more, one more, one more.
Come on, son.

311
00:16:02,754 --> 00:16:05,255
Boom.
All right, late.

312
00:16:05,381 --> 00:16:07,716
- Oh, he spun 旋转. He spun.
- Wow.

313
00:16:07,842 --> 00:16:09,926
Second person to not wait
for their drink, by the way.

314
00:16:10,053 --> 00:16:11,178
- Yep.

315
00:16:11,304 --> 00:16:12,721
Okay, let me
ask you this, then.

316
00:16:12,847 --> 00:16:14,431
What's with
the barbarian dude

317
00:16:14,557 --> 00:16:16,099
who looks like
a huge Dave Navarro?

318
00:16:16,225 --> 00:16:17,434
- Okay.
- Who's that guy?

319
00:16:17,560 --> 00:16:19,102
- Oh...

320
00:16:19,228 --> 00:16:22,147
I need a drink.
Just finished Amistad 友谊.

321
00:16:22,273 --> 00:16:24,316
I saw it 3 times,
and I'll see it 100 more times

322
00:16:24,442 --> 00:16:25,817
because, like,
I'm happy to do it.

323
00:16:25,943 --> 00:16:27,694
If that's what
those guys went through,

324
00:16:27,820 --> 00:16:29,946
I'm happy to sit through it,
you know, 300 more times

325
00:16:30,073 --> 00:16:31,531
or whatever,
you know, it takes.

326
00:16:31,658 --> 00:16:33,033
- Yeah, it's pretty intense 强烈的.

327
00:16:33,159 --> 00:16:35,160
- See, my thing is,
is, like, I don't think

328
00:16:35,286 --> 00:16:37,871
one person should own
another person, period.

329
00:16:37,997 --> 00:16:39,414
- I'm glad that that's what
you got from that movie.

330
00:16:39,540 --> 00:16:41,249
- That's a true statement.

331
00:16:41,376 --> 00:16:42,751
- Yeah, I mean, I don't care
when it happened, all right?

332
00:16:42,877 --> 00:16:44,670
It should never happen.
It should never occur.

333
00:16:44,796 --> 00:16:46,046
But, like, I'm preaching 说教
to the choir.

334
00:16:46,172 --> 00:16:47,589
You guys know
what I'm talking about.

335
00:16:47,715 --> 00:16:49,174
- Yeah, we do.
- Yeah, we do.

336
00:16:49,300 --> 00:16:51,009
- It's like, why did
anybody even do that?

337
00:16:51,135 --> 00:16:52,636
I'm like, "No, you didn't.
No, you didn't.

338
00:16:52,762 --> 00:16:54,262
What you doing, girlfriend?
No, you didn't."

339
00:16:54,389 --> 00:16:56,348
I'm ashamed.
- You know what?

340
00:16:56,474 --> 00:16:58,350
It's really probably okay
because you weren't there.

341
00:16:58,476 --> 00:17:01,478
- Okay, well, we can come up
with excuses all day, all day.

342
00:17:01,604 --> 00:17:04,606
- It's not an excuse so much.
- Let's just not do that.

343
00:17:04,732 --> 00:17:05,982
Let's not do that.

344
00:17:06,109 --> 00:17:07,901
- Look, hey,
bottom line, we cool.

345
00:17:08,027 --> 00:17:09,486
Right?
- Yeah.

346
00:17:09,612 --> 00:17:10,737
- All right.
- Yeah, we are cool.

347
00:17:10,863 --> 00:17:12,698
- We're kickity-cool?
- Yes.

348
00:17:12,824 --> 00:17:14,199
- What's that?

349
00:17:14,325 --> 00:17:15,367
- Didn't say anything.
- Nothing.

350
00:17:15,493 --> 00:17:17,619
- Oh, you didn't-

351
00:17:17,745 --> 00:17:18,995
All right,
four more years.

352
00:17:22,667 --> 00:17:24,626
- Hey, if it makes
you guys feel any better,

353
00:17:24,752 --> 00:17:26,712
black people make me
really uncomfortable.

354
00:17:28,423 --> 00:17:30,006
Thank you.

355
00:17:33,970 --> 00:17:37,514
- So uh, my favorite sport
is mixed martial arts.

356
00:17:37,640 --> 00:17:41,393
- It is. I know that.
You're a big fan.

357
00:17:41,519 --> 00:17:43,228
- Oh, I love it.

358
00:17:43,354 --> 00:17:44,938
And I think from watching it,
I think I've picked it up.

359
00:17:45,064 --> 00:17:46,690
I pick up some of that-

360
00:17:46,816 --> 00:17:49,609
- I'm sorry. You think
that you've picked it up?

361
00:17:49,736 --> 00:17:51,319
- I can get out
of any attack.

362
00:17:51,446 --> 00:17:52,529
- Can l-Really?
Can I do a thing?

363
00:17:52,655 --> 00:17:53,905
- Do whatever you want.

364
00:17:54,031 --> 00:17:55,198
- There's, like,
one thing that I know.

365
00:17:55,324 --> 00:17:56,366
- Do whatever you try.
- So what if l-

366
00:17:56,492 --> 00:17:59,202
If I do this,
if I do that-

367
00:17:59,328 --> 00:18:00,662
- All right,
you're my friend,

368
00:18:00,788 --> 00:18:02,581
and I know
you're not going to hurt me.

369
00:18:02,707 --> 00:18:04,499
You're my friend, and I know
you're not going to hurt me.

370
00:18:04,625 --> 00:18:06,251
You're my friend and I know
you're not going to hurt me.

371
00:18:06,377 --> 00:18:08,128
So there's no way-
- Of course I'm not going hurt-

372
00:18:08,254 --> 00:18:10,797
- Know who
you're dealing with.

373
00:18:10,923 --> 00:18:14,426
See what I did there?

374
00:18:24,604 --> 00:18:27,773
Hey, as far as I'm concerned,
this Saturday night,

375
00:18:27,899 --> 00:18:29,858
there's not even going
to be a fight.

376
00:18:29,984 --> 00:18:32,110
I'm gonna mercy-kill
this old man.

377
00:18:32,236 --> 00:18:34,821
- Derek is very
good person.

378
00:18:34,947 --> 00:18:37,282
But soon,
he will learn manners.

379
00:18:37,408 --> 00:18:39,201
I will torture
his body

380
00:18:39,327 --> 00:18:41,745
so that his soul
learns to be humble.

381
00:18:41,871 --> 00:18:43,872
- I'm gonna dance around him
while he tries

382
00:18:43,998 --> 00:18:47,793
to hit me with his cane 手帐,
old man.

383
00:18:47,919 --> 00:18:49,920
- God chose me
for this fight.

384
00:18:50,046 --> 00:18:53,465
God is the teacher,
Derek is the student,

385
00:18:53,591 --> 00:18:56,009
and I am
God's instrument.

386
00:18:56,135 --> 00:18:59,054
When I squeeze 挤压
your lungs 肺, Derek,

387
00:18:59,180 --> 00:19:01,389
and you beg me for life,

388
00:19:01,516 --> 00:19:04,100
then your heart
will open up to the Lord.

389
00:19:04,227 --> 00:19:07,270
- I'm gonna knock him out
round one, bitch.

390
00:19:07,396 --> 00:19:11,358
Wait.
What did he say again?

391
00:19:11,484 --> 00:19:13,860
He said God chose him.

392
00:19:13,986 --> 00:19:17,072
That doesn't
even make sense, really.

393
00:19:17,198 --> 00:19:20,367
- When you eat
through plastic tube,

394
00:19:20,493 --> 00:19:24,204
when you are paralyzed
from neck down,

395
00:19:24,330 --> 00:19:25,956
then your family

396
00:19:26,082 --> 00:19:29,501
will gather
around your hospital bed

397
00:19:29,627 --> 00:19:31,670
to see the new Derek.

398
00:19:33,965 --> 00:19:36,299
- Okay, he know
we just talking here, right?

399
00:19:36,425 --> 00:19:38,093
I mean, we just-

400
00:19:38,219 --> 00:19:40,679
We just getting people
interested in the fight-

401
00:19:40,805 --> 00:19:43,640
because I'm sorry-
is this nigga crazy?

402
00:19:43,766 --> 00:19:48,895
- God's lessons
are so beautiful.

403
00:19:52,650 --> 00:19:54,359
- "God's lessons
are beautiful"?

404
00:19:54,485 --> 00:19:56,570
I'm sorry.
Who put this fight together?

405
00:19:56,696 --> 00:19:58,655
This is just-

406
00:19:58,781 --> 00:20:02,158
If y'all got an actual
crazy person for me to fight,

407
00:20:02,285 --> 00:20:03,994
well, that's not fair
to me...

408
00:20:04,120 --> 00:20:05,412
Or him.

409
00:20:18,634 --> 00:20:21,094
- What you mean,
you can't find him?

410
00:20:21,220 --> 00:20:22,721
Well, where is he?

411
00:20:22,847 --> 00:20:24,347
Yo, man, where my manager at?
Steve?

412
00:20:24,473 --> 00:20:27,475
How's it-

413
00:20:37,445 --> 00:20:39,654
- Thank you so much.

414
00:20:39,780 --> 00:20:41,990
- All right, so here's
the thing.

415
00:20:42,116 --> 00:20:44,534
Keegan can't say good-bye,
so I'm gonna say good-bye.

416
00:20:44,660 --> 00:20:45,952
- Why can't l-
What?

417
00:20:46,078 --> 00:20:47,954
- Broke your larynx 喉.
That's why.

418
00:20:48,080 --> 00:20:49,581
Broke your larynx.
Good night, everybody.

419
00:20:49,707 --> 00:20:51,207
Good night.

420
00:20:58,007 --> 00:21:00,884
- ♪ I'm gonna do
my one line here ♪

421
00:21:01,010 --> 00:21:03,553
- Oh, yeah.

422
00:21:05,264 --> 00:21:06,514
- Remember the video stores,
there was always

423
00:21:06,641 --> 00:21:09,267
new releases, but then
there was also new arrivals.

424
00:21:09,393 --> 00:21:11,186
If you have Julia Roberts
in your movie,

425
00:21:11,312 --> 00:21:12,812
it's a new release.

426
00:21:12,939 --> 00:21:14,564
If Eric Roberts is in it,
it's a new arrival.

427
00:21:14,690 --> 00:21:16,816
Nicolas Cage,
new release.

428
00:21:16,943 --> 00:21:20,028
Nicolas Cage,
new arrival.

