﻿1
00:00:02,086 --> 00:00:03,878
- Now I know
this plan is foolproof 万无一失的.

2
00:00:04,004 --> 00:00:05,713
Check this out.

3
00:00:05,839 --> 00:00:10,093
First of all, you and me
start working at the bank.

4
00:00:10,219 --> 00:00:11,552
Doesn't matter
the position, okay,

5
00:00:11,679 --> 00:00:13,471
just so long as we get
in there, all right?

6
00:00:13,597 --> 00:00:15,765
Then we just
go there every day,

7
00:00:15,891 --> 00:00:18,518
do the work,
gain their trust

8
00:00:18,644 --> 00:00:21,145
until we get them
in the palm 手掌 of our hand.

9
00:00:21,271 --> 00:00:24,232
- All right.
So how we get the money?

10
00:00:24,358 --> 00:00:26,234
- That's the beauty
of it, bro.

11
00:00:26,360 --> 00:00:30,154
They deposit the money
into our bank accounts,

12
00:00:30,280 --> 00:00:32,490
week after week,
month after month.

13
00:00:32,616 --> 00:00:34,951
They're not even gonna know
they're being robbed.

14
00:00:35,077 --> 00:00:38,162
And then 20
or 30 years later,

15
00:00:38,288 --> 00:00:43,960
we walk out the front door
like nothing even happened.

16
00:00:46,797 --> 00:00:50,049
- Motherfucker,
that's called a job!

17
00:01:15,993 --> 00:01:17,660
- Hello.

18
00:01:17,786 --> 00:01:19,036
Good evening.

19
00:01:19,163 --> 00:01:20,371
- Thank you.

20
00:01:20,497 --> 00:01:22,290
- Thank you so much
for coming out.

21
00:01:22,416 --> 00:01:23,458
Uh, I am Keegan.
- I am Jordan.

22
00:01:23,584 --> 00:01:24,792
- And this is
Key and Peele.

23
00:01:24,918 --> 00:01:25,960
- Welcome to the show.
- Thank you for coming.

24
00:01:26,086 --> 00:01:28,921
- Thank you.

25
00:01:29,047 --> 00:01:30,423
Now, Keegan and I
have noticed

26
00:01:30,549 --> 00:01:32,133
that there, uh, seems
to be a huge difference

27
00:01:32,259 --> 00:01:33,634
between white college movies
and black college movies.

28
00:01:33,761 --> 00:01:34,802
- It's huge, yes.

29
00:01:34,928 --> 00:01:36,429
In-in white
college movies-

30
00:01:36,555 --> 00:01:39,182
white college movies are always
only about having fun,

31
00:01:39,308 --> 00:01:41,893
smoking weed,
and stealing mascots 吉祥物.

32
00:01:42,019 --> 00:01:44,061
- Right, and it's all about...

33
00:01:44,188 --> 00:01:46,856
Let's drink some beer
off of some titties!

34
00:01:46,982 --> 00:01:49,609
Yeah, let's get away

35
00:01:49,735 --> 00:01:53,946
with date rape!

36
00:01:54,072 --> 00:01:56,365
- And in black college movies,
the-the lead always

37
00:01:56,492 --> 00:01:57,992
says this line,
every time:

38
00:01:58,118 --> 00:02:01,329
- "I'm gonna go
to college."

39
00:02:01,455 --> 00:02:03,206
Toward the beginning
of the film, "I'm gonna go"-

40
00:02:03,332 --> 00:02:04,999
as if someone
just told him,

41
00:02:05,125 --> 00:02:07,376
"There's no way you're
gonna go to college, man."

42
00:02:07,503 --> 00:02:08,795
"I'm gonna go."

43
00:02:08,921 --> 00:02:11,130
That's the plot 故事情节
of the film right there.

44
00:02:11,256 --> 00:02:12,757
That this guy's
gonna go to college.

45
00:02:12,883 --> 00:02:13,925
- And in a white college movie,
it-it-it-

46
00:02:14,051 --> 00:02:15,218
uh, college
is a given.

47
00:02:15,344 --> 00:02:16,969
College is a backdrop 背景.
- Oh, yeah.

48
00:02:17,095 --> 00:02:18,846
- It's about "How do I get
out of college?"

49
00:02:18,972 --> 00:02:20,306
- That's right, "How do I do
as little work as possible?"

50
00:02:20,432 --> 00:02:21,933
- "Aw, man, I'm sick
of this trust fund

51
00:02:22,059 --> 00:02:25,686
and my dad
being a legacy 遗产."

52
00:02:30,442 --> 00:02:32,944
- No, black college movies,
always the same plot:

53
00:02:33,070 --> 00:02:34,946
"I might develop
a challenging, rhythmical 有节奏的,

54
00:02:35,072 --> 00:02:37,031
extracurricular 学校课程以外的 activity"-
- Mm-hmm.

55
00:02:37,157 --> 00:02:38,699
- "That's not gonna change
the fact that I'm going

56
00:02:38,826 --> 00:02:41,494
to stay in college."
- Mm-hmm.

57
00:02:41,620 --> 00:02:42,954
It's gonna be all right,
brother, 'cause there ain't

58
00:02:43,080 --> 00:02:44,664
nobody gonna try to kick you out
of college.

59
00:02:44,790 --> 00:02:48,125
It's a black college.
You're gonna be fine.

60
00:02:48,252 --> 00:02:49,710
- I don't understand,
when is gonna be the day

61
00:02:49,837 --> 00:02:51,504
that we can have
a college movie for black people

62
00:02:51,630 --> 00:02:52,797
where we get
to act like fools?

63
00:02:52,923 --> 00:02:54,048
- I'll tell you this
right now:

64
00:02:54,174 --> 00:02:55,716
When a black man dresses
like a woman

65
00:02:55,843 --> 00:02:57,885
in order to live
in a sorority 妇女联谊会 house,

66
00:02:58,011 --> 00:02:59,637
we will have reached equality.
- That's right.

67
00:02:59,763 --> 00:03:02,181
No, actually, that-
no, that movie exists.

68
00:03:02,307 --> 00:03:04,934
Big Momma's House 3
starring Sir Martin Lawrence.

69
00:03:08,146 --> 00:03:09,605
- Well, then, we have reached
the mountaintop 山顶.

70
00:03:09,731 --> 00:03:12,066
All right.

71
00:03:12,192 --> 00:03:17,113
- Ahh!

72
00:03:17,239 --> 00:03:19,156
Omega Pi Omega!

73
00:03:19,283 --> 00:03:21,325
- Omega Pi Omega!

74
00:03:21,451 --> 00:03:24,954
Omega Pi Omega!

75
00:03:25,080 --> 00:03:27,290
- Omega Pi Omega!
- Yeah!

76
00:03:27,416 --> 00:03:28,624
- Yo, brand me, son.
- Yeah.

77
00:03:28,750 --> 00:03:30,167
- Brand me,
do it on my chest.

78
00:03:30,294 --> 00:03:31,377
- Yeah.
- Straight down the middle, son.

79
00:03:31,503 --> 00:03:32,545
- Yeah, yeah,
yeah, yeah, yeah!

80
00:03:32,671 --> 00:03:35,923
- Ahh!

81
00:03:36,049 --> 00:03:37,508
- Yeah, son!

82
00:03:37,634 --> 00:03:39,552
- Oh, yeah-yo...
yo, yo, hold up, man.

83
00:03:39,678 --> 00:03:40,720
Come on, wait.

84
00:03:40,846 --> 00:03:42,179
Wait, wait, that's...

85
00:03:42,306 --> 00:03:44,098
that shit's upside-down, son.

86
00:03:44,224 --> 00:03:46,976
It-it's to the side
a little bit too.

87
00:03:47,102 --> 00:03:48,519
- Oh, shit, well, okay,
my-my bad, dog.

88
00:03:48,645 --> 00:03:50,062
My bad, dog.
You wanna keep doing this shit?

89
00:03:50,188 --> 00:03:52,315
Let's go!
- Yeah, man.

90
00:03:52,441 --> 00:03:55,109
I'm just-I'm saying,
yo, be careful.

91
00:03:55,235 --> 00:03:56,319
- I got you, dog,
I got you... yeah!

92
00:03:56,445 --> 00:03:58,321
- Ahh!

93
00:03:58,447 --> 00:04:00,865
- What's up?
That's what's up!

94
00:04:00,991 --> 00:04:02,366
- Yeah.
- Straight down the middle, son!

95
00:04:02,492 --> 00:04:04,535
- You can't go back
to middle now, son.

96
00:04:04,661 --> 00:04:06,162
- What?
- We already committed

97
00:04:06,288 --> 00:04:08,831
to the upside-down
and to the side.

98
00:04:08,957 --> 00:04:13,377
- I got you, dog, yeah!
- Ahh!

99
00:04:13,503 --> 00:04:14,962
What are you doing, man?

100
00:04:15,088 --> 00:04:16,756
- You said upside-down
and to the side.

101
00:04:16,882 --> 00:04:18,132
- That's three omegas!

102
00:04:18,258 --> 00:04:19,800
- Oh, man,
I forgot the Pi.

103
00:04:19,927 --> 00:04:24,388
Yeah!
- Ahh!

104
00:04:24,514 --> 00:04:28,893
Oh...

105
00:04:29,019 --> 00:04:30,645
- Oh, man,
I mean, you know...

106
00:04:30,771 --> 00:04:32,605
that-that, man...
that-that...

107
00:04:32,731 --> 00:04:33,773
that could be
a rocket ship,

108
00:04:33,899 --> 00:04:38,110
or-or
a lighthouse 灯塔.

109
00:04:38,236 --> 00:04:40,571
Naw, man, you straight up
got a dick on your chest 胸部.

110
00:04:40,697 --> 00:04:43,574
- Ahh!

111
00:04:46,370 --> 00:04:48,287
- What do we do?

112
00:04:48,413 --> 00:04:50,498
They told us at the station
to come down here and fill time,

113
00:04:50,624 --> 00:04:53,626
but, uh, there's
no story here.

114
00:04:53,752 --> 00:04:56,128
- In this neighborhood 地区,
there's always a story.

115
00:04:56,254 --> 00:04:58,547
Here, gimme the name
of a mythical 神话的 creature.

116
00:04:58,674 --> 00:05:00,007
- Mythical creature?
I don't-

117
00:05:00,133 --> 00:05:01,592
- Trust me.
- Uh, I don't-a pegasus 珀伽索斯.

118
00:05:01,718 --> 00:05:02,927
- Pegasus, perfect.

119
00:05:03,053 --> 00:05:07,348
Just roll 开始工作,
watch, and learn.

120
00:05:07,474 --> 00:05:09,016
- They can stay in my house
if they want to-

121
00:05:09,142 --> 00:05:10,977
- Hi, there. Rick Nicholsby,
Channel Six News.

122
00:05:11,103 --> 00:05:13,104
We've had reports
of a pegasus in the area.

123
00:05:13,230 --> 00:05:14,438
Has anybody seen
anything like that?

124
00:05:14,564 --> 00:05:16,524
- Aw, yeah, man,
I seen a pegasus.

125
00:05:16,650 --> 00:05:19,318
I seen it, yeah, every day.
- I sees it too, man.

126
00:05:19,444 --> 00:05:21,028
We both see
that pegasus, dog.

127
00:05:21,154 --> 00:05:23,030
- Yeah, I saw it
with my own two eyes, man.

128
00:05:23,156 --> 00:05:24,907
That horse had
a big old snake head-

129
00:05:25,033 --> 00:05:27,451
- Naw, man, it ain't
got no snake head, fool.

130
00:05:27,577 --> 00:05:29,578
Y'all, that-that horse
had some big wings, man.

131
00:05:29,705 --> 00:05:30,913
- Yeah, yeah.
- It's all flying and shit.

132
00:05:31,039 --> 00:05:32,373
- Yeah, that horse
had big old wings.

133
00:05:32,499 --> 00:05:34,125
Man, like, two sets
of wings, yeah.

134
00:05:34,251 --> 00:05:36,002
- Y'all see the pegasus?
- Yeah.

135
00:05:36,128 --> 00:05:38,504
- Everybody up in here
seen the pegasus, man.

136
00:05:38,630 --> 00:05:40,756
- Curiosity 好奇心 has brought out
large crowds 人群

137
00:05:40,882 --> 00:05:44,010
to see the alleged 所谓的 pegasus in
this South Lennox neighborhood.

138
00:05:44,136 --> 00:05:45,344
- Look, there it is.

139
00:05:45,470 --> 00:05:48,848
- I'm gonna find it,
break it,

140
00:05:48,974 --> 00:05:51,434
and then ride 骑 it
to the pegasus treasure.

141
00:05:51,560 --> 00:05:54,478
- Residents 居民 say the creature
resembles 像 this amateur 业余的 sketch 素描.

142
00:05:54,604 --> 00:05:56,063
- Kicked the fuck
out of my car.

143
00:05:56,189 --> 00:05:57,732
City's gotta pay
for that shit too.

144
00:05:57,858 --> 00:06:00,026
I ain't got no coverage 新稳报道
for no pegasus, bitch.

145
00:06:00,152 --> 00:06:01,235
- He land on my roof, man.
Look at my roof, man.

146
00:06:01,361 --> 00:06:02,486
He busted it up.

147
00:06:02,612 --> 00:06:04,280
He busted 爆破 破产
the whole thing up, man.

148
00:06:04,406 --> 00:06:06,407
Yeah, he been-he been-he been
sitting up there every day.

149
00:06:06,533 --> 00:06:08,284
- While many revel 狂欢
in the possibility

150
00:06:08,410 --> 00:06:09,785
of seeing
the winged 有翼的 horse,

151
00:06:09,911 --> 00:06:12,288
others are not
so welcoming.

152
00:06:12,414 --> 00:06:14,749
- I just know this neighborhood
would be better

153
00:06:14,875 --> 00:06:17,126
if that pegasus
is put down.

154
00:06:17,252 --> 00:06:20,504
God ain't
putting no wings on no horse.

155
00:06:20,630 --> 00:06:23,299
- Now, see, if this was
a white neighborhood,

156
00:06:23,425 --> 00:06:25,259
animal control
would be up in here

157
00:06:25,385 --> 00:06:27,303
with a pegasus trap 陷阱.
- That's right.

158
00:06:27,429 --> 00:06:31,182
They can't take our pegasus.
This is our pegasus.

159
00:06:31,308 --> 00:06:32,475
- Pegasus up
in this motherfucker!

160
00:06:32,601 --> 00:06:33,976
- Hey, man, why you
interrupting me?

161
00:06:34,102 --> 00:06:35,144
I'm trying to talk
about the pegasus.

162
00:06:35,270 --> 00:06:36,771
- Fuck you!
- Hey!

163
00:06:36,897 --> 00:06:39,940
Don't be disrespecting pegasus!
- I love pegasus!

164
00:06:42,486 --> 00:06:45,154
- Yes, we're here live
on the scene in South Lennox,

165
00:06:45,280 --> 00:06:48,324
where riots 暴动 have broken out
over the alleged appearance

166
00:06:48,450 --> 00:06:50,576
of a flying horse.

167
00:06:54,664 --> 00:06:55,956
- Did this
actually happen?

168
00:06:56,083 --> 00:06:58,959
Did Mel Gibson actually say
to that woman-

169
00:06:59,086 --> 00:07:01,962
- His-uh, not just a woman,
his wife or girlfriend.

170
00:07:02,089 --> 00:07:04,090
- Right, whatever she was.
Did he say to her,

171
00:07:04,216 --> 00:07:07,301
"I hope you get raped 强暴
by a pack of niggers"?

172
00:07:07,427 --> 00:07:08,969
- Yes, he did.
- What?

173
00:07:09,096 --> 00:07:12,348
- He is the most racist man
in the world, yes.

174
00:07:12,474 --> 00:07:15,434
- But, seriously, no-
and, for the record,

175
00:07:15,560 --> 00:07:17,144
if-if he had said,

176
00:07:17,270 --> 00:07:19,271
"I hope you get raped by a pack
of African-Americans"-

177
00:07:19,397 --> 00:07:20,898
- Not better, not better.
- Doesn't fix it.

178
00:07:21,024 --> 00:07:23,651
Doesn't even fix it,
it's that racist.

179
00:07:23,777 --> 00:07:27,071
- Because "pack" always
denotes 表示 non-human.

180
00:07:27,197 --> 00:07:28,989
- It means animals.
- It means animals every time.

181
00:07:29,116 --> 00:07:31,200
- No, l-I would've
rather he said, uh,

182
00:07:31,326 --> 00:07:33,369
"I hope you get raped
by a group of niggers."

183
00:07:33,495 --> 00:07:34,537
- Mm-hmm.
- Uh...

184
00:07:34,663 --> 00:07:36,539
"A, uh, gathering of niggers"

185
00:07:36,665 --> 00:07:37,706
would have been fine.

186
00:07:37,833 --> 00:07:39,917
- "Congregation （教堂里的）会众."

187
00:07:40,043 --> 00:07:41,293
- A congre-please.

188
00:07:44,131 --> 00:07:45,840
No, but it's,
like, a pack.

189
00:07:45,966 --> 00:07:47,299
"Pack" is the most
racist thing

190
00:07:47,425 --> 00:07:48,801
I've ever heard anyone say
in my life.

191
00:07:48,927 --> 00:07:50,761
- I would rather someone say,
"You got raped

192
00:07:50,887 --> 00:07:52,179
by a group of niggers,"

193
00:07:52,305 --> 00:07:54,640
than "A pack
of African-Americans

194
00:07:54,766 --> 00:07:56,433
went to a physics 物理学
presentation."

195
00:07:56,560 --> 00:07:58,352
- Yes, yes.

196
00:07:58,478 --> 00:08:00,229
Right?
- I would be less offended.

197
00:08:02,566 --> 00:08:04,525
- Hey, did you
see that nigga?

198
00:08:04,651 --> 00:08:07,027
- That nigga
looked like Busta Rhymes

199
00:08:07,154 --> 00:08:09,280
and Maya Angelou
had a baby.

200
00:08:09,406 --> 00:08:11,866
Yeah, yeah.

201
00:08:11,992 --> 00:08:15,202
That was
one jacked-up-looking nigger.

202
00:08:19,958 --> 00:08:22,168
- Uh-oh.
You just dropped the N-word,

203
00:08:22,294 --> 00:08:24,211
but you're not an "N."

204
00:08:24,337 --> 00:08:26,922
Lucky for you, there's a way
to avoid these situations.

205
00:08:27,048 --> 00:08:28,465
Introducing
the new smartphone application:

206
00:08:28,592 --> 00:08:29,967
The negraph.

207
00:08:30,093 --> 00:08:32,178
Finally, a technology
specifically designed

208
00:08:32,304 --> 00:08:35,389
to help you determine whether
or not you can drop the N-bomb.

209
00:08:35,515 --> 00:08:36,682
- I can say it!

210
00:08:36,808 --> 00:08:39,226
We can
all say it!

211
00:08:39,352 --> 00:08:41,395
- Not in this lifetime.

212
00:08:41,521 --> 00:08:43,856
- To see your negraph,
just enter your name,

213
00:08:43,982 --> 00:08:45,858
age, and color
of your skin,

214
00:08:45,984 --> 00:08:48,819
or just the color
of your skin.

215
00:08:48,945 --> 00:08:52,406
- I've got the negraph
on my phone and my iPad.

216
00:08:52,532 --> 00:08:54,408
My phone says
I can't ever say it.

217
00:08:54,534 --> 00:08:56,994
My iPad says
I can't ever say it...

218
00:08:57,120 --> 00:08:58,329
but bigger!

219
00:08:58,455 --> 00:09:01,081
- The negraph-
can you say the N-word?

220
00:09:01,208 --> 00:09:03,876
- I can say nigger
all day long.

221
00:09:04,002 --> 00:09:06,086
Nigger!

222
00:09:06,213 --> 00:09:08,339
- No.

223
00:09:08,465 --> 00:09:10,966
I'd like to, but...
says it right here.

224
00:09:11,092 --> 00:09:12,509
- The negraph.

225
00:09:12,636 --> 00:09:14,845
And also available
at the App Store, the fagchart.

226
00:09:17,682 --> 00:09:19,475
- Uh, greetings,
adventurers.

227
00:09:19,601 --> 00:09:21,894
- Greetings.
- Greetings.

228
00:09:22,020 --> 00:09:23,395
- As you all
can see here,

229
00:09:23,521 --> 00:09:26,106
we have a new traveler
in our ranks.

230
00:09:26,233 --> 00:09:27,691
Uh, this is
my cousin Tyrell,

231
00:09:27,817 --> 00:09:31,612
and he will be controlling
the player character.

232
00:09:31,738 --> 00:09:35,032
- His name is Kanye.
He's a giant, yo.

233
00:09:35,158 --> 00:09:38,285
Uh, dear cousin...

234
00:09:38,411 --> 00:09:41,372
traditionally, a-a giant
is not assumed 假装的 by a player.

235
00:09:41,498 --> 00:09:43,249
It is a chaotic 混乱的, evil 邪恶的-

236
00:09:43,375 --> 00:09:46,418
- Yeah, but I want
to be a giant, yo.

237
00:09:46,544 --> 00:09:48,504
All big.

238
00:09:48,630 --> 00:09:50,965
- Oh, okay. Fine.

239
00:09:51,091 --> 00:09:54,510
Well, travelers, you are joined
on your adventure today

240
00:09:54,636 --> 00:09:58,472
by a giant
named Kanye.

241
00:09:58,598 --> 00:10:00,057
Now when last we left you,

242
00:10:00,183 --> 00:10:02,434
you were at the Inn
of the North Star

243
00:10:02,560 --> 00:10:04,395
in the town of Isledor.

244
00:10:04,521 --> 00:10:05,729
How do you begin
your adventure?

245
00:10:05,855 --> 00:10:07,856
- I wanna get
some bitches.

246
00:10:07,983 --> 00:10:09,525
Where the club at
at Isledor, yo?

247
00:10:09,651 --> 00:10:11,235
- Tyrell, this is
highly unusual.

248
00:10:11,361 --> 00:10:12,778
I think the guys probably
just wanna go on the quest 探索-

249
00:10:12,904 --> 00:10:14,488
- Um, Stephen?
- Yes?

250
00:10:14,614 --> 00:10:17,449
- I would like to join
Kanye the giant in his quest.

251
00:10:17,575 --> 00:10:19,618
- Seriously?

252
00:10:21,538 --> 00:10:22,705
All right.

253
00:10:22,831 --> 00:10:25,833
The two of you
enter the tavern 酒馆.

254
00:10:25,959 --> 00:10:27,084
What do you say?

255
00:10:27,210 --> 00:10:29,586
- Kanye the giant
orders Alize.

256
00:10:29,713 --> 00:10:31,505
- "Hmm," says the barkeep 酒吧间招待.

257
00:10:31,631 --> 00:10:34,425
"Uh, I know not of this foreign
beverage 饮料 of which you speak.

258
00:10:34,551 --> 00:10:36,593
Perhaps you'd enjoy
an Elven ale?"

259
00:10:36,720 --> 00:10:38,220
- No, we both
want the Alize.

260
00:10:38,346 --> 00:10:39,763
- Alize does not exist
in this world.

261
00:10:39,889 --> 00:10:41,890
- Kanye the giant
slaps that bitch.

262
00:10:42,017 --> 00:10:43,183
- Oh!

263
00:10:43,310 --> 00:10:44,393
- Kanye the giant attempts

264
00:10:44,519 --> 00:10:46,395
to slap the bartender 酒吧间男招待.
Here.

265
00:10:46,521 --> 00:10:48,022
The bartender
is half-Hobbit 半霍比特人

266
00:10:48,148 --> 00:10:49,732
and very nimble 敏捷,

267
00:10:49,858 --> 00:10:52,401
so you're going to have
to roll an 18 or above to hit.

268
00:10:52,527 --> 00:10:53,861
20.
- Blip.

269
00:10:53,987 --> 00:10:55,529
Slap that Hobbit's
dick off, yo.

270
00:10:55,655 --> 00:10:57,823
- Okay.
Gollin the cleric 牧师,

271
00:10:57,949 --> 00:11:00,701
would you care to restrain 抑制
Kanye the giant

272
00:11:00,827 --> 00:11:02,578
before the town guard
is called?

273
00:11:02,704 --> 00:11:04,580
- Uh, I grab money
out of the register.

274
00:11:04,706 --> 00:11:06,457
- But-but-but Gollin
the cleric is lawful good.

275
00:11:06,583 --> 00:11:08,208
This act of thievery 赃物

276
00:11:08,335 --> 00:11:09,752
would dishonor 使丢脸 his gods.
- Hey, man.

277
00:11:09,878 --> 00:11:14,048
The only gods
is money and bitches, dude.

278
00:11:14,174 --> 00:11:17,801
- Ah! I accept Kanye
the giant's gods, and, uh...

279
00:11:17,927 --> 00:11:19,887
yeah, l-l-I steal
some of the money too.

280
00:11:20,013 --> 00:11:22,097
- You too?

281
00:11:22,223 --> 00:11:25,351
Okay, would you now like
to continue your quest

282
00:11:25,477 --> 00:11:27,853
for the Lance of Caldahar?
- Fellas 伙计...

283
00:11:27,979 --> 00:11:29,355
we came here
for bitches.

284
00:11:29,481 --> 00:11:30,939
Where the bitches at?

285
00:11:31,066 --> 00:11:34,693
- All right, you see a bevy 一群
of well-endowed 天资好的 wenches 少妇.

286
00:11:34,819 --> 00:11:38,655
- Great, I grab them bitches,
and we all go in my SUV.

287
00:11:38,782 --> 00:11:41,450
- Fine. Kanye the giant,
Udar 攻击 the dwarf 变矮小 侏儒,

288
00:11:41,576 --> 00:11:45,120
and Gollin the cleric
climb into his SUV

289
00:11:45,246 --> 00:11:48,290
with their bitches
and their ill-gotten 不正当手段所得的 gains.

290
00:11:48,416 --> 00:11:49,583
- And our Alize.

291
00:11:49,709 --> 00:11:52,586
Plus Kanye the giant
puts his demo CD

292
00:11:52,712 --> 00:11:54,213
in the CD player-
- No, he doesn't.

293
00:11:54,339 --> 00:11:56,215
There are no CDs
in this world!

294
00:11:56,341 --> 00:11:57,925
You know what?

295
00:11:58,051 --> 00:12:00,928
Do whatever
you want.

296
00:12:05,225 --> 00:12:06,433
- All right.

297
00:12:06,559 --> 00:12:08,811
I guess I'm
the Dungeon 地牢 Master now.

298
00:12:08,937 --> 00:12:11,980
The Eye of Ona
opens up...

299
00:12:12,107 --> 00:12:14,024
16 titties
fall out.

300
00:12:14,150 --> 00:12:16,693
- Yeah!
- Hurrah!

301
00:12:20,573 --> 00:12:24,284
- Okay, so occasionally
Keegan and I will be recognized

302
00:12:24,411 --> 00:12:25,953
on the street.
- Occasionally 偶尔.

303
00:12:26,079 --> 00:12:28,122
- Occasionally happens.
- Occasionally, yeah.

304
00:12:28,248 --> 00:12:29,915
Uh, uh, and it's
always by the-

305
00:12:30,041 --> 00:12:33,085
by the same particular kind
of guy, every single time.

306
00:12:33,211 --> 00:12:36,380
- Yes, this dude has-
wears suit pants 西装裤 and high-tops 高顶.

307
00:12:36,506 --> 00:12:38,757
- Mm-hmm,
or conversely 相反地,

308
00:12:38,883 --> 00:12:40,843
sweat pants 长运动裤
and polished 擦亮的 dress shoes.

309
00:12:40,969 --> 00:12:42,469
- One or the other.
One or the other.

310
00:12:42,595 --> 00:12:44,430
And you can tell
he doesn't know our names,

311
00:12:44,556 --> 00:12:48,183
'cause my man just talk about,
"Ah-ha-ha-ha-ha-ha-ha-ha-ha-ha!

312
00:12:48,309 --> 00:12:50,602
"Ah, yeah, yeah, yeah
yeah, yeah, yeah, yeah.

313
00:12:50,728 --> 00:12:53,188
"Yeah, oh, yay!
That's what l-yeah, yeah!

314
00:12:53,314 --> 00:12:54,815
That's my shit, dog."
- Yeah.

315
00:12:54,941 --> 00:12:57,943
- But then inevitably 不可避免的-
inevitably he will say this-

316
00:12:58,069 --> 00:13:01,864
- "Yo, but ideally,
dog, ideally...

317
00:13:01,990 --> 00:13:03,699
"we could do some
shit together, man.

318
00:13:03,825 --> 00:13:04,867
Ideally..."

319
00:13:04,993 --> 00:13:06,368
- Never met him before!

320
00:13:06,494 --> 00:13:08,620
Never seen the man before
a day in my life.

321
00:13:08,746 --> 00:13:09,955
- Straight up, but, yo, I'm
trying to get what you doin'.

322
00:13:10,081 --> 00:13:11,707
I'm tryin' to do
what you doin'.

323
00:13:11,833 --> 00:13:15,085
What you are, where you at,
where you at.

324
00:13:15,211 --> 00:13:18,922
So ideally, ideally, I be doin'
what you doin' right now.

325
00:13:19,048 --> 00:13:20,716
- Why "ideally"?
Why "ideally"?

326
00:13:20,842 --> 00:13:22,843
Of course, "ideally"!
- Of course.

327
00:13:22,969 --> 00:13:24,553
- Ideally, I want
to wake up tomorrow

328
00:13:24,679 --> 00:13:26,722
and have my penis intact 完整的.
Ideally!

329
00:13:26,848 --> 00:13:28,891
- Ideally,
you would be wearing shoes

330
00:13:29,017 --> 00:13:30,142
that match your pants,
motherfucker.

331
00:13:30,268 --> 00:13:32,478
- Right. Exactly

332
00:13:34,272 --> 00:13:35,689
- Oh, my God.
So it's definitely happening?

333
00:13:35,815 --> 00:13:37,983
We're making a record?

334
00:13:38,109 --> 00:13:41,236
Oh, no, I promise,
it's gonna be a hit.

335
00:13:41,362 --> 00:13:42,529
Hey, no, man,
thank you.

336
00:13:42,655 --> 00:13:43,989
- What's up, my man?

337
00:13:44,115 --> 00:13:45,866
I heard
about your record deal, dog.

338
00:13:45,992 --> 00:13:47,451
You gonna keep it real,
now, right, right?

339
00:13:47,577 --> 00:13:49,077
You're gonna stay true
to your roots, right?

340
00:13:49,204 --> 00:13:51,163
You ain't gonna let down
your boy-your boy Dwayne.

341
00:13:51,289 --> 00:13:52,623
- Dwayne Washington.
- Yeah, yeah, yeah, yeah.

342
00:13:52,749 --> 00:13:54,583
- Ramsey Junior High-
- Yo, yo, yo!

343
00:13:54,709 --> 00:13:57,252
If it ain't Craig,
the man of the hour, homie.

344
00:13:57,378 --> 00:13:58,420
You remember me,
I'm Teresa's cousin.

345
00:13:58,546 --> 00:13:59,796
You gonna keep it real?

346
00:13:59,923 --> 00:14:01,465
- Yeah, definitely
gonna keep it real, man.

347
00:14:01,591 --> 00:14:05,886
So, yeah...
- What up? What up?

348
00:14:06,012 --> 00:14:09,097
It's my favorite Craig, mm-hmm.
- I don't know you, man.

349
00:14:09,224 --> 00:14:10,807
- You don't know me?

350
00:14:10,934 --> 00:14:13,143
Shit, is that
how it's gonna be, Craig?

351
00:14:13,269 --> 00:14:14,770
- No, I really don't know you.

352
00:14:14,896 --> 00:14:16,146
- You changed.

353
00:14:16,272 --> 00:14:17,564
- All right,
you good, you good.

354
00:14:17,690 --> 00:14:19,441
- Craig's phone.

355
00:14:19,567 --> 00:14:21,151
- Hey, youngblood.

356
00:14:21,277 --> 00:14:24,863
My girls take care of you,
you take care of me.

357
00:14:24,989 --> 00:14:26,031
- Oh, all right.
All right.

358
00:14:30,078 --> 00:14:31,119
Oh!

359
00:14:34,207 --> 00:14:35,499
Yeah, yeah.
Yeah, all right.

360
00:14:40,129 --> 00:14:42,548
- Hello, Craig's phone.

361
00:14:42,674 --> 00:14:43,924
It's for you, brother.
- Aww.

362
00:14:44,050 --> 00:14:46,468
Thanks, man. Thanks.
All right, hello?

363
00:14:46,594 --> 00:14:48,929
Yeah.

364
00:14:49,055 --> 00:14:51,431
It fell through?

365
00:14:51,558 --> 00:14:54,226
It's not happening?

366
00:14:54,352 --> 00:14:56,061
Damn, man...

367
00:14:56,187 --> 00:14:59,773
No, it's all right.
Thanks anyway.

368
00:14:59,899 --> 00:15:00,941
Sorry, everybod-

369
00:15:11,869 --> 00:15:13,870
Damn.

370
00:15:19,961 --> 00:15:21,837
Good evening,
my fellow Americans.

371
00:15:21,963 --> 00:15:26,133
Now before we begin, I'd like
to once again introduce you

372
00:15:26,259 --> 00:15:29,219
to my anger translator Luther.
- Hi.

373
00:15:29,345 --> 00:15:32,139
- Now this November,
I want each and every one of you

374
00:15:32,265 --> 00:15:35,726
to ask yourselves, "what has
changed in the last four years?"

375
00:15:35,852 --> 00:15:38,562
- Who killed
Osama bin Laden?

376
00:15:38,688 --> 00:15:40,897
- What has my administration 政府
accomplished?

377
00:15:41,024 --> 00:15:43,483
- Did we accomplish killing
America's biggest enemy?

378
00:15:43,610 --> 00:15:46,445
Uh, check,
did that, boom!

379
00:15:46,571 --> 00:15:51,074
- In 2011 alone, we created more
jobs than George W. Bush did

380
00:15:51,200 --> 00:15:52,909
in all eight years
of his office.

381
00:15:53,036 --> 00:15:54,411
- Except
for "Osama bin Laden hunter,"

382
00:15:54,537 --> 00:15:56,246
because that job
don't exist anymore,

383
00:15:56,372 --> 00:15:58,540
'cause I went over there,
and I killed him in his face.

384
00:15:58,666 --> 00:16:00,042
- We helped make health care

385
00:16:00,168 --> 00:16:01,335
accessible to more Americans

386
00:16:01,461 --> 00:16:02,961
than ever before.

387
00:16:03,087 --> 00:16:04,379
- I'm sorry, what'd you say?

388
00:16:04,505 --> 00:16:05,881
Your World Trade Center hurts?

389
00:16:06,007 --> 00:16:07,257
Then why don't you take
two dead bin Ladens

390
00:16:07,383 --> 00:16:09,968
and call me
in the morning, biz-itch?

391
00:16:10,094 --> 00:16:12,304
- This election,
make the decision

392
00:16:12,430 --> 00:16:15,265
that you think best serves
the future of this country.

393
00:16:15,391 --> 00:16:17,476
- Or you can eat
a dum-dum sandwich

394
00:16:17,602 --> 00:16:19,936
and just vote for the person
who didn't kill Osama bin Laden.

395
00:16:20,063 --> 00:16:22,648
But why would you do it?
Why would you do it?

396
00:16:22,774 --> 00:16:23,857
I mean, God damn.

397
00:16:23,983 --> 00:16:26,485
- I plan to run
a clean campaign,

398
00:16:26,611 --> 00:16:29,154
one based on the issues
and the accomplishments 造诣

399
00:16:29,280 --> 00:16:30,530
of my administration.
- Mm-hmm.

400
00:16:30,657 --> 00:16:32,282
But I'm gonna
tell you right now,

401
00:16:32,408 --> 00:16:35,994
if the Republicans-if they
had caught Osama bin Laden,

402
00:16:36,120 --> 00:16:37,496
there wouldn't even be
an election, man.

403
00:16:37,622 --> 00:16:38,997
They'd just put
a crown 皇冠 on his head,

404
00:16:39,123 --> 00:16:40,707
and give him a castle 城堡,
and just call him

405
00:16:40,833 --> 00:16:42,459
the King of America,
and that'd be it.

406
00:16:42,585 --> 00:16:43,835
I said,
that'd be it!

407
00:16:43,961 --> 00:16:45,504
- All right,
all right, all right.

408
00:16:45,630 --> 00:16:47,631
Just, you know, bring it down
a notch 等级, there, Luther.

409
00:16:47,757 --> 00:16:49,091
- Okay, come on,
Luther, man,

410
00:16:49,217 --> 00:16:50,509
you're straight up
out of control, brother.

411
00:16:50,635 --> 00:16:52,052
- Well, l-
it's not that bad.

412
00:16:52,178 --> 00:16:53,637
- Okay, don't beat
yourself up, it's okay.

413
00:16:53,763 --> 00:16:55,347
'Scuse me.

414
00:16:55,473 --> 00:16:57,015
- Can a nigga
get a lozenge?

415
00:16:57,141 --> 00:16:59,351
- Now, Luther, you-you-
you can't say that word.

416
00:16:59,477 --> 00:17:01,186
- Oh...

417
00:17:01,312 --> 00:17:03,647
actually it says right here,
I can say it whenever I want.

418
00:17:06,317 --> 00:17:09,695
- I guess I can say it too.

419
00:17:09,821 --> 00:17:13,657
Good night, my niggas.

420
00:17:24,961 --> 00:17:27,087
- Okay, so here's
a very sad thing-

421
00:17:27,213 --> 00:17:29,631
Jordan and I
could not be rappers.

422
00:17:29,757 --> 00:17:32,843
Couldn't do it.
- Whoa, whoa, whoa, whoa.

423
00:17:32,969 --> 00:17:35,595
- Seriously, Jordan?
Are you seriously fronting?

424
00:17:35,722 --> 00:17:38,557
We have absolutely
no street cred whatsoever 无论如何.

425
00:17:38,683 --> 00:17:42,269
What am I supposed to rap about,
my master's degree in fine arts?

426
00:17:45,231 --> 00:17:48,483
- L-I did-l-l-I dropped out
of college, so, you know,

427
00:17:48,609 --> 00:17:49,985
I still got that-
I still got that-

428
00:17:50,111 --> 00:17:53,321
- You dropped out
of Sarah Lawrence College

429
00:17:53,448 --> 00:17:55,198
in Westchester County,

430
00:17:55,324 --> 00:17:57,576
which makes you educated
and a lesbian.

431
00:17:57,702 --> 00:18:00,287
- I'm saying-it's just,
you gotta twist it, man.

432
00:18:00,413 --> 00:18:01,538
You just gotta-
- Oh, you gotta twist it.

433
00:18:01,664 --> 00:18:03,039
- Yeah.
- You gotta-you gotta twist it.

434
00:18:03,166 --> 00:18:04,207
- I'm talking about-
yeah, you-you do!

435
00:18:04,333 --> 00:18:05,834
Talking about,
yo, check it-

436
00:18:07,837 --> 00:18:10,046
♪ Yo, NYC,
born and raised ♪

437
00:18:10,173 --> 00:18:11,423
♪ In the playground is where
I spent most of my days ♪-

438
00:18:11,549 --> 00:18:15,010
- No, you did not!
No, you didn't.

439
00:18:15,136 --> 00:18:17,804
Fresh Prince of Bel-Air?
- Hold up, hold up, hold up.

440
00:18:17,930 --> 00:18:19,765
All right, I ain't finished,
I ain't finished:

441
00:18:19,891 --> 00:18:21,933
♪ Yo, all I had was my smarts,
and then the liberal arts ♪-

442
00:18:22,059 --> 00:18:23,518
- No!
- ♪ Came into my life ♪-

443
00:18:23,644 --> 00:18:25,103
- No, can't-
- All right-

444
00:18:25,229 --> 00:18:27,439
- No, because you cannot say
the words "liberal arts"

445
00:18:27,565 --> 00:18:28,857
in a rap song.
- Fine.

446
00:18:35,740 --> 00:18:38,283
All y'all rappers out there
think y'all hard

447
00:18:38,409 --> 00:18:40,786
'cause what y'all
been through?

448
00:18:40,912 --> 00:18:42,329
Hey, check it out.

449
00:18:42,455 --> 00:18:44,122
♪ It was Friday night chillin' ♪

450
00:18:44,248 --> 00:18:45,665
♪ On my front lawn ♪

451
00:18:45,792 --> 00:18:49,002
♪ Me and my crew,
I had my bathrobe on ♪

452
00:18:49,128 --> 00:18:50,796
♪ Everybody high ♪

453
00:18:50,922 --> 00:18:52,088
♪ Just watched X-Men ♪

454
00:18:52,215 --> 00:18:54,132
♪ Got plenty
chicken heads ♪

455
00:18:54,258 --> 00:18:55,300
♪ Ready for sex, men ♪

456
00:18:55,426 --> 00:18:56,760
♪ Busters roll up ♪

457
00:18:56,886 --> 00:18:58,386
♪ They tryin'
to talk smack ♪

458
00:18:58,513 --> 00:19:00,388
♪ Don't these
niggas know ♪

459
00:19:00,515 --> 00:19:01,932
♪ They fuckin'
with Tha Mack? ♪

460
00:19:02,058 --> 00:19:03,141
♪ Dude pulled his nine ♪

461
00:19:03,267 --> 00:19:05,018
♪ I knew homies seen us ♪

462
00:19:05,144 --> 00:19:06,478
♪ He pulled
two shots ♪

463
00:19:06,604 --> 00:19:08,021
♪ Somehow my penis
got between us ♪

464
00:19:08,147 --> 00:19:09,272
♪ I got shot
in the dick ♪

465
00:19:09,398 --> 00:19:10,816
♪ Shot, shot
in the dick ♪

466
00:19:10,942 --> 00:19:12,526
♪ I got shot
in the dick ♪

467
00:19:12,652 --> 00:19:14,402
- ♪ But his dick's
still standing ♪

468
00:19:14,529 --> 00:19:15,612
- ♪ Shot in the dick ♪

469
00:19:15,738 --> 00:19:17,447
♪ Shot, shot in the dick ♪

470
00:19:17,573 --> 00:19:18,949
♪ I got shot
in the dick ♪

471
00:19:19,075 --> 00:19:20,992
- ♪ But his dick's
still standing ♪

472
00:19:27,208 --> 00:19:29,584
- ♪ Went back
to my palace fast ♪

473
00:19:29,710 --> 00:19:33,880
♪ Now I'm the dude walking
'round with a phallus cast ♪

474
00:19:34,006 --> 00:19:36,550
♪ Like Wolverine,
I'm trying to be a loner ♪

475
00:19:36,676 --> 00:19:40,178
♪ But they think a nigga
got an adamantium boner ♪

476
00:19:40,304 --> 00:19:41,847
♪ What can I say? ♪

477
00:19:41,973 --> 00:19:43,765
♪ They still up
on my totem pole ♪

478
00:19:43,891 --> 00:19:46,393
♪ Bitch, get your tongue ring
away from the scrotum hole ♪

479
00:19:46,519 --> 00:19:48,144
♪ Don't know
why they love it ♪

480
00:19:48,271 --> 00:19:50,230
♪ But it keeps
the ladies moistening ♪

481
00:19:50,356 --> 00:19:52,941
♪ Gave half the honeys
in the 'hood lead poisoning ♪

482
00:19:53,067 --> 00:19:55,443
- ♪ Shot in the dick,
shot, shot in the dick ♪

483
00:19:55,570 --> 00:19:57,112
♪ I got shot in the dick ♪

484
00:19:57,238 --> 00:19:59,155
- ♪ But his dick's
still standing ♪

485
00:19:59,282 --> 00:20:00,490
- ♪ Shot in the dick ♪

486
00:20:00,616 --> 00:20:02,075
♪ Shot, shot
in the dick ♪

487
00:20:02,201 --> 00:20:03,910
♪ I got shot
in the dick ♪

488
00:20:04,036 --> 00:20:05,829
- ♪ But his dick's
still standing ♪

489
00:20:12,169 --> 00:20:15,922
- Yo, they shot
that fool in the dick!

490
00:20:22,179 --> 00:20:23,597
- Ahh!
Gee, fu-man!

491
00:20:23,723 --> 00:20:26,224
Damn, that hurts.
- What?

492
00:20:26,350 --> 00:20:29,019
- Damn-no, video over, man.
Video's over.

493
00:20:29,145 --> 00:20:31,271
- No!
- I'm serious, man.

494
00:20:31,397 --> 00:20:32,731
Damn...

495
00:20:35,526 --> 00:20:36,818
- Thank you.

496
00:20:36,944 --> 00:20:39,154
- Thank you very much.

497
00:20:39,280 --> 00:20:40,947
- He's got one.
- Yo.

498
00:20:41,073 --> 00:20:42,407
- He's got one.
- Check it, check it, check it.

499
00:20:42,533 --> 00:20:43,867
Check it, check it,
check it, check it:

500
00:20:43,993 --> 00:20:45,535
♪ Yo, all my boys
have my back ♪

501
00:20:45,661 --> 00:20:46,786
♪ While we're
playing hacky-sack ♪

502
00:20:46,913 --> 00:20:48,288
- Nope, nope, nope.

503
00:20:48,414 --> 00:20:51,082
Good night, everybody.
Safe home, God bless.

504
00:20:58,174 --> 00:21:01,051
- ♪ I'm gonna do
my one line here ♪

505
00:21:01,177 --> 00:21:03,178
- Oh, yeah.

506
00:21:05,765 --> 00:21:07,557
- Dude, I'm gonna-
I'm gonna put some weed smoke

507
00:21:07,683 --> 00:21:09,142
in your ear, dog.
- It's all right, dude.

508
00:21:09,268 --> 00:21:11,561
I'm gonna take
this grenade and toss it

509
00:21:11,687 --> 00:21:13,438
in the fraternity house
for no reason at all.

