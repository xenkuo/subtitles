﻿1
00:00:01,001 --> 00:00:02,293
- The zombies have
broken through!

2
00:00:02,419 --> 00:00:04,212
I repeat, zombies!

3
00:00:04,338 --> 00:00:07,256
- Aah!
- Oh, my God!

4
00:00:07,383 --> 00:00:10,009
Tommy's been bit!

5
00:00:19,978 --> 00:00:21,062
- Aah, damn!

6
00:00:22,940 --> 00:00:24,607
Damn it!
No, no, no!

7
00:00:24,733 --> 00:00:26,651
- What are you waiting for?
You know what you have to do!

8
00:00:26,777 --> 00:00:28,027
- No, I ca-
I ca-I can't do it.

9
00:00:28,153 --> 00:00:29,696
- D-do what?

10
00:00:29,822 --> 00:00:31,447
- You have to do it!
He's been bit!

11
00:00:31,573 --> 00:00:34,701
- Yes-aah!
Aah! Unh! Aah!

12
00:00:34,827 --> 00:00:36,494
- No, no, no!
- Forgive me, Tommy!

13
00:00:36,620 --> 00:00:37,912
- No, no, no, no, no!

14
00:00:45,045 --> 00:00:46,629
- Oh, no!

15
00:00:46,755 --> 00:00:48,381
- Aah!

16
00:00:48,507 --> 00:00:52,468
I ca-I can't believe
the zombies bit him!

17
00:00:52,594 --> 00:00:57,765
- It wasn't
a zombie, it was a raccoon 浣熊.

18
00:01:03,731 --> 00:01:06,733
Zombies and raccoons, right?

19
00:01:10,070 --> 00:01:13,489
No, it's just zombies?

20
00:01:16,618 --> 00:01:19,746
Oh, shit,
you just killed Tommy.

21
00:01:44,897 --> 00:01:46,522
- Yeah!

22
00:01:46,648 --> 00:01:48,357
Thank you!

23
00:01:48,484 --> 00:01:49,859
- Thank you.

24
00:01:49,985 --> 00:01:52,195
- Welcome to the show everybody,
I am Keegan.

25
00:01:52,321 --> 00:01:53,613
- And I am Jordan.

26
00:01:53,739 --> 00:01:55,031
- And this is Key & Peele.
- Yes.

27
00:01:55,157 --> 00:01:56,949
- So thanks for coming out.
- Thank you.

28
00:01:57,075 --> 00:01:59,202
Now I just wanna
start off by saying

29
00:01:59,328 --> 00:02:01,245
we are huge horror movie fans.
- Yes.

30
00:02:01,371 --> 00:02:03,206
- We love horror movies.
- We love the slashers.

31
00:02:03,332 --> 00:02:05,124
- Don't know if anybody's seen-
- Skite-skite-skite-skite!

32
00:02:05,250 --> 00:02:07,251
- Yeah, that's
our jam right there.

33
00:02:09,338 --> 00:02:10,797
- I don't know if anyone's
seen that movie Candyman?

34
00:02:10,923 --> 00:02:13,466
That's one
of our favorites, Candyman.

35
00:02:13,592 --> 00:02:17,136
That's the movie where you
say "Candyman" five times

36
00:02:17,262 --> 00:02:20,181
into a mirror in the bathroom,
and a black dude

37
00:02:20,307 --> 00:02:22,558
from the 19th century
with a hook for a hand

38
00:02:22,684 --> 00:02:25,478
and bees all over his face
comes out, and he kills you.

39
00:02:25,604 --> 00:02:27,897
That's just relevant because
if you watched that movie,

40
00:02:28,023 --> 00:02:30,274
and you went home and you
said "Candyman" five times

41
00:02:30,400 --> 00:02:33,736
into a mirror,
you are a white person.

42
00:02:33,862 --> 00:02:35,446
- Yes, a black person
would never do that.

43
00:02:35,572 --> 00:02:36,906
- Nope, we don't do that.

44
00:02:37,032 --> 00:02:38,908
- We do not take chances
with the supernatural 超自然.

45
00:02:39,034 --> 00:02:40,743
Just in case.

46
00:02:40,869 --> 00:02:42,578
- Well, the last thing that came
along that we didn't understand

47
00:02:42,704 --> 00:02:44,413
ended up bringing us to America,
so that was kind of

48
00:02:44,540 --> 00:02:46,082
a fucked up situation.

49
00:02:50,254 --> 00:02:53,005
That's why street magicians,
like, on the street-

50
00:02:53,131 --> 00:02:55,883
Like, a black person,
we will just excuse ourselves-

51
00:02:56,009 --> 00:02:57,468
- Oh, no, we will
vacate 空出 the premises 房屋/契约前言.

52
00:02:57,594 --> 00:02:59,512
- From that whole situation.
It would be like this-

53
00:02:59,638 --> 00:03:00,763
"Was the four
of clubs your card?"

54
00:03:00,889 --> 00:03:03,057
- "Oh, hell no.
You a demon, so..."

55
00:03:06,186 --> 00:03:09,313
- But, babe, moving out?

56
00:03:09,439 --> 00:03:12,316
Please, this
is the worst possible time.

57
00:03:12,442 --> 00:03:14,443
Please don't do this.

58
00:03:14,570 --> 00:03:15,903
Babe?

59
00:03:20,701 --> 00:03:23,411
- Garbage.
- Huh?

60
00:03:23,537 --> 00:03:24,704
- Can I take your garbage?

61
00:03:24,830 --> 00:03:28,040
- Yeah, sure.

62
00:03:29,585 --> 00:03:34,672
You know, I find
the more garbage in the can,

63
00:03:34,798 --> 00:03:40,094
the better it feels
to dump it all out.

64
00:03:40,220 --> 00:03:42,263
I suppose that's
why we let it get so full

65
00:03:42,389 --> 00:03:46,350
in the first place.

66
00:03:46,476 --> 00:03:49,604
So we can start over.

67
00:03:53,942 --> 00:03:57,320
- Here to fix the copier.
- Yeah, sure, sure.

68
00:03:57,446 --> 00:03:59,030
Hey, how did you-

69
00:03:59,156 --> 00:04:01,157
- Sometimes things
ain't really broken.

70
00:04:01,283 --> 00:04:03,492
It's the way
we treat 'em

71
00:04:03,619 --> 00:04:07,038
that needs to be fixed.

72
00:04:10,208 --> 00:04:14,503
Heh.

73
00:04:19,009 --> 00:04:20,885
- Who the hell
are you guys?

74
00:04:21,011 --> 00:04:25,806
The important question
is who are you, Steve?

75
00:04:28,727 --> 00:04:32,897
- Well, if it
isn't Mr. Stanley.

76
00:04:33,023 --> 00:04:35,566
- Carl.

77
00:04:40,238 --> 00:04:43,824
- You need to find
your own troubled white boy.

78
00:04:49,623 --> 00:04:52,959
- I was here first.

79
00:04:58,048 --> 00:04:59,090
Copycat 盲目模仿者!

80
00:04:59,216 --> 00:05:02,760
- Aagh!

81
00:05:04,680 --> 00:05:06,097
Sweep this...

82
00:05:06,223 --> 00:05:07,306
bitch!

83
00:05:07,432 --> 00:05:09,308
- Unh!

84
00:05:16,108 --> 00:05:17,149
Uh-oh.
- Hah!

85
00:05:17,275 --> 00:05:19,485
- Whoa! Unh!

86
00:05:35,460 --> 00:05:37,086
- Damn!

87
00:05:37,212 --> 00:05:38,254
Chesterfield!

88
00:05:41,842 --> 00:05:44,010
There can be only one

89
00:05:44,136 --> 00:05:46,554
magical negro!

90
00:05:46,680 --> 00:05:49,765
Hunh!

91
00:06:07,951 --> 00:06:10,953
Yaarrgh!

92
00:06:26,178 --> 00:06:27,678
- Good lord.

93
00:06:27,804 --> 00:06:31,098
Are you all right?

94
00:06:31,224 --> 00:06:36,312
Well, I guess sometimes
things have to come apart

95
00:06:36,438 --> 00:06:39,315
before we can put
'em back together again.

96
00:06:39,441 --> 00:06:42,818
- Oh, you're
a magical negro too.

97
00:06:42,944 --> 00:06:46,405
- Who you callin' "negro,"
bitch?

98
00:06:51,661 --> 00:06:53,871
- So Keegan's the nicest
guy in the world.

99
00:06:53,997 --> 00:06:55,456
Too nice, though.
- You think I'm too nice.

100
00:06:55,582 --> 00:06:57,166
- Sometimes you're too nice.
- Okay.

101
00:06:57,292 --> 00:06:58,876
- That one time
we went to a restaurant.

102
00:06:59,002 --> 00:07:01,378
We're starving, and
we are going to a restaurant,

103
00:07:01,505 --> 00:07:03,005
and there is a woman
outside the door

104
00:07:03,131 --> 00:07:04,507
of the restaurant
with a clipboard

105
00:07:04,633 --> 00:07:06,175
talking about giving money
for homeless kids.

106
00:07:06,301 --> 00:07:07,551
- Very nice girl,
Very sweet girl.

107
00:07:07,677 --> 00:07:08,761
- I don't care,
I don't care.

108
00:07:08,887 --> 00:07:09,929
- Very sweet.
- Of course she's sweet.

109
00:07:10,055 --> 00:07:11,472
I don't care.

110
00:07:11,598 --> 00:07:13,974
You do not stop for a bitch
with a clipboard.

111
00:07:14,101 --> 00:07:16,310
- Jordan-
- Don't do it, Keegan.

112
00:07:16,436 --> 00:07:17,770
- Let's talk about
what you did.

113
00:07:17,896 --> 00:07:19,313
Let's talk about
what he did for a second.

114
00:07:19,439 --> 00:07:20,773
This is my man right here-
my man just said,

115
00:07:20,899 --> 00:07:22,233
"Just so you know,
it's a no for me."

116
00:07:23,985 --> 00:07:27,655
Excuse me,
uh-huh, mm-hmm.

117
00:07:27,781 --> 00:07:29,073
- Look at that.
- Mm-mm, mm-mm.

118
00:07:29,199 --> 00:07:30,533
Mm, thank you, mm!

119
00:07:30,659 --> 00:07:31,700
- Well, that's because-
No, that's because

120
00:07:31,827 --> 00:07:32,868
you have to know-

121
00:07:32,994 --> 00:07:34,453
- Yeah, mm-hmm, mmm!

122
00:07:34,579 --> 00:07:35,746
- That's because I know he's
gonna be out there

123
00:07:35,872 --> 00:07:37,081
for the next 48 minutes
talking about,

124
00:07:37,207 --> 00:07:39,041
"Fascinating 迷人的, fascinating.

125
00:07:39,167 --> 00:07:41,460
"Why aren't these kids housed?

126
00:07:41,586 --> 00:07:43,212
"What's wrong?
I wanna know.

127
00:07:43,338 --> 00:07:44,547
"I wanna know
all the info.

128
00:07:44,673 --> 00:07:45,923
"What are their names?
One at a time.

129
00:07:46,049 --> 00:07:47,508
"Give them to me,
alphabetical 按字母顺序的.

130
00:07:47,634 --> 00:07:49,176
"No, I'll remember them.
I have a good memory.

131
00:07:49,302 --> 00:07:52,096
"I'll give you my email,
I'll give you my sosh.

132
00:07:52,222 --> 00:07:53,430
Do they have
a father figure?"

133
00:07:53,557 --> 00:07:55,641
"My name's Keegan.

134
00:07:55,767 --> 00:07:57,184
I'll give it to 'em."

135
00:07:59,479 --> 00:08:01,147
- All right.
Where you headed, youngster 年轻人?

136
00:08:01,273 --> 00:08:04,150
- Yeah I'm headed to
the Cobo building on Jefferson.

137
00:08:04,276 --> 00:08:06,318
- All right, yeah,
Cobo Hall, Jefferson Avenue.

138
00:08:06,444 --> 00:08:08,612
Mm-hmm.

139
00:08:08,738 --> 00:08:10,781
So, uh, what
you in Detroit for?

140
00:08:10,907 --> 00:08:12,449
- Uh, gotta
do a presentation.

141
00:08:12,576 --> 00:08:16,745
- All right, okay.
Yeah, a presentation, okay.

142
00:08:16,872 --> 00:08:18,414
What are you presenting?

143
00:08:18,540 --> 00:08:20,875
- Uh, actually
it's about how market research

144
00:08:21,001 --> 00:08:23,836
can be interpreted
with out-of-the-box solutions.

145
00:08:23,962 --> 00:08:25,004
- Oh, all right,
so you're gonna get this all

146
00:08:25,130 --> 00:08:28,340
outside the box, all right.

147
00:08:28,466 --> 00:08:30,050
- Most people
think that all customers

148
00:08:30,177 --> 00:08:31,844
are a single demographic 人口统计学的.

149
00:08:31,970 --> 00:08:33,721
- Oh, okay, you're trying
to get all them demographics

150
00:08:33,847 --> 00:08:35,139
up in here, all right.

151
00:08:35,265 --> 00:08:37,141
All right, yeah.

152
00:08:37,267 --> 00:08:39,310
- Online trends
are less linear than-

153
00:08:39,436 --> 00:08:42,605
- Oh, okay, online...
trends.

154
00:08:42,731 --> 00:08:45,357
Linear, mm-hmm.

155
00:08:47,485 --> 00:08:50,321
I'm also thinking about
doing the presentation naked.

156
00:08:50,447 --> 00:08:51,697
- Ooh.
- Just no clothes on.

157
00:08:51,823 --> 00:08:53,532
- Oh, yeah, yeah,
naked presentation.

158
00:08:53,658 --> 00:08:55,367
No clothes on, that's good.

159
00:08:55,493 --> 00:08:58,162
- Afterward, I might
just piss on the floor

160
00:08:58,288 --> 00:08:59,455
in front of everyone.

161
00:08:59,581 --> 00:09:00,998
- Oh, yeah,
okay, all right.

162
00:09:01,124 --> 00:09:02,208
Pissing right there
up in front of everybody.

163
00:09:02,334 --> 00:09:03,959
All right, sounds good.

164
00:09:04,085 --> 00:09:05,836
- Then I might just
shoot everybody in the place,

165
00:09:05,962 --> 00:09:07,213
you know.

166
00:09:07,339 --> 00:09:08,714
With the gun
I'm hiding in my ass.

167
00:09:08,840 --> 00:09:09,924
- Yeah, yeah,
gun way up in that ass.

168
00:09:10,050 --> 00:09:11,634
That's the way
to do it.

169
00:09:11,760 --> 00:09:13,886
- Cut my victims' faces off,
wear them as masks.

170
00:09:14,012 --> 00:09:15,971
- Mm-hmm, yeah,
masks go on the face.

171
00:09:16,097 --> 00:09:18,390
Uh-huh.
Yeah, okay.

172
00:09:18,516 --> 00:09:20,809
- Ich bin
ein schein. Heil Hitler.

173
00:09:20,936 --> 00:09:23,395
- Yep, okay,
heil Hitler to you too.

174
00:09:23,521 --> 00:09:24,730
- Heh.

175
00:09:24,856 --> 00:09:27,858
- All right, here we are.

176
00:09:30,195 --> 00:09:31,904
Oh, by the way.

177
00:09:32,030 --> 00:09:34,323
When you kill those people
with your ass gun,

178
00:09:34,449 --> 00:09:35,950
make sure to use
a serrated 锯齿形的 blade 刀片

179
00:09:36,076 --> 00:09:37,243
to cut their faces off.

180
00:09:38,995 --> 00:09:42,164
Auf wiedersehen.

181
00:09:54,386 --> 00:09:56,595
- Call my agent.

182
00:09:58,473 --> 00:10:00,224
- Jaden Pinkett Smith, my man.

183
00:10:00,350 --> 00:10:01,809
- What up, Clyde?

184
00:10:01,935 --> 00:10:04,186
Hey look, man, I'm gonna
be real with you okay?

185
00:10:04,312 --> 00:10:07,273
This Alien Boy script,
I'm not feelin' it.

186
00:10:07,399 --> 00:10:09,733
I mean, it's hard
being Will Smith's son.

187
00:10:09,859 --> 00:10:13,487
I just wanna make sure
my next project reflects

188
00:10:13,613 --> 00:10:15,322
that I'm a down-to-earth,
normal kid.

189
00:10:15,448 --> 00:10:16,907
Am I making any sense?

190
00:10:17,033 --> 00:10:18,909
- Yeah, yeah,
I'm reading you loud and clear.

191
00:10:19,035 --> 00:10:21,745
I've got a script right here,
very real, called Street Ball.

192
00:10:21,871 --> 00:10:23,914
- Talk to me.
- Okay, so you play Tony.

193
00:10:24,040 --> 00:10:25,541
He's a kid from the streets,
lives in the hood

194
00:10:25,667 --> 00:10:27,084
in a house
with his mom.

195
00:10:27,210 --> 00:10:29,628
- Stop.

196
00:10:29,754 --> 00:10:30,796
House?

197
00:10:30,922 --> 00:10:31,964
Oh, it's like a...

198
00:10:32,090 --> 00:10:34,341
tiny mansion 公馆.
- Got it, go on.

199
00:10:34,467 --> 00:10:36,218
- So his mom and he,
uh, they live in this house

200
00:10:36,344 --> 00:10:37,845
and she works
at a supermarket.

201
00:10:37,971 --> 00:10:39,388
- Stop.

202
00:10:39,514 --> 00:10:41,724
- Oh, yeah, a supermarket's
like a mansion,

203
00:10:41,850 --> 00:10:43,559
uh, but it's full of food,
and anyone can go there.

204
00:10:43,685 --> 00:10:46,103
- So, like,
where the butlers 男管家 go?

205
00:10:46,229 --> 00:10:48,063
- Yeah, basically.
So he, uh-

206
00:10:48,189 --> 00:10:49,315
- And wait.

207
00:10:49,441 --> 00:10:51,358
You said his mom
was doing something

208
00:10:51,484 --> 00:10:52,735
at the supermarket?

209
00:10:52,861 --> 00:10:55,404
- Working.
- So...

210
00:10:55,530 --> 00:10:57,906
- Working is kind of like acting
on a set every day

211
00:10:58,033 --> 00:10:59,700
in a film that
no one's ever gonna see.

212
00:10:59,826 --> 00:11:01,535
And it lasts
for the rest of your life.

213
00:11:01,661 --> 00:11:03,287
- Oh, like a maid 女仆!

214
00:11:03,413 --> 00:11:04,997
- Yeah, yeah, she's like
the maid of the supermarket.

215
00:11:05,123 --> 00:11:06,790
- Awesome, continue.

216
00:11:06,916 --> 00:11:09,209
- So anyway, Tony doesn't
make the basketball team, right?

217
00:11:09,336 --> 00:11:10,627
So he decides to start
playing pickup games

218
00:11:10,754 --> 00:11:12,463
of street ball outside.

219
00:11:12,589 --> 00:11:16,425
- So he plays basketball
in his plane?

220
00:11:16,551 --> 00:11:17,843
- No, on the ground.

221
00:11:17,969 --> 00:11:19,345
- In his limo 豪华轿车?

222
00:11:19,471 --> 00:11:21,305
- No, uh, outside
is that stuff

223
00:11:21,431 --> 00:11:23,640
that goes by when
you're inside the limo.

224
00:11:23,767 --> 00:11:25,100
- Oh, snap!

225
00:11:25,226 --> 00:11:27,186
He plays basketball
outside the limo door?

226
00:11:27,312 --> 00:11:28,604
- Yeah, yeah,
yeah, yeah, yeah.

227
00:11:28,730 --> 00:11:30,731
So then he gets
really good at basketball,

228
00:11:30,857 --> 00:11:32,524
and then blah-blah
blah-blah-blah, fast forward.

229
00:11:32,650 --> 00:11:35,736
At the heart of the story, Tony
has to choose between his mom-

230
00:11:35,862 --> 00:11:38,822
- Choose.

231
00:11:38,948 --> 00:11:41,158
- Yeah, "choose" is when
you have to make a decision

232
00:11:41,284 --> 00:11:44,620
between two things you want,
but you can have only one.

233
00:11:44,746 --> 00:11:48,332
- But... there's two.

234
00:11:48,458 --> 00:11:52,044
- Yeah, but, I mean,
he can't have both.

235
00:11:52,170 --> 00:11:55,297
- What'd I say
about science fiction 小说, Clyde?

236
00:11:55,423 --> 00:11:57,174
It's a no.

237
00:11:57,300 --> 00:11:59,635
- Okay, we'll pass.

238
00:11:59,761 --> 00:12:00,803
Achoo!

239
00:12:00,929 --> 00:12:02,763
- Dad bless you.
- Thanks.

240
00:12:07,936 --> 00:12:09,311
- Welcome to the show.

241
00:12:09,437 --> 00:12:10,854
Tonight we are a full hour
on Baby Prudence 审慎,

242
00:12:10,980 --> 00:12:12,773
the child who
was abducted 劫持 yesterday.

243
00:12:12,899 --> 00:12:15,317
We have the 911 call
and the last taken home video

244
00:12:15,443 --> 00:12:16,527
of Baby Pru.

245
00:12:16,653 --> 00:12:18,112
We have reporters
in the field

246
00:12:18,238 --> 00:12:20,572
as hundreds of people
are searching for-

247
00:12:20,698 --> 00:12:21,949
What's that?
They found her?

248
00:12:22,075 --> 00:12:25,285
She's fine?
Well fuck me! Gaa!

249
00:12:29,707 --> 00:12:33,043
- Uh, so I have been married,
uh, for 13 years.

250
00:12:34,337 --> 00:12:38,715
Thank you,
thank you.

251
00:12:38,842 --> 00:12:40,968
I'm very happily married.
- Gross 显而易见的.

252
00:12:41,094 --> 00:12:43,387
No, no, no.

253
00:12:43,513 --> 00:12:45,597
Your marriage is beautiful.
Your marriage is not gross.

254
00:12:45,723 --> 00:12:48,350
But for me,
that is not an option, marriage.

255
00:12:48,476 --> 00:12:50,894
- Okay, yeah, Jordan
is not a big fan of marriage.

256
00:12:51,020 --> 00:12:53,856
For Jordan, marriage is, um-
- The end of life.

257
00:12:53,982 --> 00:12:55,149
- Yes.

258
00:12:55,275 --> 00:12:57,151
The end of life.
- That, yes, yes.

259
00:12:57,277 --> 00:12:58,402
I don't get tattoos 刺青 either,
by the way.

260
00:12:58,528 --> 00:13:00,237
- Mm-hmm.
- Okay?

261
00:13:00,363 --> 00:13:02,781
To me, marriage seems like
getting a tattoo that's alive

262
00:13:02,907 --> 00:13:05,409
and makes you wait to watch
the next episode 插曲 of everything.

263
00:13:05,535 --> 00:13:07,161
I don't want that.
- See, and I don't care.

264
00:13:07,287 --> 00:13:10,205
I just as soon wait for my wife
so we can watch it together.

265
00:13:11,624 --> 00:13:12,666
- Ew.

266
00:13:14,043 --> 00:13:16,712
No, no!

267
00:13:16,838 --> 00:13:18,505
But, okay, I like
to go to Amsterdam, for example.

268
00:13:18,631 --> 00:13:19,673
- Yes, mm-hmm.
- Every now and then.

269
00:13:20,967 --> 00:13:22,676
- Thank you!

270
00:13:22,802 --> 00:13:24,803
When you-when you get married,
you can't go to Amsterdam.

271
00:13:24,929 --> 00:13:26,096
- Oh, yes, you can.
- No.

272
00:13:26,222 --> 00:13:27,890
- I can go to Amsterdam
with my wife.

273
00:13:28,016 --> 00:13:29,516
- Yes,
but you can't Amsterdam.

274
00:13:31,269 --> 00:13:34,229
- Oh, well...

275
00:13:34,355 --> 00:13:36,106
- The mood is infectious 有传染/感染力的
and exciting today

276
00:13:36,232 --> 00:13:37,816
as people from
all walks of life

277
00:13:37,942 --> 00:13:39,568
celebrate becoming
the seventh state

278
00:13:39,694 --> 00:13:41,195
to legalize 使合法化 gay marriage.

279
00:13:41,321 --> 00:13:43,030
We're here talking
to excited couples about

280
00:13:43,156 --> 00:13:45,949
how they feel
on this historic day.

281
00:13:46,075 --> 00:13:47,367
Oh, hi.
- Hi, hi.

282
00:13:47,494 --> 00:13:48,827
Uh, yeah, it's
a very historic day

283
00:13:48,953 --> 00:13:49,995
for civil 公民的 rights.

284
00:13:50,121 --> 00:13:51,413
- Whoo!
- And for gay Americans.

285
00:13:51,539 --> 00:13:52,956
And Americans
all over the country-

286
00:13:53,082 --> 00:13:56,335
- Whoo! We're gonna
get married! Yeah!

287
00:13:56,461 --> 00:13:58,462
Well, you know, wait-

288
00:13:58,588 --> 00:14:00,547
- We said that
it would be a conversation,

289
00:14:00,673 --> 00:14:01,882
you know what I mean?
Because we didn't know

290
00:14:02,008 --> 00:14:03,634
this was gonna pass
so darn 见鬼 fast.

291
00:14:03,760 --> 00:14:05,594
- Oh, my God!
- So are you guys a couple?

292
00:14:05,720 --> 00:14:06,929
Are we a couple?

293
00:14:07,055 --> 00:14:08,514
Come on, girl,
let's get serious.

294
00:14:08,640 --> 00:14:10,557
- No, it's just so fast.
- My name is Lashawn.

295
00:14:10,683 --> 00:14:13,602
And this is right here
is my "Samwich."

296
00:14:13,728 --> 00:14:15,687
It's, uh, Samuel, yeah.

297
00:14:15,813 --> 00:14:18,357
And we're gonna
get married! Yeah!

298
00:14:18,483 --> 00:14:19,691
- That's so great.

299
00:14:19,817 --> 00:14:21,151
How long have you guys
been together?

300
00:14:21,277 --> 00:14:22,903
- Well, we've been-
- Three years.

301
00:14:23,029 --> 00:14:25,113
It's been forever,
we've been waiting forever!

302
00:14:25,240 --> 00:14:27,783
- It's really important
to know the person-

303
00:14:27,909 --> 00:14:30,786
- Who is the bride?
I am the bride.

304
00:14:30,912 --> 00:14:32,204
Do-do-do-do-do-do-do!

305
00:14:32,330 --> 00:14:33,997
- Oh, well tell us
all about your plans.

306
00:14:34,123 --> 00:14:35,958
- You know, we never thought
it was important to have

307
00:14:36,084 --> 00:14:37,793
a piece of paper,
so there's not any plans-

308
00:14:37,919 --> 00:14:39,294
- Oh, yeah!
Piece of paper!

309
00:14:39,420 --> 00:14:40,837
We're gonna get
that piece of paper, Sammy!

310
00:14:40,964 --> 00:14:42,881
- Yeah, yeah.
- That piece of paper!

311
00:14:43,007 --> 00:14:44,841
- Where do you think
you guys will get married?

312
00:14:44,968 --> 00:14:46,718
- You know, there's a lot
of hidden costs in a wedding-

313
00:14:46,844 --> 00:14:48,095
- Oh, everywhere!

314
00:14:48,221 --> 00:14:49,596
We're gonna
get married over here

315
00:14:49,722 --> 00:14:52,015
and over there
and in the sky and on a cloud.

316
00:14:52,141 --> 00:14:53,934
- Oh, wow, it sounds like
it's gonna be a big wedding.

317
00:14:54,060 --> 00:14:55,644
- Well, you know it's just
a conversation that we have-

318
00:14:55,770 --> 00:15:00,315
- Girl, we're gonna rent
the moon and fill it with roses!

319
00:15:00,441 --> 00:15:02,693
- We really need to talk
about whether or not we think

320
00:15:02,819 --> 00:15:04,820
it's fair to even get married
when it's still illegal

321
00:15:04,946 --> 00:15:06,238
in so many
other states-

322
00:15:06,364 --> 00:15:07,614
- You see? Look at him!

323
00:15:07,740 --> 00:15:09,408
That's my man
with his big heart.

324
00:15:09,534 --> 00:15:12,327
I'm sorry, my husband.
You my husband now.

325
00:15:12,453 --> 00:15:13,870
- Well, we just-
- You my husband now, bitch.

326
00:15:13,997 --> 00:15:15,747
- We just don't wanna
rush into anything,

327
00:15:15,873 --> 00:15:17,082
because stuff
gets overturned 倒转的.

328
00:15:17,208 --> 00:15:18,709
Remember what happened
in California.

329
00:15:18,835 --> 00:15:20,419
- Baby, I'm gonna
get a 14-karat ring

330
00:15:20,545 --> 00:15:22,838
the size
of 14 motherfuckin' carrots.

331
00:15:22,964 --> 00:15:25,591
That's what's up, doc!

332
00:15:25,717 --> 00:15:27,509
- Well, you two
certainly seem excited.

333
00:15:27,635 --> 00:15:29,469
- Yeah, do we seem excited?
- Oh, yeah, yeah.

334
00:15:29,596 --> 00:15:30,887
- Oh, okay.
- Congratulations.

335
00:15:31,014 --> 00:15:32,889
I hope you guys
have a wonderful life together.

336
00:15:33,016 --> 00:15:34,891
- We just-we really just
didn't think it was gonna pass.

337
00:15:35,018 --> 00:15:37,936
- We're gonna have a house
that's shaped like a unicorn,

338
00:15:38,062 --> 00:15:39,396
and we're gonna
have five little girls.

339
00:15:39,522 --> 00:15:42,357
Their names are gonna be
Etnie, Carousel, Sequin,

340
00:15:42,483 --> 00:15:43,942
Abercrombie, and Phantom.

341
00:15:44,068 --> 00:15:46,403
And we're gonna
have a little dog named Ruffalo.

342
00:15:46,529 --> 00:15:48,947
And the dog gonna
have a cat named Myriad.

343
00:15:53,036 --> 00:15:54,202
- And we have breaking news.

344
00:15:54,329 --> 00:15:56,038
Baby Lakisha has gone missing.

345
00:15:56,164 --> 00:15:57,831
For the next hour we will-
what's that? Nobody cares?

346
00:15:57,957 --> 00:16:00,167
Moving on, let's check in
with baby Prudence.

347
00:16:00,293 --> 00:16:03,295
Her 14th day home,
and she's still safe and sound.

348
00:16:08,217 --> 00:16:09,926
- No, I definitely-
I am very happily married,

349
00:16:10,053 --> 00:16:11,511
but I have to say
that there are times

350
00:16:11,638 --> 00:16:13,347
in a relationship where
you have to use psychology 心理学,

351
00:16:13,473 --> 00:16:14,681
you know,
to get what you want.

352
00:16:14,807 --> 00:16:16,224
- Oh, definitely.
- Oh, yeah, yeah.

353
00:16:16,351 --> 00:16:17,643
- I know exactly-
I mean, I have a girlfriend,

354
00:16:17,769 --> 00:16:19,019
so I know what
you're talking about.

355
00:16:19,145 --> 00:16:20,479
For me it's when
I'm watching HBO,

356
00:16:20,605 --> 00:16:23,565
and the girl takes her top off
in the HBO show,

357
00:16:23,691 --> 00:16:24,983
which is inevitable 不可避免的.

358
00:16:25,109 --> 00:16:27,653
And the only way
I can continue watching the show

359
00:16:27,779 --> 00:16:29,237
is if I preempt 取代 her.

360
00:16:29,364 --> 00:16:30,572
I'm like, "Oh, see,
that's just a shame

361
00:16:30,698 --> 00:16:32,032
"she feels like
she has to do that.

362
00:16:34,702 --> 00:16:36,620
"It's just a shame.
It's gratuitous 不必要的, is what it is.

363
00:16:38,581 --> 00:16:40,791
I don't even wanna watch-
We can watch it, but you know."

364
00:16:43,544 --> 00:16:44,670
- Oh, man.

365
00:16:44,796 --> 00:16:46,797
What I do,
I like to walk my dogs.

366
00:16:46,923 --> 00:16:49,966
But I pretend like
I hate walking my dogs.

367
00:16:50,093 --> 00:16:52,844
Because one hour of dog walking
equals five hours of football.

368
00:16:52,970 --> 00:16:54,721
- Yes, smart.

369
00:16:54,847 --> 00:16:58,475
- So I'll come in the door
just talking about, Ooh! Ooh!

370
00:16:58,601 --> 00:17:00,352
- Right.

371
00:17:00,478 --> 00:17:02,771
- Ooh!
- Yeah, you gotta play it up.

372
00:17:02,897 --> 00:17:05,857
- My hammies 肌腱!
- Yeah, yeah, you gotta...

373
00:17:05,983 --> 00:17:08,610
Very smart.
- Oh! Five hours of football.

374
00:17:08,736 --> 00:17:10,445
- So you sort of bank
the favor, in a way.

375
00:17:10,571 --> 00:17:12,280
- So, yeah,
I bank the favor.

376
00:17:12,407 --> 00:17:13,865
- Which is kind of-
Another thing I do

377
00:17:13,991 --> 00:17:15,867
is I will bank a complaint.
- Oh, yeah, yeah, yeah.

378
00:17:15,993 --> 00:17:18,870
- Yeah, if she does something
I don't really care about

379
00:17:18,996 --> 00:17:22,124
if she did it,
but I will wait.

380
00:17:22,250 --> 00:17:24,084
And I will wait until
she calls me out on some shit

381
00:17:24,210 --> 00:17:25,627
to bring it up.

382
00:17:25,753 --> 00:17:26,878
- Yeah, yeah.
- You know what I'm saying?

383
00:17:27,004 --> 00:17:28,255
- So if she
says to you, um,

384
00:17:28,381 --> 00:17:29,589
"why'd you leave
the toilet seat up?"

385
00:17:29,716 --> 00:17:31,633
- Bitch, why was
you late last week,

386
00:17:31,759 --> 00:17:34,261
and I ain't say nothing?

387
00:17:34,387 --> 00:17:37,222
I was gonna let
that shit slide!

388
00:17:37,348 --> 00:17:38,724
- That's very good.
I'm gonna try that.

389
00:17:38,850 --> 00:17:40,308
- Yeah.

390
00:17:43,604 --> 00:17:45,689
- Now, first,
let me just say

391
00:17:45,815 --> 00:17:48,859
that I'm encouraged that so many
of you from the Republican party

392
00:17:48,985 --> 00:17:52,154
have agreed
to meet me in this way.

393
00:17:52,280 --> 00:17:54,531
Now, as you know,
I've tried very hard

394
00:17:54,657 --> 00:17:56,533
to reach across the aisle 通道

395
00:17:56,659 --> 00:17:58,618
and govern this country
from the middle.

396
00:17:58,745 --> 00:17:59,786
- Well...

397
00:17:59,912 --> 00:18:03,290
all due respect,
Mr. President,

398
00:18:03,416 --> 00:18:05,709
we disagree with you.

399
00:18:05,835 --> 00:18:08,628
- We think you should
run the country from...

400
00:18:08,755 --> 00:18:10,380
not the middle.

401
00:18:10,506 --> 00:18:12,924
- Well,
that's why we're here.

402
00:18:13,050 --> 00:18:15,302
If you would, I'd just
appreciate some feedback

403
00:18:15,428 --> 00:18:17,971
on some new ideas
and directions for this country.

404
00:18:18,097 --> 00:18:21,349
Feel free to agree
or disagree.

405
00:18:21,476 --> 00:18:23,268
Whatever you want.

406
00:18:23,394 --> 00:18:24,936
- We're gonna disagree.

407
00:18:26,439 --> 00:18:28,607
- Yeah, disagree.
- Disagree.

408
00:18:28,733 --> 00:18:30,358
Disagree.

409
00:18:30,485 --> 00:18:34,196
- First, I think
the government is too big.

410
00:18:34,322 --> 00:18:36,198
I think we need
to shrink the size

411
00:18:36,324 --> 00:18:38,909
of the federal government
so that all decisions

412
00:18:39,035 --> 00:18:40,786
can be made
at the state level.

413
00:18:40,912 --> 00:18:42,788
- We disagree, Mr. President.

414
00:18:42,914 --> 00:18:45,123
- Mm-hmm.

415
00:18:45,249 --> 00:18:47,667
- You drive
a hard bargain 交易, you win.

416
00:18:50,254 --> 00:18:52,589
- There we go.

417
00:18:52,715 --> 00:18:55,509
- Big government it is.

418
00:18:57,762 --> 00:18:59,179
- All right, next up, this
has been a hot button issue,

419
00:18:59,305 --> 00:19:01,515
but I think we can
settle it right here.

420
00:19:01,641 --> 00:19:04,976
No taxes for rich people.

421
00:19:05,102 --> 00:19:06,228
- We completely disagree!

422
00:19:06,354 --> 00:19:08,021
- Round two
goes to you guys.

423
00:19:09,315 --> 00:19:11,107
- Okay.
- Uh-huh.

424
00:19:11,234 --> 00:19:13,109
- More taxes
for the rich.

425
00:19:13,236 --> 00:19:14,736
- What the fuck!

426
00:19:14,862 --> 00:19:17,531
- What is happening to us?

427
00:19:17,657 --> 00:19:19,783
- Man, I am taking
a beating here, guys.

428
00:19:19,909 --> 00:19:22,661
All right, last issue-
immigration.

429
00:19:22,787 --> 00:19:25,497
Now, we need
to secure our borders.

430
00:19:25,623 --> 00:19:28,124
And anybody
who's here illegally

431
00:19:28,251 --> 00:19:30,585
should be hunted down
and deported 驱逐出境.

432
00:19:35,424 --> 00:19:37,467
We disagree!

433
00:19:43,182 --> 00:19:46,017
- We are country of immigrants!
No!

434
00:19:46,143 --> 00:19:49,062
- We need a clear path
to citizenship 公民权利

435
00:19:49,188 --> 00:19:51,356
for all of those
who are already here!

436
00:19:51,482 --> 00:19:53,900
Aah!
Help me!

437
00:19:59,490 --> 00:20:03,201
- Great,
I love your ideas.

438
00:20:03,327 --> 00:20:06,204
Lastly, we cannot
regulate firearms 轻武器.

439
00:20:06,330 --> 00:20:08,331
Yes, we can!

440
00:20:08,457 --> 00:20:11,251
Yes, we can!
Yes, we can!

441
00:20:11,377 --> 00:20:14,337
Yes, we can!
Yes, we can!

442
00:20:15,923 --> 00:20:18,091
- All right, all right.
If you guys say so.

443
00:20:18,217 --> 00:20:21,511
And nobody better
throw me a cigarette.

444
00:20:25,224 --> 00:20:27,893
Ain't I a stinker 臭鬼?

445
00:20:38,279 --> 00:20:41,615
- Oh, hell no, you a demon!
No, no, you a demon.

446
00:20:41,741 --> 00:20:44,075
- Good night, everybody!
Thank you!

447
00:20:48,080 --> 00:20:50,957
- ♪ I'm gonna do
my one line here ♪

448
00:20:51,083 --> 00:20:54,085
- Oh, yeah.

449
00:20:55,880 --> 00:20:57,797
- When you get married,
you can't hang out with a girl

450
00:20:57,924 --> 00:20:58,965
who is just
a friend anymore.

451
00:20:59,091 --> 00:21:00,717
- No, no, that's not true.

452
00:21:00,843 --> 00:21:03,345
There are some women out there
who are more than willing

453
00:21:03,471 --> 00:21:05,096
to let you hang out
with your female friends.

454
00:21:05,222 --> 00:21:07,349
- Okay, well, when
I meet that mythical bitch-

455
00:21:07,475 --> 00:21:09,726
my knee will hit the floor.

